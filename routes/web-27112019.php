<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();





Route::prefix('admin')->namespace('Admin')->group(function () {

    Route::middleware('admin')->group(function () {
       
       
    		Route::get('/', 'HomeController@index')->name('home');

            /* ---------- Profile Crud ------------*/

            Route::get('/profile', 'ProfileController@index')->name('profile');
            Route::post('/profile-save', 'ProfileController@save')->name('profile.store');


            Route::match(['get','post'],'/currency', 'ProfileController@currency')->name('currency.store');

            /* ---------- Sales Tracker ------------*/

            Route::post('/business-role', 'ProfileController@business')->name('business');
            Route::get('/sales-record-single/{userId}', 'SalesTrackerController@single');

            Route::get('/sales-tracker', 'SalesTrackerController@trackerList')->name('admin.tracker');
            Route::get('/sales-search', 'SalesTrackerController@searchRecord')->name('admin.searchrecord');

            Route::get('/tracker-chart/{userId}', 'SalesTrackerController@charts')->name("admin.tracker.chart");
            Route::post('/tracker-save', 'SalesTrackerController@save')->name('admin.tracker.store');

            /* ---------- User Crud ------------*/

            Route::get('/user-list', 'UserController@index')->name('admin.user.trash');
            Route::get('/user-status', 'UserController@index')->name('user.status');
            Route::match(['get','post'],'/user-create', 'UserController@create')->name('user.create');
            Route::post('/user-save', 'UserController@store')->name('user.store');
            Route::match(['get','post'],'/user-password', 'UserController@password')->name('user.password');
            Route::get('/user-status', 'UserController@status')->name('user.status');
       
	});

});

Route::prefix('salesmanager')->namespace('Manager')->group(function () {

    Route::middleware('manager')->group(function () {

        Route::get('/', 'HomeController@index')->name('home');

        /* ---------- Profile Crud ------------*/

        Route::get('/profile', 'ProfileController@index')->name('manager.profile');
        Route::post('/profile-save', 'ProfileController@save')->name('manager.profile.store');

        /* ---------- Sales Manager ------------*/

        Route::get('/sales-record-single/{userId}', 'SalesTrackerController@single');

        Route::get('/tracker-chart', 'SalesTrackerController@charts')->name("manager.tracker.chart");

        /* ---------- User Crud ------------*/

        Route::get('/user-list', 'UserController@index')->name('manager.user.trash');
        Route::get('/user-status', 'UserController@index')->name('manager.user.status');
        Route::match(['get','post'],'/user-create', 'UserController@create')->name('manager.user.create');
        Route::post('/user-save', 'UserController@store')->name('manager.user.store');
        Route::match(['get','post'],'/user-password', 'UserController@password')->name('manager.user.password');
        Route::get('/user-status', 'UserController@status')->name('manager.user.status');

    });

});

Route::prefix('salesperson')->namespace('SalesPerson')->group(function () {

    Route::middleware('salesperson')->group(function () {

        Route::get('/', 'HomeController@index')->name('salesperson.home');

        /* ---------- Profile Crud ------------*/

        Route::get('/profile', 'ProfileController@index')->name('salesperson.profile');

        Route::post('/profile-save', 'ProfileController@save')->name('salesperson.profile.store');

        Route::get('/salestracker', 'TrackerController@index')->name('tracker');

        Route::post('/tracker-save', 'TrackerController@save')->name('tracker.store');

        Route::post('/update-list', 'TrackerController@updateList')->name('salesperson.record');

    });

});
Route::get('/salesmanagers', 'HomeController@salesmanagers')->name('salesmanagers');


Route::get('/home', 'HomeController@index')->name('home');
Route::post('postaction', 'LoginController@postaction')->name('postaction');
Route::get('login', 'LoginController@index')->name('login');
Route::get('/', 'LoginController@index');
Route::get('/logout', 'LoginController@logout');

Route::get('reset/{token}', 'LoginController@resetpassword');
Route::post('/resetaction/{otp}','LoginController@resetActionpassword')->name("resetaction.reset");
/*15645
|----------------------------------------------------------------------------------------------------------
*/
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize-clear', function() {
    $exitCode = Artisan::call('optimize:clear');
    return '<h1>Reoptimized class loader</h1>';
});
<?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/remindertomanager', 'HomeController@reminderTMemail')->name('remindertomanager');

Route::get('/remindertoadmin', 'HomeController@reminderAdmin')->name('remindertoadmin');

Auth::routes();





Route::prefix('admin')->namespace('Admin')->group(function () {

    Route::middleware('admin')->group(function () {
       
       
    		Route::get('/', 'HomeController@index')->name('home');

            Route::get('/business-line/{id}', 'HomeController@businessline')->name('businessline');

            Route::get('/records', 'SalesTrackerController@records')->name('admin.records');

            /* ---------- Profile Crud ------------*/

            Route::get('/profile', 'ProfileController@index')->name('profile');
            Route::post('/profile-save', 'ProfileController@save')->name('profile.store');


            Route::match(['get','post'],'/currency', 'ProfileController@currency')->name('currency.store');

            /* ---------- Sales Tracker ------------*/

            Route::post('/business-role', 'ProfileController@business')->name('business');
            Route::get('/sales-record-single/{userId}', 'SalesTrackerController@single');

            Route::get('/sales-tracker', 'SalesTrackerController@trackerList')->name('admin.tracker');
            Route::get('/sales-search', 'SalesTrackerController@searchRecord')->name('admin.searchrecord');

            Route::get('/tracker-chart/{userId}', 'SalesTrackerController@charts')->name("admin.tracker.chart");
            Route::post('/tracker-save', 'SalesTrackerController@save')->name('admin.tracker.store');

            Route::post('/target-save', 'SalesTrackerController@targetSave')->name('admin.target.save');

            /* ---------- User Crud ------------*/ 

            Route::match(['get','post'],'/user-list', 'UserController@index')->name('admin.user.trash');
            Route::get('/user-status', 'UserController@index')->name('user.status');
            Route::match(['get','post'],'/user-create', 'UserController@create')->name('user.create');
            Route::post('/user-save', 'UserController@store')->name('user.store');
            Route::match(['get','post'],'/user-password', 'UserController@password')->name('user.password');
            Route::get('/user-status', 'UserController@status')->name('user.status');

            Route::get('/record_manage', 'SalesTrackerController@recordManage')->name('admin.record_manage');

            Route::post('/record_save', 'SalesTrackerController@saveRcords')->name('admin.record_save');

            Route::get('send-password/{id}','UserController@sendPassword')->name("user.passwordsend");

            Route::get('referencePrefix','UserController@referencePrefix')->name("user.prefix");
            
       
	});

});

Route::prefix('salesmanager')->namespace('Manager')->group(function () {

    Route::middleware('manager')->group(function () {

        Route::get('/', 'HomeController@index')->name('home');
        Route::get('/records', 'SalesTrackerController@records')->name('manager.records');
        /* ---------- Profile Crud ------------*/

        Route::get('/profile', 'ProfileController@index')->name('manager.profile');
        Route::post('/profile-save', 'ProfileController@save')->name('manager.profile.store');

        /* ---------- Sales Manager ------------*/

        Route::get('/sales-record-single/{userId}', 'SalesTrackerController@single');

        Route::get('/tracker-chart', 'SalesTrackerController@charts')->name("manager.tracker.chart");

        Route::post('/tracker-save', 'SalesTrackerController@save')->name('manager.tracker.store');

        /* ---------- User Crud ------------*/

        Route::match(['get','post'],'/user-list', 'UserController@index')->name('manager.user.trash');
        Route::get('/user-status', 'UserController@index')->name('manager.user.status');
        Route::match(['get','post'],'/user-create', 'UserController@create')->name('manager.user.create');
        Route::post('/user-save', 'UserController@store')->name('manager.user.store');
        Route::match(['get','post'],'/user-password', 'UserController@password')->name('manager.user.password');
        Route::get('/user-status', 'UserController@status')->name('manager.user.status');

    });

});

Route::prefix('salesperson')->namespace('SalesPerson')->group(function () {

    Route::middleware('salesperson')->group(function () {

        Route::get('/', 'HomeController@index')->name('salesperson.home');


        Route::match(['get','post'],'/records', 'TrackerController@records')->name('salesperson.records');

        Route::get('/record_manage', 'TrackerController@recordManage')->name('salesperson.record_manage');

        Route::post('/record_save', 'TrackerController@saveRcords')->name('salesperson.record_save');
        
        /* ---------- Profile Crud ------------*/

        Route::get('/profile', 'ProfileController@index')->name('salesperson.profile');

        Route::post('/profile-save', 'ProfileController@save')->name('salesperson.profile.store');

        Route::get('/salestracker', 'TrackerController@index')->name('tracker');

        Route::post('/tracker-save', 'TrackerController@save')->name('salesperson.tracker.store');

        Route::post('/update-list', 'TrackerController@updateList')->name('salesperson.record');

    });

}); 

Route::get('/businessLineSalesPerson', 'HomeController@businessLineSalesPerson')->name('businessLineSalesPerson');
Route::get('/salesManagerBusinessLineData', 'HomeController@salesManagerBusinessLineData')->name('salesManagerBusinessLineData');


Route::get('/salesmanagers', 'HomeController@salesmanagers')->name('salesmanagers');







Route::get('/businessLineSalesManagers', 'HomeController@businessLineSalesManagers')->name('businessLineSalesManagers');
Route::get('/salesPersonBusinessLineData','HomeController@salesPersonBusinessLineData')->name("salesPersonBusinessLineData");



Route::get('/salespersongroup', 'HomeController@salesperson')->name('salespersongroup');
Route::get('/reminderemail', 'HomeController@reminderemail')->name('reminderemail');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('postaction', 'LoginController@postaction')->name('postaction');
Route::get('login', 'LoginController@index')->name('login');
Route::get('/', 'LoginController@index');
Route::get('/logout', 'LoginController@logout');

Route::get('reset/{token}', 'LoginController@resetpassword');
Route::post('/resetaction/{otp}','LoginController@resetActionpassword')->name("resetaction.reset");



$router->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
$router->post('password/email', 'ForgotPasswordController@resetEmailLink')->name('password.email');
$router->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.token');



/*15645
|----------------------------------------------------------------------------------------------------------
*/
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize-clear', function() {
    $exitCode = Artisan::call('optimize:clear');
    return '<h1>Reoptimized class loader</h1>';
});
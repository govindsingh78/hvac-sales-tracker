

function columnChart(id,title,data){


	return myChart = Highcharts.chart(id, {

	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: title
	    },
	    plotOptions: {
	        series: {
	            cursor: 'pointer',
	            events: {
	                click: function () {
	                    alert('You just clicked the graph');
	                }
	            },
	        }
	    },
	    xAxis: {
	        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
	    },
	    yAxis: {
	        title: {
	            text: '£'
	        }
	    },
	    series: data

	});

}
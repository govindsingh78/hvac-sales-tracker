<div style="width:600px; margin:0px auto; display: block; border:1px dotted #ccc; color:#838383">
	<div style="display:block; padding:10px; border-bottom:1px dotted #ccc; background-image:url({{ asset('skin/frontend/images/logo.png') }}); background-repeat: no-repeat; background-size: 150px; background-position: 17px 11px; font-weight: 600; font-size:30px; text-align: center;     border-top: 5px solid #838383;font-family: times new roman;">{{ env('APP_NAME','Toshiba HVAC Sales Tracker')}}
	
	</div>

	<div style="display:block; padding:20px; border-bottom:1px dotted #ccc">

	Hi {!! $templateData['name'] !!}, 
	<br/><br/>
	Welcome to our community. <br>At "{{ env('APP_NAME','Toshiba HVAC Sales Tracker')}}" as a {!! $templateData['role'] !!}. <br>To Log in now and enjoy your new world of work click

	@if($templateData['type']==1)

		To reset Password Click the following link:-
		<a href='{{ URL("reset/".$templateData['otp']) }}'>Reset password</a>.  
	@elseif($templateData['type']==2)

		To reset Password Click the following link:-
		<a href='{{ URL("reset/".$templateData['otp']) }}'>Reset password</a>.

	@elseif($templateData['type']==3)

		To reset Password Click the following link:-
		<a href='{{ URL("reset/".$templateData['otp']) }}'>Reset password</a>.

	@endif
	 
	<br/><br/>
	Thank you. 
	<br/>
	<b>{{ env('APP_NAME','Toshiba HVAC Sales Tracker')}}</b><br/>

	</div>

	<div style="display:block; padding:20px; border-bottom:1px dotted #ccc; text-align:center">
		
			&copy; {{ date("Y") }} <a style="color:#f96688" href="{{ url('/') }}">{!! $templateData['sitename'] !!}</a> - All Rights Reserved. 
		<div style="clear: both; content: ''"></div>
	</div>

</div>
<div style="width:600px; margin:0px auto; display: block; border:1px dotted #ccc; font-size: 13px;color:#838383;font-family: arial;">
	<div style="display:block; padding:10px; border-bottom:1px dotted #ccc; background-image:url({{ asset('skin/frontend/images/logo.png') }}); background-repeat: no-repeat; background-size: 150px; background-position: 17px 11px; font-weight: 600; font-size:30px; text-align: center;     border-top: 5px solid #838383;font-family: times new roman;">{{ env('APP_NAME','Toshiba HVAC Sales Tracker')}}
	
	</div>

	<div style="display:block; padding:20px; border-bottom:1px dotted #ccc;">


		Dear {!! $templateData['name'] !!}, <br>

		<p>
		Please find below your monthly report:</p>

		
            
                <table style="text-align: left;color: #838383;font-size: 11px;" cellpadding="10" cellspacing="0" border="1" Bordercolor="#fafafa">
                    <thead>
                        <tr>
                            <th width="100">Sales Person</th>

                            <th>{{ date("Y")-1 }} Actual Value &pound;</th>
                            <th>{{ date("Y") }} Target Value &pound;</th>
                            <th>{{ date("Y") }} Actual Value &pound;</th>
                             <th>{{ date("Y") }} Growth Value &pound;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                <tbody>
                     @php $col1 = $col2 = $col3 = $col4 = $col5 = 0; @endphp

                    @foreach($templateData['datas'] as $child)
                    <tr>
                       

                        <td>{{ $child['name'] }}</td>

                       
                        <td>{{ number_format($child['prevActual']) }}</td>

                        <td>{{ number_format($child['currentTarget']) }}</td>

                        <td>{{ number_format($child['currentActual']) }}</td>

                        <td>{{ number_format($child['currentGrowth']) }}</td>

                        
                        <td><a href="{{ url('salesmanager/sales-record-single',$child['id']) }}" class="btn btn-sm btn-primary">View Details</a></td>
                    </tr>
                    @endforeach

                    </tbody>
                   
                </table>
               
             

		<br>
		<a href="{{ env('APP_URL','') }}">{{ env('APP_URL','') }}</a>

		<br>
		<br>
		Kind Regards<br>

		HVAC Tracker   
		<br>
	</div>

	<div style="display:block; padding:20px; border-bottom:1px dotted #ccc; text-align:center">
		
			&copy; {{ date("Y") }} <a style="color:#f96688" href="{{ url('/') }}">{!! $templateData['sitename'] !!}</a> - All Rights Reserved. 
		<div style="clear: both; content: ''"></div>
	</div>

</div>
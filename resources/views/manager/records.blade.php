@extends('layouts.app')
  <!-- Content -->
@section('content')
   
          <div class="container-fluid">

            <h4 class="title font-weight-bold py-3 mb-4">
              Records
             
            </h4>

            @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
            @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   

          @if(!empty($years))
          @foreach($years as $key=>$year)
            <div class="accordion" id="accordion{{ $key }}">
              <a data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="{{ $key == 0 ? 'true' : 'false' }}" aria-controls="collapse{{ $key }}" class="list-block">
                @if($key == 0)
                            <svg width="50" height="45" class="ficon">
                              <use xlink:href="#folder-open"></use>
                            </svg>
                @else
                      <svg width="50" height="45" class="ficon">
                        <use xlink:href="#folder-close"></use>
                      </svg>
                @endif
                {{ $year->uploaded_year }}
              </a>
              <div id="collapse{{ $key }}" class="collapse {{ $key==0 ? 'show' : null }} " aria-labelledby="heading{{ $key }}" data-parent="#accordion{{ $key }}">
                 <table>
                    @foreach($records as $key=>$record)
                       @if($year->uploaded_year == $record->uploaded_year)
                        <tr>
                            <td>---</td>
                            <td><a href="{{ url($record->data) }}"><img src="{{ asset('images/excel-icon.png') }}" width="40"> {{ $record->uploaded_year }} {{ ucfirst(date('M',mktime(0, 0, 0, $record->uploaded_month, 10))) }} </a></td>
                            <td><b>{{ $record->salesuser }}</b></td>  
                            <td>{{ $record->created_at }}</td>
                        </tr>
                      @endif
                    @endforeach
              </table>
                  </div>
                </div>
              @endforeach
            
          @else

            No Record Found

          @endif
         {{ Form::close() }}

        
@endsection
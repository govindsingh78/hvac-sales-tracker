@extends('layouts.app')

@section('content')



<script type="text/javascript" src="{{ asset('js/chart.js') }}"></script>


<div class="container">
@php $i =0; @endphp
	
	@if(!empty($recordList))
    @foreach($recordList as $record)

    <div class="form-group py-3 mt-3">
        <a class="title2" href="{{ url('salesmanager/sales-record-single',$record['id']) }}">{{ $record['person'] }}</a>
    </div>
    <div class="row justify-content-center">

        <div class="col-md-6">
            <div class="card">

                <div class="card-body">
                        <div id="sales{{ $i }}" style="width:100%; height:350px;"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                
                <div class="card-body">
                        <div id="sales{{ $i+1 }}" style="width:100%; height:350px;"></div>
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {

                setTimeout(function(){

                    columnChart('sales{{ $i }}','Sales By Month',{!! json_encode($record['salesActual']) !!});

                    columnChart('sales{{ $i+1 }}','Cumulative Sales By Month',{!! json_encode($record['salesYkd']) !!});

                },1000);
        });
    </script>

    @php $i = $i+2; @endphp
    @endforeach
	@else
	<label>Record Not Found</label>
	@endif
</div>


@endsection

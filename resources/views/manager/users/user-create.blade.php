@extends('layouts.app')
  <!-- Content -->
@section('content')

<div class="layout-content">



<?php

// print_r($data['parent']);
// die;

?>


  
  <div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-4">
      <span class="text-muted font-weight-light"><a href="{{ url('admin/users/user-list') }}">Users</a> /</span> Create User
    </h4>

    <div class="row">
      <div class="col-md-4">
        @if(!empty($data['record']) && !empty($data['record']->image))
        <img src="{{ asset($data['record']->image) }}" class="img-fluid">
        @else
        <img src="{{ asset('images/user-dummy.png') }}" class="img-fluid">
        @endif
      </div>
      <div class="card mb-4 offset-md-2 col-md-6">

        

         @if (\Session::has('success'))
          <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ \Session::get('success') }}
          </div>
          @endif
        @if (\Session::has('error'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ \Session::get('error') }}
          </div>
        @endif   



        
          {{ Form::open(['route'=>'manager.user.store','files'=> true]) }}
            <div class="card-body">
              <div class="form-group">

                @if(!empty($data['record']) && !empty($data['record']->image))

                  {{ Form::hidden('prev_image',$data['record']->image) }}
              
                @endif

                @if(!empty($data['record']))

                {{ Form::hidden('id',base64_encode($data['record']->id),array('id'=>"user_id")) }}

                @endif


                
              </div>

              <div class="form-group">

                {{ Form::label('Username','Username',array('class'=>"form-label")) }}

                @if(!empty($data['record']) && !empty($data['record']->username))

                {{ Form::text('feild[username]',$data['record']->username,array('class'=>"form-control")) }}

                @else

                {{ Form::text('feild[username]','',array('class'=>"form-control")) }}

                @endif
                @if ($errors->has('feild.username'))
                    <span class="text-danger">
                        <strong>This username already taken.</strong>
                    </span>
                @endif
              </div>

              
              <div class="form-group">

                {{ Form::label('email','E-mail',array('class'=>"form-label")) }}

                @if(!empty($data['record']) && !empty($data['record']->email))

                {{ Form::text('feild[email]',$data['record']->email,array('class'=>"form-control")) }}

                @else

                {{ Form::text('feild[email]','',array('class'=>"form-control")) }}

                @endif
                @if ($errors->has('feild.email'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.email') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group">

                {{ Form::label('firstname','First Name',array('class'=>"form-label")) }}

                @if(!empty($data['record']) && !empty($data['record']->firstname))

                {{ Form::text('feild[firstname]',$data['record']->firstname,array('class'=>"form-control")) }}

                @else

                {{ Form::text('feild[firstname]','',array('class'=>"form-control")) }}

                @endif
                @if ($errors->has('feild.firstname'))
                    <span class="text-danger">
                        <strong>First Name required</strong>
                    </span>
                @endif
              </div>

              <div class="form-group">

                {{ Form::label('lastname','Last Name',array('class'=>"form-label")) }}

                @if(!empty($data['record']) && !empty($data['record']->lastname))

                {{ Form::text('feild[lastname]',$data['record']->lastname,array('class'=>"form-control")) }}

                @else

                {{ Form::text('feild[lastname]','',array('class'=>"form-control")) }}

                @endif
                @if ($errors->has('feild.lastname'))
                    <span class="text-danger">
                        <strong>Last Name required</strong>
                    </span>
                @endif
              </div>

              

              <div class="form-group">

                {{ Form::label('role','Role',array('class'=>"form-label")) }}
                {{ Form::select('feild[role]',$roles,(!empty($data['record']) && !empty($data['record']->role) ? $data['record']->role : ''),array('class'=>"form-control","id"=>"role")) }}

                @if ($errors->has('feild.role'))
                    <span class="text-danger">
                        <strong>Role of the user required</strong>
                    </span>
                @endif
              </div>

                
                <input type="hidden" id="salesManagerId" name="feild[parent]" value="{{ Auth::user()->id }}">
                

              <!-- <div class="salesManager">
                @if(!empty($data['record']) && !empty($data['record']->role) && $data['record']->role == 3)
                  <input type="hidden" name="feild[parent]" value="{{ Auth::user()->id }}">
                @endif
              </div>
              <div class="form-group">

                {{ Form::label('business','Business Line',array('class'=>"form-label")) }}
                {{ Form::text('feild[business]',(!empty($data['record']) && !empty($data['record']->business) ? $data['record']->business : Auth::user()->firstname.' '.Auth::user()->lastname),array('class'=>"form-control")) }}

                @if ($errors->has('feild.business'))
                    <span class="text-danger">
                        <strong>Business Line required</strong>
                    </span>
                @endif
              </div> -->


              <div class="salesManager">
                @if(!empty($data['record']) && !empty($data['record']->role) && $data['record']->role == 3)
               
                  
                  
                  <div class="form-group">
                    {{ Form::label('salesmanager','Sales Manager',array('class'=>"form-label")) }}
                    
                    {{ Form::select('feild[parent]',$data['parent'],old('feild[parent]',$data['record']->parent),array("class"=>"form-control manager", "id"=>"parent_id")) }}
                  
                    <?php //die;?>
                  
                  </div>
                @endif
              </div>
 
              
              <div class="form-group" id="salesPersonBusinessLineData">

               
              </div>
              <div class="form-group">
                    @error('salespersonbuisnessline[]')
                    <div class="alert alert-danger">Business line required to choose !</div>
                    @enderror
                </div>

             
             

              @if(empty($data['record']))
              <div class="form-group">

                {{ Form::label('password','Password',array('class'=>"form-label")) }}
                {{ Form::text('feild[password]','',array('class'=>"form-control")) }}

                @if ($errors->has('feild.password'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.password') }}</strong>
                    </span>
                @endif
              </div>
              @endif

              @if(!empty($data['record']))
              <div class="form-group">

                {{ Form::label('status','Status.',array('class'=>"form-label")) }}
                {{ Form::select('feild[status]',['0'=>'In active','1'=>'Active'],$data['record']->status,array('class'=>"form-control")) }}

                @if ($errors->has('feild.password'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.password') }}</strong>
                    </span>
                @endif
              </div>

              @else
              {{ Form::hidden('feild[status]',1) }}
              @endif

              <div class="form-group">

                {{ Form::label('image','Display Image',array('class'=>"form-label")) }}
                {{ Form::file('image',array('class'=>"form-control")) }}

                @if ($errors->has('image'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                @endif

              </div>

               @if(!empty($data['record']) && !empty($data['record']->image))
                <div class="form-group mb-0">
                        {{ Form::checkbox('removeImage',1,0,array("id"=>'removeImage')) }}
                    
                      {{ Form::label('removeImage','Remove Image',array('class'=>"form-label")) }}
                   
                </div>
              @endif

              <div class="text-right">

                <button type="submit" class="btn btn-primary">Submit</button>

              </div>
            </div>
            
           
          {{ Form::close() }}
            

        </div>
      </div>
    </div>
    </div>
  </div>
@if(empty($data['record']))
<script type="text/javascript">

$(document).ready(function(){

       var prevKey = "";
  window.addEventListener('beforeunload', function (e) { 

          if (e.key=="F5") {
                      window.onbeforeunload = ConfirmLeave;
                  }
                  else if (e.key.toUpperCase() == "W" && prevKey == "CONTROL") {                
                      window.onbeforeunload = ConfirmLeave;   
                  }
                  else if (e.key.toUpperCase() == "R" && prevKey == "CONTROL") {
                      window.onbeforeunload = ConfirmLeave;
                  }
                  else if (e.key.toUpperCase() == "F4" && (prevKey == "ALT" || prevKey == "CONTROL")) {
                      window.onbeforeunload = ConfirmLeave;
                  }
                  prevKey = e.key.toUpperCase();


        }); 

         $("a").each(function(e)
          {

            $(this).click(function()
            {
              var r = confirm("New user has not been SAVED!");

                if (r == true) {
                  
                  return true;

                } else {

                  return false;

                }

            });
          });

       });
</script>

@endif

  <script type="text/javascript">

    


    $(document).ready(function(){

        $("#role").change(function(){
          
            var roleId = $(this).val();

            if(roleId == 3){

              var InstCont = $(".salesManager");

              $(".salesManager").html('<input type="hidden" name="feild[parent]" value="{{ Auth::user()->id }}">');

            }else{

              $(".salesManager").html('');
            }

        });



        // New Script Starts

        $("#salesPersonBusinessLineData").html('');
        jQuery.ajax({
                url: "{{ route('salesPersonBusinessLineData') }}",
                method: 'get',
                data: {
                   id: $("#salesManagerId").val(),
                   user_id: $("#user_id").val()
                },
                success: function(data){
                  console.log(data);
                  $("#salesPersonBusinessLineData").html(data);
                }
                  
          });




    });
  </script>
@endsection
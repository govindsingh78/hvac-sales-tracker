@extends('layouts.app')

@section('content')

<!-- <div class="container-fluid">

    <h2>[Sales Manager - Portal]</h2>

</div> -->
<div class="container">
    <div class="justify-content-center">
           @php $i = 0; @endphp
            @foreach($datas as $data)
              
            
                <div class="form-group pl-3 pr-3 py-3 mt-3" style="background-color: #ddd; padding: 10px">
                    <a href="{{ route('manager.tracker.chart',$data->id) }}" class="title2">{{ $data->businessline_name }}</a>
                    <a href="javascript:void(0)" class="title2 pull-right" data-toggle="collapse" data-target="#show{{ $data->businessline_id }}" ><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                </div>
                <div id="show{{ $data->businessline_id }}" class="collapse <?php if($i == 0){echo 'show';}?>">
                
                <table class="table table-bordered dashboard" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Reference No</th>
                            <th>Sales Person</th>

                            <th>{{ date("Y") }} Target Value $</th>
                            <th>{{ date("Y") }} Target Value &pound;</th>

                            <th>{{ date("Y") }} Actual &pound;</th>
                            <th>YTD v TARGET</th>
                            <th>Growth V Last YTD</th>
                            <th>Years in Business / Role</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                <tbody>
                     @php $col1 = $col2 = $col3 = $col4 = $col5 = 0; @endphp
                    @foreach($data->fatchData as $child)
                    <tr>
                        @php 
                            $reco = helper::growthVLast($data->id,$child->id);

                            $ytdTarget = helper::ytdVTarget($data->id,$child->id);

                            $col1 += ($settings->value*$reco[1]);
                            $col2 += $reco[1];
                            $col3 += $ytdTarget;
                            $col4 += $reco[0];
                            $col5 += $reco[3];

                        @endphp
                        <td data-order="{{ $child->sorting }}">{{ $child->role==3 ? helper::referencePrefix($child->parent).'-' : null }}{{ $child->sorting }}</td>
                        <td>{{ $child->firstname.' '.$child->lastname }}</td>

                        <td>{{ !empty($reco[1]) ? number_format(round($settings->value*$reco[1])) : 0 }}</td>
                        <td>{{ !empty($reco[1]) ? number_format(round($reco[1])) : 0 }}</td>
                        <td>{{ !empty($reco[3]) ? number_format(round($reco[3])) : 0 }}</td>
                        <td align="right">
                            <span style="width:{{ str_replace('-','',$ytdTarget) }}%" class="bgPercent {{ !empty($ytdTarget) && $ytdTarget > 0 ? 'success' : 'danger' }}"></span>
                            <span class="label-text">{{ !empty($ytdTarget) ? round($ytdTarget) : 0 }}%</span>

                        </td>

                        <td align="right">
                            <span style="width:{{ str_replace('-','',$reco[0]) }}%" class="bgPercent {{ !empty($reco[0]) && $reco[0] > 0 ? 'success' : 'danger' }}"></span><span class="label-text">{{ !empty($reco[0]) ? round($reco[0]) : 0 }}%</span>
                        </td>
                        <td>
                            {{ $child->businessrole }}
                        </td>
                        <td><a href="{{ url('salesmanager/sales-record-single',$child->id) }}" class="btn btn-sm btn-primary">View Details</a></td>
                    </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                        <tr>
                             
                            <th colspan="2">Total</th>
                            <th>{{ number_format(round($col1)) }}</th>
                            <th>{{ number_format(round($col2)) }}</th>
                            <th>{{ number_format($col5) }}</th>
                            <!-- <th style="text-align: right;">{{-- sizeof($data->fatchData) > 0 ? round($col3/sizeof($data->fatchData)) : 0 --}}</th>
                            <th style="text-align: right;">{{-- sizeof($data->fatchData) > 0 ? round($col4/sizeof($data->fatchData)) : 0 --}}</th>
                            <th>&nbsp;</th> -->
                            <th  colspan="4">&nbsp;</th>
                        </tr>
                    </tfoot>
                </table><hr>
               </div>
               @php $i++; @endphp
                
            @endforeach
        </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                  oTable = $('.table').dataTable({
                      "iDisplayLength": -1,
                      "bLengthChange":false,
                      "bPaginate": false,
                      "pageLength":20,
                      "bInfo": false,
                      "bFilter": false,
                      'columnDefs': [ {

                        'targets': [1,2,3,4,5,6,7,8], /* column index */

                        'orderable': false, /* true or false */

                     }] 
                  });

                  oTable.fnSort( [ [0,'asc'] ] );

            })
        </script>
    </div>
</div>
@endsection

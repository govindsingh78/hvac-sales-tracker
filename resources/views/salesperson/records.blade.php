@extends('layouts.app')
  <!-- Content -->
@section('content')

          <div class="container-fluid">
            {{ Form::open(['route'=>'salesperson.records','onsubmit'=>"return show_alert()"]) }}
            <h4 class="title font-weight-bold py-3 mb-4">
              Records
              <button type="button" onclick="window.location.href='{{ route("salesperson.record_manage") }}'" class="btn btn-primary btn-round pull-right"><span class="ion ion-md-add"></span>&nbsp; Create Record</button>

            </h4>

            @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
            @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   

         
  

          @foreach($years as $key=>$year)
             <div class="accordion" id="accordion{{ $key }}">
              <a data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="{{ $key == 0 ? 'true' : 'false' }}" aria-controls="collapse{{ $key }}" class="list-block">
                @if($key == 0)
                            <svg width="50" height="45" class="ficon">
                              <use xlink:href="#folder-open"></use>
                            </svg>
                @else
                      <svg width="50" height="45" class="ficon">
                        <use xlink:href="#folder-close"></use>
                      </svg>
                @endif
                            {{ $year->uploaded_year }}
              </a>
              <div id="collapse{{ $key }}" class="collapse {{ $key==0 ? 'show' : null }} " aria-labelledby="heading{{ $key }}" data-parent="#accordion{{ $key }}">
                 <table>

                  @foreach($records as $keys=>$record)

                      @if($year->uploaded_year == $record->uploaded_year)

                        <tr>
                            <td>---</td>
                            <td><a href="{{ url($record->data) }}"><img src="{{ asset('images/excel-icon.png') }}" width="40"> {{ $record->uploaded_year }} {{ ucfirst(date('M',mktime(0, 0, 0, $record->uploaded_month, 10))) }}  </a></td>
                            <td>{{ $record->created_at }}</td>
                            
                            <td><a href="{{ route('salesperson.record_manage',['id'=>$record->id]) }}" class="btn btn-primary">Edit</a> <a href="{{ route('salesperson.records',['id'=>$record->id]) }}" class="btn btn-danger">Delete</a></td>
                        </tr>
                       
                        @endif
                    @endforeach
                     </table>
                  </div>
                </div>
              @endforeach
            
         
         {{ Form::close() }}

         
   
@endsection
@extends('layouts.app')

@section('content')

<!-- <div class="container-fluid mb-4">

    <h2 class="mb-5">[Sales Person - Portal]</h2>

</div>
 -->

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-6">
            <div class="card">

                <div class="card-body">
                        <div id="sales1" style="width:100%; height:350px;"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                
                <div class="card-body">
                        <div id="sales2" style="width:100%; height:350px;"></div>
                </div>
            </div>
        </div>

    </div>
</div>
 <div class="container py-5">

        <label class="title2 pb-2">Monthly Sales Figures</label>

        {{ Form::open(['route'=>'salesperson.tracker.store','files'=> true]) }}

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th>Actual  {{ date("Y") - 1 }}</th>
                    <th>Target  {{ date("Y") }}</th>
                    <th>Actual {{ date("Y") }}</th>
                    <th>Cumulative {{ date("Y")-1 }}</th>
                    <th>Cumulative Target {{ date("Y") }}</th>
                    <th>Cumulative Actual {{ date("Y") }}</th>
                    <th>Growth YTD Current v Last</th>
                    <th>Backlog</th>
                </tr>
            </thead>
            <tbody>
                @php $i = 0; @endphp
                @foreach($datas as $key => $data)
                     @if($data->year == date("Y"))   
                        @php $target[] = $data->tarrget; $actual[] = $data->actual; $targetykd[] = $data->targetykd; 
                        $ykd[] = $data->ykd; $growthykd[] = $data->growthykd; $backlog[] = $data->backlog; @endphp
                        <tr>
                            <th>{{ date('M', mktime(0, 0, 0, $data->month , 10)) }}</th>
                            <td>{{ isset($actualPevious[$i]) ? round($actualPevious[$i]) : 0 }}</td>
                            <td>{{ helper::numformat($data->tarrget) }}</td>

                            <td>{{ Form::hidden('id[]',$data->id,array("class"=>"customeId")) }} {{ Form::text('actualSales[]',round($data->actual),array("class"=>"form-control actual currentRs","rel"=>"actualSales")) }}</td>

                            <td>
                                {{ Form::text('previousytd[]',isset($comPevious[$i]) ? round($comPevious[$i]) : 0,array("class"=>"form-control actual previousytd","readonly"=>true)) }}
                            </td>
                            <td>{{ ($data->year == Date("Y") ? round($data->targetykd) : null)  }}</td>

                            <td>{{ Form::text('ytd[]',($data->year == Date("Y") ? round($data->ykd) : null),array("class"=>"form-control actual ytd","rel"=>"actualSales","readonly"=>true)) }}
                            </td>

                            <td>{{ Form::text('growthykd[]',round($data->growthykd),array("class"=>"form-control actual growthykd","readonly"=>true)) }}</td>

                            <td>{{ Form::text('backlog[]',round($data->backlog),array("class"=>"form-control actual","rel"=>"backlog")) }}</td>
                        </tr>
                        @php $i = $i+1; @endphp
                   @endif
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Total</th>
                    <th>{{ number_format(round(array_sum($actualPevious))) }}</th>
                    <th>{{ (helper::numformat(array_sum($target))) }}</th>
                    <th style="text-align: center;">{{ number_format(round(array_sum($actual))) }}</th>
                    <th>{{-- number_format(round(array_sum($comPevious))) --}}</th>
                    <th>{{-- number_format(round(array_sum($targetykd))) --}}</th>
                    <th>{{-- number_format(round(array_sum($ykd))) --}}</th>
                    <th>{{-- number_format(round(array_sum($growthykd))) --}}</th>
                    <th style="text-align: center;">{{ number_format(round(array_sum($backlog))) }}</th>
                </tr>
            </tfoot>
        </table>

        <div class="form-group text-center">
            {{ Form::submit("Update Records",array("class"=>"btn btn-danger")) }}
        </div>

        {{ Form::close() }}

    </div>





<script type="text/javascript" src="{{ asset('js/chart.js') }}"></script>


<script type="text/javascript">

    document.addEventListener('DOMContentLoaded', function () {

        setTimeout(function(){

            columnChart('sales1','Sales By Month',{!! json_encode($salesActual) !!});

            columnChart('sales2','Cumulative Sales By Month',{!! json_encode($salesYkd) !!});

        },1000);

        $(".actual").blur(function(){
            id = ($(this).closest("tr").find(".customeId").val());
            val = $(this).val();


            var activePoint = $('.currentRs').index(this);

            var actualR = 0;
            
            $(".currentRs").each(function( index ) {

                actualR += parseFloat($( this ).val());

                var currentRs = $( this ).val();

                if(index >= activePoint ){

                    $(".ytd:eq( "+index+" )").val(actualR);

                    growth = actualR - $(".previousytd:eq( "+index+" )").val();


                    $(".growthykd:eq( "+index+" )").val(growth);

                    //console.log( index + ": " + $( this ).val() + ": " + $(".ytd:eq( "+index+" )").val());

                }

            });

           /* type = $(this).attr("rel");

            if((val!="") && (id != "")){
               jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('input[name="_token"]').val()
                  }
              });
               jQuery.ajax({
                  url: "{{ route('salesperson.record') }}",
                  method: 'post',
                  data: {
                     records: val,
                     id: id,
                     type: type 
                  },
                  success: function(data){

                    

                  }
                    
                  });
             }*/

        });

    });
</script>

@endsection

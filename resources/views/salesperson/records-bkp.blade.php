@extends('layouts.app')
  <!-- Content -->
@section('content')

        
          <div class="container-fluid">
            {{ Form::open(['route'=>'salesperson.records','onsubmit'=>"return show_alert()"]) }}
            <h4 class="title font-weight-bold py-3 mb-4">
              Records
              <button type="button" onclick="window.location.href='{{ route("salesperson.record_manage") }}'" class="btn btn-primary btn-round pull-right"><span class="ion ion-md-add"></span>&nbsp; Create Record</button>

            </h4>

            @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
            @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   


            <table class="table" id="table">
               <thead>
                  <tr>
                     <th>Sr No</th>
                     <th>Data</th>
                     <th>Year</th>
                     <th>Month</th>
                     <th>Created at</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                @foreach($records as $key=>$record)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td><a href="{{ url($record->data) }}"><img src="{{ asset('images/excel-icon.png') }}" width="40"></a></td>
                        <td>{{ $record->uploaded_year }}</td>
                        <td>{{ ucfirst(date('M',mktime(0, 0, 0, $record->uploaded_month, 10))) }}</td>
                        <td>{{ $record->created_at }}</td>
                        <td><a href="{{ route('salesperson.record_manage',['id'=>$record->id]) }}" class="btn btn-primary">Edit</a> <a href="{{ route('salesperson.records',['id'=>$record->id]) }}" class="btn btn-danger">Delete</a></td>
                    </tr>
                @endforeach
               </tbody>
            </table>
         </div>

         {{ Form::close() }}
       <script>
         $(function() {
               $('#table').DataTable();
         });
         </script>
   
@endsection
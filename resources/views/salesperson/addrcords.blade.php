@extends('layouts.app')
  <!-- Content -->
@section('content')

         <div class="container-fluid">

            <fieldset>
              <legend>Upload Report</legend>
              {{ Form::open(['route'=>'salesperson.record_save','files'=> true]) }}
                {{ Form::hidden('id',!empty($data) ? $data->id : null) }}
                <div class="form-group row">
                    <div class="col-md-3">
                      <label>Select File</label>
                      <input type="file" class="form-control" name="file">

                      @if(!empty($data))
                        <input type="hidden" name="prev_file" value="{{ $data->file }}">
                      @endif
                      @if ($errors->has('file'))
                          <span class="text-danger">
                              <strong>{{ $errors->first('file') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="col-md-3">

                      <label>Select Year</label>
                      <select class="form-control" name="year">
                        @php $year = date('Y'); @endphp
                        @for($i=$year-5;$i<=$year;$i++)
                          <option {{ !empty($data) && $data->uploaded_year == $i ? 'selected' : null }} value="{{ $i }}">{{ $i }}</option>
                        @endfor
                      </select>
                      @if ($errors->has('year'))
                          <span class="text-danger">
                              <strong>{{ $errors->first('year') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="col-md-3">
                      <label>Select Month</label>
                      <select class="form-control" name="month">
                        @for($i=1;$i<=12;$i++)

                          <option {{ !empty($data) && $data->uploaded_month == $i ? 'selected' : null }} value="{{ $i }}">{{ ucfirst(date('M',mktime(0, 0, 0, $i, 10))) }}</option>
                        @endfor
                      </select>
                      @if ($errors->has('month'))
                          <span class="text-danger">
                              <strong>{{ $errors->first('month') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="col-md-3">
                        <label>&nbsp;</label><br>
                        {{ Form::submit("Upload Data",array("class"=>"btn btn-primary")) }}
                    </div>
                </div>
              {{ Form::close() }}
            </fieldset>
          </div>

@endsection
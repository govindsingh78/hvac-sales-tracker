
<html lang="en">
<head>
    <title>Laravel DataTables Tutorial Example</title>

        <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        
</head>
      <body>
         <div class="container">
               <h2>Laravel DataTables Tutorial Example</h2>
            <table class="table table-bordered" id="table">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Email</th>
                  </tr>
               </thead>
               <tbody>
                    <tr>
                        <td>1</td>
                         <td>test</td>
                          <td>tst@gmail.com</td>
                    </tr>
                    <tr>
                        <td>1</td>
                         <td>Mukul</td>
                          <td>mukul@gmail.com</td>
                    </tr>
               </tbody>
            </table>
         </div>
       <script>
         $(function() {
               $('#table').DataTable();
         });
         </script>
   </body>
</html>
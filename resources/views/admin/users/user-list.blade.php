@extends('layouts.app')
  <!-- Content -->
@section('content')

<!-- Layout content -->
        <div class="layout-content">

          <!-- Content -->
          <div class="container-fluid flex-grow-1 container-p-y">

          	{{ Form::open(['url'=>'admin/user-list','onsubmit'=>"return show_alert()"]) }}
            <h4 class="title font-weight-bold py-3 mb-4">
              Users
              <button type="button" onclick="window.location.href='{{ url('admin/user-create') }}'" class="btn btn-primary btn-round pull-right"><span class="ion ion-md-add"></span>&nbsp; Create User</button>

              <button type="submit" class="btn btn-danger btn-round mr-2 pull-right"><span class="ion ion-ios-trash"></span>&nbsp; Delete Users</button>
            </h4>

            @if (\Session::has('success'))
	          <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            {{ \Session::get('success') }}
	          </div>
	          @endif
	        @if (\Session::has('error'))
	        <div class="alert alert-danger">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            {{ \Session::get('error') }}
	          </div>
	        @endif   


                <table id="table" class="table table-striped">
                  <thead>
                    <tr>
                    	<th></th>
                    	<th></th>
                      <th>Reference No</th>
                      <th>Full Name</th>
                      <th>E-mail</th>
                      <th>Sales Manager</th>
                      <th>Role</th>
                      <th>Status</th>
                      <th width="200px;">Action</th>

                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($data['list'] as $list)
                  	<tr>
                  		<td><input type="checkbox" name="ids[]" value="{{ $list->id }}"></td>
                  		<td>@if(!empty($list->image))
        					        <img src="{{ asset($list->image) }}" class="img-fluid" width="50">
        					        @else
        					        <img src="{{ asset('images/user-dummy.png') }}" class="img-fluid" width="50">
        					        @endif
                      </td>
                      <td>{{ $list->role==2 || $list->role==3 ? !empty($list->parent) ? helper::referencePrefix($list->parent).'-' 
                      : helper::referencePrefix($list->id).'-' : null }}{{ $list->sorting }}</td>
                  		<td>{{ $list->firstname.' '.$list->lastname }}</td>

                  		<td>{{ $list->email }}</td>
                  		<td> 
                      {{ ($list->role==3) ? helper::getSalesPersonManager($list->parent)  
                      : "N/A" }} 
                           
                      
                      </td>
                  		<td>{{ $list->userrole }}</td>
                      <td>@if($list->status==0)
                         <a href="{{ route('user.status',array('id'=>base64_encode($list->id))) }}" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash"></i></a>
                        @else
                         <a href="{{ route('user.status',array('id'=>base64_encode($list->id))) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                        @endif
                      </td>
                      <td>


                        <a data-toggle="modal" id="{{ base64_encode($list->id) }}" href="#password" class="btn btn-warning btn-sm"><i class="fa fa-lock"></i></a>
                  		  <a href="{{ route('user.create',array('id'=>base64_encode($list->id))) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>

                        <a onclick="deletePrompt(this)" link="{{ route('admin.user.trash',array('id'=>base64_encode($list->id))) }}" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>

                        <a href="{{ route('user.passwordsend',array('id'=>base64_encode($list->id))) }}" class="btn btn-primary btn-sm"><i class="fa fa-envelope"></i></a>
                      </td>
                  	</tr>
                  	@endforeach
                  </tbody>



                </table>

            {{ Form::close() }}
          </div>

<div class="modal fade" id="password" role="dialog">

  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

          {{ Form::open(['route'=>'user.password','file'=>true]) }}
              {{ Form::hidden("id","",array("class"=>"id_req")) }}
            <div class="modal-body">
              <div class="alert alert-success" style="display:none"></div>
              <div class="alert alert-danger" style="display:none"></div>
              <div class="form-group"> 
                {{ Form::label("password","New Password",array("class"=>"label-control")) }}
                {{ Form::password("password",array("class"=>"form-control","id"=>"new_password")) }}
              </div>
              <div class="form-group mb-0"> 
                {{ Form::label("password","Confirm Password",array("class"=>"label-control")) }}
                {{ Form::password("cpassword",array("class"=>"form-control","id"=>"cunfirm_password")) }}
              </div>
            </div>
            <div class="modal-footer">
              {{ Form::button("Change Password", array("class"=>"btn btn-primary","id"=>"submit")) }}
              <button type="button"  class="btn btn-default" data-dismiss="modal">Cancel</button>
              
            </div>
          {{ Form::close() }}
      
    </div>
    
  </div>
</div>

<script type="text/javascript">

         jQuery(document).ready(function(){

          jQuery(document).on("click","a[href='#password']",function(){

            console.log($(this).attr("id"));

            $(".id_req").val($(this).attr("id"));
          });
            jQuery('#submit').click(function(e){
               e.preventDefault();

               if((jQuery('#new_password').val()!="") && (jQuery('#new_password').val() == jQuery('#cunfirm_password').val())){
                   jQuery.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('input[name="_token"]').val()
                      }
                  });
                   jQuery.ajax({
                      url: "{{ route('user.password') }}",
                      method: 'post',
                      data: {
                         password: jQuery('#new_password').val(),
                         cpassword: jQuery('#cunfirm_password').val(),
                         id: jQuery('.id_req').val()
                      },
                      success: function(data){

                        jQuery('.alert-success').show();
                        jQuery('.alert-success').append('Password Updated Successfully');
                        setTimeout(function(){ window.location.href='{{ url("admin/user-list") }}' },3000);
                      }
                        
                      });
                 }else{
                    jQuery('.alert-danger').show();
                    jQuery('.alert-danger').append('Password Not Match');
                 }
              });
                
            });
</script>
@endsection
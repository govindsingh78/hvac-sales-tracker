@extends('layouts.app')
  <!-- Content -->
@section('content')

<div class="layout-content">
 
   
  <div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-4">
      <span class="text-muted font-weight-light"><a href="{{ url('admin/user-list') }}">Users</a> /</span> 
<?php if(empty($_REQUEST['id'])){   echo "Create User"; }else{ echo "Update User"; }?>
    </h4>

    <div class="row">
      <div class="col-md-4">
        @if(!empty($data['record']) && !empty($data['record']->image))
        <img src="{{ asset($data['record']->image) }}" class="img-fluid">
        @else
        <img src="{{ asset('images/user-dummy.png') }}" class="img-fluid">
        @endif
      </div>
      <div class="card mb-4 offset-md-2 col-md-6">

       

        


        
          {{ Form::open(['route'=>'user.store','files'=> true,"id"=>"userContainer"]) }}
            <div class="card-body">

                @if (\Session::has('success'))
                  <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ \Session::get('success') }}
                  </div>
                  @endif
                @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ \Session::get('error') }}
                  </div>
                @endif  


              <div class="form-group">

                @if(!empty($data['record']) && !empty($data['record']->image))

                  {{ Form::hidden('prev_image',$data['record']->image) }}
              
                @endif

                @if(!empty($data['record']))

                {{ Form::hidden('id',base64_encode($data['record']->id),array('id'=>"user_id")) }}

                @endif
              </div>

              <div class="form-group">

                {{ Form::label('Username','Username',array('class'=>"form-label")) }}

                @if(!empty($data['record']) && !empty($data['record']->username))

                {{ Form::text('feild[username]',$data['record']->username,array('class'=>"form-control")) }}

                @else

                {{ Form::text('feild[username]','',array('class'=>"form-control")) }}

                @endif
                
                @if ($errors->has('feild.username'))
                    <span class="text-danger">
                        <strong>This username already taken.</strong>
                    </span>
                @endif
              </div>

              <div class="form-group row">
                <div class="col-md-3">
                    {{ Form::label('Sorting','&nbsp;',array('class'=>"form-label")) }}

                   

                    @if(!empty($data['record']->parent))
                    <label class="form-control prefix">{{ helper::referencePrefix($data['record']->parent) }}</label>
                    @elseif(!empty($data['record']->id))
                     
                    <label class="form-control prefix">{{ helper::referencePrefix($data['record']->id) }}</label>
                    @else
                    <label class="form-control prefix">&nbsp;</label>
                    @endif
                </div> 
                <div class="col-md-9">

                   
                    {{ Form::label('Sorting','Reference No',array('class'=>"form-label")) }}

                    @if(!empty($data['record']) && !empty($data['record']->sorting))

                    {{ Form::number('feild[sorting]',$data['record']->sorting,array('class'=>"form-control")) }}

                    @else

                    {{ Form::number('feild[sorting]','',array('class'=>"form-control")) }}

                    @endif

                  @if ($errors->has('feild.sorting'))
                  <span class="text-danger">
                    <strong>{{ $errors->first('feild.sorting') }}</strong>
                  </span>
                  @endif
                </div>
              </div>


              <div class="form-group">

                {{ Form::label('email','E-mail',array('class'=>"form-label")) }}

                @if(!empty($data['record']) && !empty($data['record']->email))

                {{ Form::text('feild[email]',$data['record']->email,array('class'=>"form-control")) }}

                @else

                {{ Form::text('feild[email]','',array('class'=>"form-control")) }}

                @endif
                @if ($errors->has('feild.email'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.email') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group">

                {{ Form::label('firstname','First Name',array('class'=>"form-label")) }}

                @if(!empty($data['record']) && !empty($data['record']->firstname))

                {{ Form::text('feild[firstname]',$data['record']->firstname,array('class'=>"form-control")) }}

                @else

                {{ Form::text('feild[firstname]','',array('class'=>"form-control")) }}

                @endif
                @if ($errors->has('feild.firstname'))
                    <span class="text-danger">
                        <strong>First Name required</strong>
                    </span>
                @endif
              </div>
              <input type="hidden" name="roleChnaged" id="roleChanged" value="0">
              <div class="form-group">

                {{ Form::label('lastname','Last Name',array('class'=>"form-label")) }}

                @if(!empty($data['record']) && !empty($data['record']->lastname))

                {{ Form::text('feild[lastname]',$data['record']->lastname,array('class'=>"form-control")) }}

                @else

                {{ Form::text('feild[lastname]','',array('class'=>"form-control")) }}

                @endif
                @if ($errors->has('feild.lastname'))
                    <span class="text-danger">
                      <strong>Last Name required</strong>
                    </span>
                @endif
              </div>

              

              <div class="form-group">

                {{ Form::label('role','Role',array('class'=>"form-label")) }}
                 
                @if(!empty($data['record']) && !empty($data['record']->role))
                    {{ Form::select('feild[role]',$roles, $data['record']->role ,array('class'=>"form-control role","id"=>"role")) }}
                @else

                    <script>
                      $(document).ready(function(){
                          $("#role1").val($("#role1 option:first").val());
                      });
                    </script>

                    {{ Form::select('feild[role]',$roles, null,array('class'=>"form-control role","id"=>"role1")) }}

                @endif
                <?php //print_r($errors);?>

                @if ($errors->has('feild.role'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.role') }}</strong>
                    </span>
                @endif
              </div>

              <div id="businessLineData">
                
              </div>

              <div class="form-group" id="salesPersonBusinessLineData">


              </div>

              

              <div class="salesManager">
                @if(!empty($data['record']) && !empty($data['record']->role) && $data['record']->role == 3)
                  <div class="form-group">
                    {{ Form::label('salesmanager','Sales Manager',array('class'=>"form-label")) }}
                    
                    {{ Form::select('feild[parent]',$parent,old('feild[parent]',$data['record']->parent),array("class"=>"form-control manager", "id"=>"parent_id")) }}
                  </div>
                @endif
              </div>

              
              <div class="form-group" id="salesPersonManager">


              </div>
            

             
             

              @if(empty($data['record']))
              <div class="form-group">

                {{ Form::label('password','Password',array('class'=>"form-label")) }}
                {{ Form::text('feild[password]','',array('class'=>"form-control")) }}

                @if ($errors->has('feild.password'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.password') }}</strong>
                    </span>
                @endif
              </div>
              @endif

              @if(!empty($data['record']))
              <div class="form-group">

                {{ Form::label('status','Status.',array('class'=>"form-label")) }}
                {{ Form::select('feild[status]',['0'=>'In active','1'=>'Active'],$data['record']->status,array('class'=>"form-control")) }}

               
              </div>

              @else
              {{ Form::hidden('feild[status]',1) }}
              @endif

              <div class="form-group">

                {{ Form::label('image','Display Image',array('class'=>"form-label")) }}
                {{ Form::file('image',array('class'=>"form-control")) }}

                @if ($errors->has('image'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                @endif

              </div>

               @if(!empty($data['record']) && !empty($data['record']->image))
                <div class="form-group mb-0">
                        {{ Form::checkbox('removeImage',1,0,array("id"=>'removeImage')) }}
                    
                      {{ Form::label('removeImage','Remove Image',array('class'=>"form-label")) }}
                   
                </div>
              @endif

              <div class="text-right">

                <button type="submit" class="btn btn-primary submit">Submit</button>

              </div>
            </div>
            
           
          {{ Form::close() }}
            

        </div>
      </div>
    </div>
    </div>
  </div>

@if(empty($data['record']))
<script type="text/javascript">

$(document).ready(function(){

       var prevKey = "";
        window.addEventListener('beforeunload', function (e) { 

          if (e.key=="F5") {
                      window.onbeforeunload = ConfirmLeave;
                  }
                  else if (e.key.toUpperCase() == "W" && prevKey == "CONTROL") {                
                      window.onbeforeunload = ConfirmLeave;   
                  }
                  else if (e.key.toUpperCase() == "R" && prevKey == "CONTROL") {
                      window.onbeforeunload = ConfirmLeave;
                  }
                  else if (e.key.toUpperCase() == "F4" && (prevKey == "ALT" || prevKey == "CONTROL")) {
                      window.onbeforeunload = ConfirmLeave;
                  }
                  prevKey = e.key.toUpperCase();


        }); 

         $("a").each(function(e)
          {

            $(this).click(function()
            {
              var r = confirm("New user has not been SAVED!");

                if (r == true) {
                  
                  return true;

                } else {

                  return false;

                }

            });
          });

       });
</script>

@endif
   
  <script type="text/javascript">

       
    $(document).ready(function(){

      //change the key of role from 0 to blank
      $("#role option:first").val('');
      $("#role1 option:first").val('');

      var user_id = $("#user_id").val();
      var role_id =  $(".role option:selected").val();
      var parent_id = $("#parent_id").val();

     

      if(user_id !== ""){
        
        onChangeRoleChange(user_id, role_id, parent_id);
 
      }

        $(".submit").click(function(e){
             e.preventDefault();
             $("#userContainer").submit();
         });

        $(document).on('change',".salesManager .form-control",function(){
          $("#salesPersonBusinessLineData").html('');
          $("#businessLineData").html('');
          $("#salesPersonManager").html('');
          
         

          jQuery.ajax({
                url: "{{ route('salesPersonBusinessLineData') }}",
                method: 'get',
                data: {
                   id: $(".salesManager .form-control option:selected").val(),
                   user_id: user_id
                },
                success: function(data){
                  // $(".prefix").html(data);
                  console.log(data);
                  $("#salesPersonBusinessLineData").html(data);
                }
                  
          });
          
        });



        $(document).on('click',".salesPersonBusiness",function(){

           
          $("#salesPersonBusinessLineData").html('');
          $("#businessLineData").html('');

          $("#salesPersonManager").html('');
         

          jQuery.ajax({
                url: "{{ route('salesManagerBusinessLineData') }}",
                method: 'get',
                data: {
                   bussiness_id: $(this).val(),
                  },
                success: function(data){
                  // $(".prefix").html(data);
                  console.log(data);
                  $("#salesPersonManager").html(data);
                }
                  
          });
          
        });

        $(".role").change(function(){

              $("#roleChanged").val(1);
          
              $(".salesManager").html('');
              $("#businessLineData").html('');
              $("#salesPersonBusinessLineData").html('');
              $("#salesPersonManager").html('');

            var roleId = $(this).val();
            if(roleId == 2){

              
              var InstCont = $("#businessLineData");
 
              jQuery.ajax({
                url: "{{ route('businessLineSalesManagers') }}",
                method: 'get',
                data: {
                   role_id: roleId,
                   user_id: user_id,
                   roleChanged: $("#roleChanged").val()
                },
                success: function(data){
                  InstCont.html(data);
                }
                  
                });

            }else if(roleId == 3){

             

              var InstCont = $(".salesManager");

              jQuery.ajax({
              url: "{{ route('businessLineSalesPerson') }}",
              method: 'get',
              data: {
                role_id: roleId,
                user_id: user_id,
                roleChanged: $("#roleChanged").val()
              },
              success: function(data){
                InstCont.html(data);
              }
                
              });

              }
            });



        function onChangeRoleChange(user_id, role_id, parent_id){
           //console.log($('input[name="buisnessline[]"]:checked').val());
           $(".salesManager").html('');
           $("#businessLineData").html('');
           $("#salesPersonBusinessLineData").html('');
           $("#salesPersonManager").html('');
         

            if(role_id == 2){
       
              
              var InstCont = $("#businessLineData");

              jQuery.ajax({
                url: "{{ route('businessLineSalesManagers') }}",
                method: 'get',
                data: {
                   role_id: role_id,
                   user_id: user_id,
                   roleChanged: $("#roleChanged").val()
                },
                success: function(data){
                  InstCont.html(data);
                }
                  
                });


                

            }else if(role_id == 3){

             

             
              var InstCont = $(".salesManager");

              jQuery.ajax({
              url: "{{ route('businessLineSalesPerson') }}",
              method: 'get',
              data: {
                role_id: role_id,
                user_id: user_id,
                roleChanged: $("#roleChanged").val()
                  
              },
              success: function(data){
                InstCont.html(data);
                var radioSales = $('input[name="buisnessline[]"]:checked').val();
                //onChangeRoleChange(user_id, role_id, parent_id, radioSales);
                console.log(radioSales+">>>>>>>>>>>>>>>>>>>>>>>>>");
                jQuery.ajax({
                url: "{{ route('salesManagerBusinessLineData') }}",
                method: 'get',
                data: {
                   bussiness_id: radioSales,
                   user_id: user_id,
                  },
                success: function(data){
                  // $(".prefix").html(data);
                  console.log(data);
                  $("#salesPersonManager").html(data);
                }
                  
                 });



              }
                
              });

 
            

              

              }
             
        }


    });
  </script>
@endsection
@extends('layouts.app')
  <!-- Content -->
@section('content')

         <div class="container-fluid">

            <fieldset>
              <legend>Upload Report</legend>
              {{ Form::open(['route'=>'admin.record_save','files'=> true]) }}
                {{ Form::hidden('id',!empty($data) ? $data->id : null) }}
                <div class="form-group row">

                    <div class="col-md-3">
                      <label>Select Manager</label>
                     

                      {{ Form::select('salesmanager',$users,(!empty($data->salesmanager)? $data->salesmanager : null),['class'=>'form-control salesmanager']) }}

                      @if ($errors->has('salesmanager'))
                          <span class="text-danger">
                              <strong>{{ $errors->first('salesmanager') }}</strong>
                          </span>
                      @endif
                    </div>

                    @if(!empty($data))

                      <div class="col-md-3 salesPerson">
                      
                        <div class="form-group">
                          <label for="salesmanager" class="form-label">Sales Person</label>
                          {{ Form::select('salesperson',$person,$data->salesperson,array("class"=>"form-control")) }}
                        </div>
                      </div>
                      
                    @else

                    <div class="col-md-3 salesPerson" style="display: none">
                      

                    </div>
                    @endif
                   


                    <div class="col-md-3">
                      <label>Select File</label>
                      <input type="file" class="form-control" name="file">

                      @if(!empty($data))
                        <input type="hidden" name="prev_file" value="{{ $data->file }}">
                      @endif
                      @if ($errors->has('file'))
                          <span class="text-danger">
                              <strong>{{ $errors->first('file') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="col-md-3">

                      <label>Select Year</label>
                      <select class="form-control" name="year">
                        @php $year = date('Y'); @endphp
                        @for($i=$year-5;$i<=$year;$i++)
                          <option {{ !empty($data) && $data->uploaded_year == $i ? 'selected' : null }} value="{{ $i }}">{{ $i }}</option>
                        @endfor
                      </select>
                      @if ($errors->has('year'))
                          <span class="text-danger">
                              <strong>{{ $errors->first('year') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="col-md-3">
                      <label>Select Month</label>
                      <select class="form-control" name="month">
                        @for($i=1;$i<=12;$i++)

                          <option {{ !empty($data) && $data->uploaded_month == $i ? 'selected' : null }} value="{{ $i }}">{{ ucfirst(date('M',mktime(0, 0, 0, $i, 10))) }}</option>
                        @endfor
                      </select>
                      @if ($errors->has('month'))
                          <span class="text-danger">
                              <strong>{{ $errors->first('month') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="col-md-3">
                        <label>&nbsp;</label><br>
                        {{ Form::submit("Upload Data",array("class"=>"btn btn-primary")) }}
                    </div>
                </div>
              {{ Form::close() }}
            </fieldset>
          </div>

          <script type="text/javascript">
            $(document).ready(function(){
              $(".salesmanager").change(function(){
                
                  var roleId = $(this).val();

                  if(roleId != null){

                    var InstCont = $(".salesPerson");

                    jQuery.ajax({
                      url: "{{ route('salespersongroup') }}",
                      method: 'get',
                      data: {
                         id: roleId
                      },
                      success: function(data){
                        InstCont.html(data);
                        InstCont.show();
                      }
                        
                      });

                  }else{

                    $(".salesManager").html('');
                  }

              });
            });
          </script>
@endsection
@extends('layouts.app')

@section('content')

<!-- <div class="container-fluid">

    <h2>[Super User - Portal]</h2>

</div> -->
<div class="container">
    <div class=" justify-content-center">
            @foreach($datas as $data)
                {{ Form::open(['route'=>'business','files'=> true]) }}

                <div class="form-group py-3 mt-3">
                    <a href="{{ route('admin.tracker.chart',$data->id) }}" class="title2">{{ $data->firstname.' '.$data->lastname }}</a>
                </div>

                <table class="table table-bordered dashboard">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th width="180">{{ date("Y") -1 }} Target Value $ 2</th>
                            <th width="180">{{ date("Y") -1 }} Target Value &pound; 2</th>
                            <th width="180">YTD v TARGET</th>
                            <th width="180">Growth V Last YTD</th>
                            <th>Years in Business / Role</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                <tbody>
                    @foreach($data->fatchData as $child)
                    <tr>
                        @php 
                            $reco = helper::growthVLast($data->id,$child->id);

                            $ytdTarget = helper::ytdVTarget($data->id,$child->id);
                        @endphp

                        <td>{{ $child->firstname.' '.$child->lastname }}</td>

                        <td>{{ !empty($reco[1]) ? ($settings->value)*($reco[1]) : 0 }}</td>

                        <td>{{ !empty($reco[1]) ? $reco[1] : 0 }}</td>

                        <td align="right">
                            <span style="width:{{ str_replace('-','',$ytdTarget) }}%" class="bgPercent {{ !empty($ytdTarget) && $ytdTarget > 0 ? 'success' : 'danger' }}"></span>
                            <span class="label-text">{{ !empty($ytdTarget) ? $ytdTarget : 0 }}%</span>

                        </td>

                        <td align="right">
                            <span style="width:{{ str_replace('-','',$reco[0]) }}%" class="bgPercent {{ !empty($reco[0]) && $reco[0] > 0 ? 'success' : 'danger' }}"></span><span class="label-text">{{ !empty($reco[0]) ? $reco[0] : 0 }}%</span>
                        </td>
                        <td>
                            {{ Form::hidden('id[]',$child->id) }} {{ Form::text('profileYear[]',$child->businessrole,array("class"=>"form-control actual")) }}
                        </td>
                        <td><a href="{{ url('admin/sales-record-single',$child->id) }}" class="btn btn-sm btn-primary">View Details</a></td>
                    </tr>
                    @endforeach

                    </tbody>
                </table><hr>
                <div class="form-group text-center"> 
                    {{ Form::submit("Submit Record",array("class"=>"btn btn-primary")) }}
                </div>
                {{ Form::close() }}
                
            @endforeach
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="col-md-6"> 
          @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
          @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   
        </div>
    </div>

   @php

   $i = 0;

   @endphp

    @foreach($grossData as $key => $datas)

    @php $datas = $datas[0]; @endphp
    
    @if(isset($datas->id))
    <div class="justify-content-center">
     
       
            <?php //echo "<pre>"; print_r($grossData[0]['fatchData']); die;?>
        
            {{ Form::open(['route'=>'business','files'=> true]) }}
            
                <div class="form-group py-3 pl-3 pr-3 mt-3" style="background-color: #ddd; padding: 10px">
                    <a href="{{ route('admin.tracker.chart',$datas->id) }}" class="title2">{{ $datas->firstname.' '.$datas->lastname }}</a>

                    <a href="javascript:void(0)" class="title2 pull-right" data-toggle="collapse" data-target="#show{{ $datas->id }}" ><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                </div>

                <div id="show{{ $datas->id }}" class="collapse <?php if($i == 0){echo 'show';}?>">
                <table class="table table-bordered dashboard" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Reference No</th>
                            <th>Sales Person</th>

                            <th>{{ date("Y") }} Target Value $</th>
                            <th>{{ date("Y") }} Target Value &pound;</th>
                            <th>{{ date("Y") }} Actual Value &pound;</th>

                            <th>YTD v TARGET</th>
                            <th>Growth V Last YTD</th>
                            <th>Years in Business / Role</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                <tbody>
                    @php $col1 = $col2 = $col3 = $col4 = $col5 = 0; @endphp
                    @foreach($grossData[$key]['fatchData'] as $child)
                    <tr>
                        @php 
                            $reco = helper::growthVLast($child->parent,$child->id);

                            $ytdTarget = helper::ytdVTarget($child->parent,$child->id);

                            $col1 += round($settings->value*$reco[1]);
                            $col2 += round($reco[1]);
                            $col3 += $ytdTarget;
                            $col4 += $reco[0];
                            $col5 += $reco[3];
                        @endphp
                        <td data-order="{{ $child->sorting }}">{{ $child->role==3 ? helper::referencePrefix($child->parent).'-' : null }}{{ $child->sorting }}</td>
                        <td>{{ $child->firstname.' '.$child->lastname }}</td>

                        <td>{{ !empty($reco[1]) ? number_format(round($settings->value*$reco[1])) : 0 }}</td>
                        <td>{{ !empty($reco[1]) ? number_format(round($reco[1])) : 0 }}</td>
                        <td>{{ !empty($reco[3]) ? number_format(round($reco[3])) : 0 }}</td>
                        <td align="right">
                            <span style="width:{{ str_replace('-','',$ytdTarget) }}%" class="bgPercent {{ !empty($ytdTarget) && $ytdTarget > 0 ? 'success' : 'danger' }}"></span>
                            <span class="label-text">{{ !empty($ytdTarget) ? round($ytdTarget) : 0 }}%</span>

                        </td>

                        <td align="right">
                            <span style="width:{{ str_replace('-','',$reco[0]) }}%" class="bgPercent {{ !empty($reco[0]) && $reco[0] > 0 ? 'success' : 'danger' }}"></span><span class="label-text">{{ !empty($reco[0]) ? round($reco[0]) : 0 }}%</span>
                        </td>
                        <td>

                            {{ Form::hidden('id[]',$child->id) }} {{ Form::text('profileYear[]',$child->businessrole,array("class"=>"form-control actual")) }}
 
                        </td>
                        <td><a href="{{ url('admin/sales-record-single',$child->id) }}" class="btn btn-sm btn-primary">View Details</a></td>
                    </tr>

                     

                    @php 
                       
                       $var = 'salesmanager_'.$child->id;
                       Session::put($var ,  $child->parent);
                       
                    @endphp


                    @endforeach

                    </tbody>


                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Total</th>
                            <th>{{ number_format($col1) }}</th>
                            <th>{{ number_format($col2) }}</th>
                            <th>{{ number_format($col5) }}</th>
                            <th>{{-- sizeof($datas->fatchData) > 0 ? round($col3/sizeof($datas->fatchData)) : 0 --}}</th>
                            <th>{{-- sizeof($datas->fatchData) > 0 ? round($col4/sizeof($datas->fatchData)) : 0 --}}</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
                
                <hr>
                 <div class="form-group text-center"> 
                    {{ Form::submit("Submit Record",array("class"=>"btn btn-primary")) }}
                </div>
            {{ Form::close() }}

          
            </div>
        </div>

        @endif
        @php $i++; @endphp
        @endforeach

        <?php //print_r(Session::get('cart'));?>

        <script type="text/javascript">
            $(document).ready(function(){
                  oTable = $('.table').dataTable({
                      "iDisplayLength": -1,
                      "bLengthChange":false,
                      "bPaginate": false,
                      "pageLength":20,
                      "bInfo": false,
                      "bFilter": false,
                      'columnDefs': [ {

                        'targets': [1,2,3,4,5,6,7,8], /* column index */

                        'orderable': false, /* true or false */

                     }] 
                  });

                  oTable.fnSort( [ [0,'asc'] ] );

            })
        </script>
    </div>
</div>
@endsection

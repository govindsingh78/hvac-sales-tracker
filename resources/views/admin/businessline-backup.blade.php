@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="col-md-6"> 
          @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
          @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   
        </div>
    </div>

    
    <div class="justify-content-center">

                    <?php
                    $sum_rec_setting_81_11 = 0;
                    $sum_rec_one_81_12 = 0;
                    $sum_rec_three_81_13 = 0;
                    $sum_ytdTarget_total_81_14 = 0;
                    $sum_rec_zero_81_15 = 0;


                    $sum_rec_setting_82_11 = 0;
                    $sum_rec_one_82_12 = 0;
                    $sum_rec_three_82_13 = 0;
                    $sum_ytdTarget_total_82_14 = 0;
                    $sum_rec_zero_82_15 = 0;



                    $sum_rec_setting_83_11 = 0;
                    $sum_rec_one_83_12 = 0;
                    $sum_rec_three_83_13 = 0;
                    $sum_ytdTarget_total_83_14 = 0;
                    $sum_rec_zero_83_15 = 0;


                    $sum_rec_setting_84_11 = 0;
                    $sum_rec_one_84_12 = 0;
                    $sum_rec_three_84_13 = 0;
                    $sum_ytdTarget_total_84_14 = 0;
                    $sum_rec_zero_84_15 = 0;



                    $sum_rec_setting_77_11 = 0;
                    $sum_rec_one_77_12 = 0;
                    $sum_rec_three_77_13 = 0;
                    $sum_ytdTarget_total_77_14 = 0;
                    $sum_rec_zero_77_15 = 0;
                            

                    foreach($datas_4->fatchData as $child){
                    if($child->id == 95){
                                   
                    $reco = helper::growthVLast($datas_4->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_4->id,$child->id);
                         

                    $sum_rec_setting_81_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_81_12 += round($reco[1]);
                    $sum_rec_three_81_13 += round($reco[3]);
                    $sum_ytdTarget_total_81_14 += round($ytdTarget);
                    $sum_rec_zero_81_15 += round($reco[0]);
                    
                    }


                    if($child->id == 96){
                                   
                    $reco = helper::growthVLast($datas_4->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_4->id,$child->id);
                         

                    $sum_rec_setting_82_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_82_12 += round($reco[1]);
                    $sum_rec_three_82_13 += round($reco[3]);
                    $sum_ytdTarget_total_82_14 += round($ytdTarget);
                    $sum_rec_zero_82_15 += round($reco[0]);
                    
                    }


                    if($child->id == 97){
                                   
                    $reco = helper::growthVLast($datas_4->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_4->id,$child->id);
                         

                    $sum_rec_setting_83_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_83_12 += round($reco[1]);
                    $sum_rec_three_83_13 += round($reco[3]);
                    $sum_ytdTarget_total_83_14 += round($ytdTarget);
                    $sum_rec_zero_83_15 += round($reco[0]);
                    
                    }



                    if($child->id == 98){
                                   
                    $reco = helper::growthVLast($datas_4->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_4->id,$child->id);
                         

                    $sum_rec_setting_84_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_84_12 += round($reco[1]);
                    $sum_rec_three_84_13 += round($reco[3]);
                    $sum_ytdTarget_total_84_14 += round($ytdTarget);
                    $sum_rec_zero_84_15 += round($reco[0]);
                    
                    }


                    if($child->id == 99){
                                   
                    $reco = helper::growthVLast($datas_4->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_4->id,$child->id);
                         

                    $sum_rec_setting_77_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_77_12 += round($reco[1]);
                    $sum_rec_three_77_13 += round($reco[3]);
                    $sum_ytdTarget_total_77_14 += round($ytdTarget);
                    $sum_rec_zero_77_15 += round($reco[0]);
                    
                    }


                   }

                   foreach($datas_2->fatchData as $child){
                    if($child->id == 85){
                                   
                    $reco = helper::growthVLast($datas_2->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_2->id,$child->id);
                         

                    $sum_rec_setting_81_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_81_12 += round($reco[1]);
                    $sum_rec_three_81_13 += round($reco[3]);
                    $sum_ytdTarget_total_81_14 += round($ytdTarget);
                    $sum_rec_zero_81_15 += round($reco[0]);
                    
                    }


                    if($child->id == 86){
                                   
                    $reco = helper::growthVLast($datas_2->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_2->id,$child->id);
                         

                    $sum_rec_setting_82_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_82_12 += round($reco[1]);
                    $sum_rec_three_82_13 += round($reco[3]);
                    $sum_ytdTarget_total_82_14 += round($ytdTarget);
                    $sum_rec_zero_82_15 += round($reco[0]);
                    
                    }


                    if($child->id == 87){
                                   
                    $reco = helper::growthVLast($datas_2->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_2->id,$child->id);
                         

                    $sum_rec_setting_83_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_83_12 += round($reco[1]);
                    $sum_rec_three_83_13 += round($reco[3]);
                    $sum_ytdTarget_total_83_14 += round($ytdTarget);
                    $sum_rec_zero_83_15 += round($reco[0]);
                    
                    }



                    if($child->id == 88){
                                   
                    $reco = helper::growthVLast($datas_2->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_2->id,$child->id);
                         

                    $sum_rec_setting_84_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_84_12 += round($reco[1]);
                    $sum_rec_three_84_13 += round($reco[3]);
                    $sum_ytdTarget_total_84_14 += round($ytdTarget);
                    $sum_rec_zero_84_15 += round($reco[0]);
                    
                    }


                    if($child->id == 17){
                                   
                    $reco = helper::growthVLast($datas_2->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_2->id,$child->id);
                         

                    $sum_rec_setting_77_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_77_12 += round($reco[1]);
                    $sum_rec_three_77_13 += round($reco[3]);
                    $sum_ytdTarget_total_77_14 += round($ytdTarget);
                    $sum_rec_zero_77_15 += round($reco[0]);
                    
                    }


                   }


                   foreach($datas_3->fatchData as $child){
                    if($child->id == 90){
                                   
                    $reco = helper::growthVLast($datas_3->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_3->id,$child->id);
                         

                    $sum_rec_setting_81_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_81_12 += round($reco[1]);
                    $sum_rec_three_81_13 += round($reco[3]);
                    $sum_ytdTarget_total_81_14 += round($ytdTarget);
                    $sum_rec_zero_81_15 += round($reco[0]);
                    
                    }


                    if($child->id == 91){
                                   
                    $reco = helper::growthVLast($datas_3->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_3->id,$child->id);
                         

                    $sum_rec_setting_82_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_82_12 += round($reco[1]);
                    $sum_rec_three_82_13 += round($reco[3]);
                    $sum_ytdTarget_total_82_14 += round($ytdTarget);
                    $sum_rec_zero_82_15 += round($reco[0]);
                    
                    }


                    if($child->id == 92){
                                   
                    $reco = helper::growthVLast($datas_3->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_3->id,$child->id);
                         

                    $sum_rec_setting_83_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_83_12 += round($reco[1]);
                    $sum_rec_three_83_13 += round($reco[3]);
                    $sum_ytdTarget_total_83_14 += round($ytdTarget);
                    $sum_rec_zero_83_15 += round($reco[0]);
                    
                    }



                    if($child->id == 93){
                                   
                    $reco = helper::growthVLast($datas_3->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_3->id,$child->id);
                         

                    $sum_rec_setting_84_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_84_12 += round($reco[1]);
                    $sum_rec_three_84_13 += round($reco[3]);
                    $sum_ytdTarget_total_84_14 += round($ytdTarget);
                    $sum_rec_zero_84_15 += round($reco[0]);
                    
                    }


                    if($child->id == 94){
                                   
                    $reco = helper::growthVLast($datas_3->id,$child->id);
                    $ytdTarget = helper::ytdVTarget($datas_3->id,$child->id);
                         

                    $sum_rec_setting_77_11 +=  $settings->value*$reco[1];
                    $sum_rec_one_77_12 += round($reco[1]);
                    $sum_rec_three_77_13 += round($reco[3]);
                    $sum_ytdTarget_total_77_14 += round($ytdTarget);
                    $sum_rec_zero_77_15 += round($reco[0]);
                    
                    }


                   }
                  ?>

                    



        
            {{ Form::open(['route'=>'business','files'=> true]) }}
            
                <div class="form-group py-3 mt-3">
                    <a href="{{ route('admin.tracker.chart',$datas->id) }}" class="title2">{{ $datas->firstname.' '.$datas->lastname }}</a>
                </div>

                <table class="table table-bordered dashboard" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Reference No</th>
                            <th>Sales Person</th>

                            <th>{{ date("Y") }} Target Value $</th>
                            <th>{{ date("Y") }} Target Value &pound;</th>
                            <th>{{ date("Y") }} Actual Value &pound;</th>

                            <th>YTD v TARGET</th>
                            <th>Growth V Last YTD</th>
                            <th>Years in Business / Role</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    






                    @php $col1 = $col2 = $col3 = $col4 = $col5 = 0; @endphp
                    @foreach($datas->fatchData as $child)
                    <tr>
                        @php 
                            $reco = helper::growthVLast($datas->id,$child->id);

                            $ytdTarget = helper::ytdVTarget($datas->id,$child->id);

                            $col1 += round($settings->value*$reco[1]);
                            $col2 += round($reco[1]);
                            $col3 += $ytdTarget;
                            $col4 += $reco[0];
                            $col5 += $reco[3];
                        @endphp
                        <td data-order="{{ $child->sorting }}">{{ $child->role==3 ? helper::referencePrefix($child->parent).'-' : null }}{{ $child->sorting }}</td>
                        <td>{{ $child->firstname.' '.$child->lastname }}</td>


                        <?php

                        

                        $sum_rec_1 = "sum_rec_setting_".$child->id."_11";
                        $sum_rec_11 = isset($$sum_rec_1) ? $$sum_rec_1: 0;
                        $total_11 = $sum_rec_11; //$settings->value*$reco[1] +  

                        $sum_rec_2 = "sum_rec_one_".$child->id."_12";
                        $sum_rec_12 = isset($$sum_rec_2) ? $$sum_rec_2: 0;
                        $total_12 = $sum_rec_12; //$reco[1] + 

                        $sum_rec_3 = "sum_rec_three_".$child->id."_13";
                        $sum_rec_13 = isset($$sum_rec_3) ? $$sum_rec_3: 0;
                        $total_13  =  $sum_rec_13; //$reco[3] + 
                         
                        ?>

                        <td>{{ !empty($total_11) ? $total_11 : 0 }}</td>
                        <td>{{ !empty($total_12) ? $total_12 : 0 }}</td>
                        <td>{{ !empty($total_13) ? $total_13 : 0 }}</td>

                        <?php
                        $sum_rec = "sum_ytdTarget_total_".$child->id."_14";
                        $graphWidth = isset($$sum_rec) ? $$sum_rec: 0;
                        $oldGraphWidth = $ytdTarget;
                        $newGraphWidth = $graphWidth;
                        $totalGraphWidth = $newGraphWidth; //$oldGraphWidth + 
                        ?>


                        <td align="right">
                            <span style="width:{{ $totalGraphWidth }}%" class="bgPercent {{ !empty($totalGraphWidth) && $totalGraphWidth > 0 ? 'success' : 'danger' }}"></span>
                            <span class="label-text">{{ !empty($totalGraphWidth) ? round($totalGraphWidth) : 0 }}%</span>

                        </td>
                        <?php
                        $sum_rec = "sum_rec_zero_".$child->id."_15";
                        $graphWidth = isset($$sum_rec) ? $$sum_rec: 0;
                        $oldGraphWidth = str_replace('-','',$reco[0]);
                        $newGraphWidth = str_replace('-','',$graphWidth);
                        $totalGraphWidth =  $newGraphWidth; //$oldGraphWidth +
                        ?>
                        <td align="right">
                            <span style="width:{{ $totalGraphWidth }}%" class="bgPercent {{ !empty($totalGraphWidth) && $totalGraphWidth > 0 ? 'success' : 'danger' }}"></span><span class="label-text">{{ !empty($totalGraphWidth) ? round($totalGraphWidth) : 0 }}%</span>
                        </td>
                        <td>

                            {{ Form::hidden('id[]',$child->id) }} {{ Form::text('profileYear[]',$child->businessrole,array("class"=>"form-control actual")) }}

                        </td>
                        <td><a href="{{ url('admin/sales-record-single',$child->id) }}" class="btn btn-sm btn-primary">View Details</a></td>
                    </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Total</th>
                            <th>{{ number_format($col1) }}</th>
                            <th>{{ number_format($col2) }}</th>
                            <th>{{ number_format($col5) }}</th>
                            <th>{{-- sizeof($datas->fatchData) > 0 ? round($col3/sizeof($datas->fatchData)) : 0 --}}</th>
                            <th>{{-- sizeof($datas->fatchData) > 0 ? round($col4/sizeof($datas->fatchData)) : 0 --}}</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table><hr>
                 <div class="form-group text-center"> 
                    {{ Form::submit("Submit Record",array("class"=>"btn btn-primary")) }}
                </div>
            {{ Form::close() }}
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                  oTable = $('.table').dataTable({
                      "iDisplayLength": -1,
                      "bLengthChange":false,
                      "bPaginate": false,
                      "pageLength":20,
                      "bInfo": false,
                      "bFilter": false,
                      'columnDefs': [ {

                        'targets': [1,2,3,4,5,6,7,8], /* column index */

                        'orderable': false, /* true or false */

                     }] 
                  });

                  oTable.fnSort( [ [0,'asc'] ] );

            })
        </script>
    </div>
</div>
@endsection

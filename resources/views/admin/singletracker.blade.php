@extends('layouts.app')

@section('content')




<div class="container">

    <label class="title2 pb-2">{{ $personRecord->firstname.' '.$personRecord->lastname }}</label>

    <div class="row justify-content-center mb-3">
        <div class="col-md-6"> 
          @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
          @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   
        </div>
    </div>


    <div class="row justify-content-center">

        <div class="col-md-6">
            <div class="card">

                <div class="card-body">
                        <div id="sales1" style="width:100%; height:350px;"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                
                <div class="card-body">
                        <div id="sales2" style="width:100%; height:350px;"></div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container py-5">

        <label class="title2 pb-2">Monthly Sales Figures</label>

        
            <table class="table table-striped table-bordered">
                <thead>
                    <th>Target</th>
                    <th><span class="totalTarget"></span></th>
                    <th>Month %</th>
                </thead>
                <tbody>
                     @foreach($datas as $key => $data)
                         @if($data->year == date("Y")) 
                            <tr>
                                <th>{{ date('M', mktime(0, 0, 0, $data->month , 10)) }}</th>
                                <td>{{ Form::hidden('actualSales',($data->tarrget),array("class"=>"actual targetprice")) }} {{ !empty($data->tarrget) ? helper::numformat($data->tarrget) : 0 }}</td>
                                <td><span class="percent">{{ round($data->percent) }}%</span></td>
                            </tr>
                         @endif
                     @endforeach
                 </tbody>
            </table>
           
</div>

 <div class="container py-5">

        <label class="title2 pb-2">Monthly Sales Figures</label>

        {{ Form::open(['route'=>'admin.tracker.store','files'=> true,"id"=>"mainform"]) }}

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th>Actual  {{ date("Y") - 1 }}</th>
                    <th>Target  {{ date("Y") }}</th>
                    <th>Actual {{ date("Y") }}</th>
                    <th>Cumulative {{ date("Y")-1 }}</th>
                    <th>Cumulative Target {{ date("Y") }}</th>
                    <th>Cumulative Actual {{ date("Y") }}</th>
                    <th>Growth YTD Current v Last</th>
                    <th>Backlog</th>
                </tr>
            </thead>
            <tbody>
              @php $backlog[] = $growthykd[]=$ykd[] = $targetykd[] = $actual[] = $target[] = null; $i=0; @endphp
                @foreach($datas as $key => $data)

                     @if($data->year == date("Y"))   


                        @php $target[] = $data->tarrget; $actual[] = $data->actual; $targetykd[] = $data->targetykd; 
                        $ykd[] = $data->ykd; $growthykd[] = $data->growthykd; $backlog[] = $data->backlog; @endphp

                       
                        <tr>
                            <th>{{ date('M', mktime(0, 0, 0, $data->month , 10)) }}</th>

                            <td>

                              {{ Form::text('previous_sctual[]',isset($actualPevious[$i]) ? round($actualPevious[$i]) : 0,array("class"=>"form-control actual previous_sctual")) }}

                               {{ Form::hidden('prev_id[]',isset($previous_id[$i]) ? round($previous_id[$i]) : 0,array("class"=>"customeId")) }} 

                               {{ Form::hidden('salesperson',$personRecord->id) }} 

                               {{ Form::hidden('salesmanager',$personRecord->parent) }} 

                               {{ Form::hidden('month[]',$data->month) }} 
                              
                            </td>

                            <td>{{ Form::hidden('id[]',$data->id,array("class"=>"customeId")) }} 
                                {{ helper::numformat($data->tarrget) }}
                            </td>


                            <td>{{ Form::text('actual[]',round($data->actual),array("class"=>"form-control actual actualV")) }}</td>
                            <td>

                              {{ Form::text('previousYtd[]',isset($comPevious[$i]) ? round($comPevious[$i]) : 0,array("class"=>"form-control actual previousYtd","readonly"=>"true")) }} 

                            </td>
                            <td>
                                 {{ round($data->targetykd) }}
                                 
                            </td>
                            <td>
                                {{ Form::text('ykd[]',round($data->ykd),array("class"=>"form-control actual tactualV","readonly"=>"true")) }}

                            </td>
                            <td>{{ Form::text('growth[]',round($data->growthykd),array("class"=>"form-control actual growthykd","readonly"=>"true")) }}</td>
                            <td>{{ Form::text('backlog[]',round($data->backlog),array("class"=>"form-control actual","rel"=>"backlog")) }}</td>
                        </tr>
                        
                        @php $i=$i+1; @endphp
                   @endif
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Total</th>
                    <th>{{ number_format(round(array_sum($actualPevious))) }}</th>
                    <th><span class="totalTarget1">{{ helper::numformat(array_sum($target)) }}</span></th>
                    <th>{{ number_format(round(array_sum($actual))) }}</th>
                    <th>{{-- number_format(round(array_sum($comPevious))) --}}</th>
                    <th style="text-align: center;">{{-- number_format(round(array_sum($targetykd))) --}}</th>
                    <th>{{-- number_format(round(array_sum($ykd))) --}}</th>
                    <th>{{-- number_format(round(array_sum($growthykd))) --}}</th>
                    <th>{{ number_format(round(array_sum($backlog))) }}</th>
                </tr>
            </tfoot>
        </table>

         <div class="form-group text-center pt-5"> 
                <a id="submit" class="btn btn-danger">Update Record</a> 
            </div>

        {{ Form::close() }}

    </div>





<script type="text/javascript" src="{{ asset('js/chart.js') }}"></script>


<script type="text/javascript">


    document.addEventListener('DOMContentLoaded', function () {

        $("#submit").click(function(){
            $("#mainform").submit();
        });

        var totalRecord;
        function total(){

             var actualR = 0;
            $(".targetprice").each(function( index ) {
                actualR += parseFloat($( this ).val());
            });
            $(".totalTarget,.totalTarget1").text(Math.round(actualR).toLocaleString());

            return Math.round(actualR);

        }
        setTimeout(function(){

            columnChart('sales1','Sales By Month',{!! json_encode($salesActual) !!});

            columnChart('sales2','Cumulative Sales By Month',{!! json_encode($salesYkd) !!});

           totalRecord = total();

        },1000);



        $(".previous_sctual").blur(function(){

            var activePoint = $('.previous_sctual').index(this);



            PactualR = 0;

            $(".previous_sctual").each(function(){

                PactualR += parseFloat($( this ).val());

               // console.log(actualR);

                var index = $('.previous_sctual').index(this);


                if(index >= activePoint ){

                    growth = parseFloat($(".tactualV:eq("+index+")").val()) - PactualR;

                    $(".previousYtd:eq( "+index+" )").val(PactualR);

                    $(".growthykd:eq("+index+")").val(growth);

                }


            });
            

        });


        $(".actualV").blur(function(){

            var activePoint = $('.actualV').index(this);



            PactualR = 0;

            $(".actualV").each(function(){

                PactualR += parseFloat($( this ).val());

               // console.log(actualR);

                var index = $('.actualV').index(this);


                if(index >= activePoint ){

                    growth = PactualR - parseFloat($(".previousYtd:eq("+index+")").val());

                    $(".tactualV:eq( "+index+" )").val(PactualR);

                    $(".growthykd:eq("+index+")").val(growth);

                }


            });
            

        });




        /*$(".targetprice").blur(function(){

            var activePoint = $('.targetprice').index(this);

            $(".target:eq("+activePoint+")").val($(this).val());

            var actualR = 0, current=0;

            
            $(".targetprice").each(function( index ) {

                actualR += parseFloat($( this ).val());

               var current = parseFloat($( this ).val());
                 
               var activePoint = $('.targetprice').index(this);

                   setTimeout(function(){  

                    total = parseInt($(".totalTarget").text()); 

                    percent  = (current / total)*100;

                    if(Number.isNaN(percent)){

                      $(".percent:eq("+activePoint+")").text(0 + "%");

                      $(".percentreal:eq("+activePoint+")").val(0 + "%");

                    }else{

                      $(".percent:eq("+activePoint+")").text((percent).toFixed(2) + "%");

                      $(".percentreal:eq("+activePoint+")").val((percent).toFixed(2) + "%");

                    }
                    

                   },1000);


                if(index >= activePoint ){

                    $(".targetytd:eq( "+index+" )").val(actualR);
                   // console.log( index + ": " + $( this ).val() + ": " + $(".targetytd:eq( "+index+" )").val());

                }

                

            });

            $(".totalTarget,.totalTarget1").text(Math.round(actualR));*/

            /*id = ($(this).closest("tr").find(".customeId").val());
            val = $(this).val();*/



            /*type = $(this).attr("rel");

            if((val!="") && (id != "")){
               jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('input[name="_token"]').val()
                  }
              });
               jQuery.ajax({
                  url: "{{ route('salesperson.record') }}",
                  method: 'post',
                  data: {
                     records: val,
                     id: id,
                     type: type 
                  },
                  success: function(data){

                    

                  }
                    
                  });
             }
*/
       /* });*/

    });
</script>

@endsection

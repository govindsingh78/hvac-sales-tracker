@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center mb-3">
        <div class="col-md-6"> 
            {{ Form::label('name',"Search by Business Line") }}
            {{ Form::select('name',$users,null,array("class"=>"form-control search")) }}
            
        </div>
    </div>
    <div class="row justify-content-center mb-3">
        <div class="col-md-6"> 
          @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
          @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   
        </div>
    </div>

    <div class="searchResult justify-content-center">
    </div>
</div>

<script type="text/javascript">
    
    $(".search").change(function(){

        val = $(this).val();

        container = $(".searchResult");

        if((val!="") && val!=0){
            container.show();
            $(".searchResult1").hide();
           jQuery.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('input[name="_token"]').val()
              }
          });
           jQuery.ajax({
              url: "{{ route('admin.searchrecord') }}",
              method: 'get',
              data: {
                 id: val,
              },
              success: function(data){

                    container.html(data);

              }
                
              });
         }else{

            container.hide();
            $(".searchResult1").show();
         }

    })
    
</script>
@endsection

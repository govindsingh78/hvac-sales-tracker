@extends('layouts.app')

@section('content')

<div class="container">
    <div class="py-4 justify-content-center">
        <table class="table table-bordered dashboard">
            <thead>
                <tr bgcolor="#fafafa">
                    <th>Business Line</th>
                    <th>{{ date("Y")  }} Target Value &pound;</th>
                    <th>{{ date("Y")  }} Actual Value &pound;</th>
                    <th>YTD v TARGET</th>
                    <th>Growth V Last YTD</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @php $actual = $col3B = $col2B = $col1B = 0; @endphp


             


                @foreach($datas as $key => $record)

                    @php $col1B += $record['target']; $col2B += $record['ytdTarget'] > 0 ? $record['ytdTarget'] : 0; $col3B += $record['growth'] > 0 ? $record['growth'] : 0; $actual += $record['actual']; @endphp

                    <tr>
                        <td>{{ $record['name'] }}</td>
                        <td>{{ number_format($record['target']) }}</td>
                        <td>{{ number_format($record['actual']) }}</td>
                        <td>{{  $record['count'] > 0 ? (round($record['ytdTarget']/$record['count'])) : 0 }} %</td>
                        <td>{{ $record['count'] >0  ? (round($record['growth']/$record['count'])) : 0 }} %</td>
                        <td><a href="{{ route('businessline',$record['id']) }}" class="btn btn-sm btn-danger">Details</a></td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <th>Total</th>
                <th>{{ number_format(round($col1B)) }}</th>
                 <th>{{ number_format($actual) }}</th>
                <th>{{-- sizeof($datas) > 0 ? (round($col2B/sizeof($datas))) : 0 --}}</th>
                <th>{{-- sizeof($datas) > 0 ? (round($col3B/sizeof($datas))) : 0 --}}</th>
                <th>&nbsp;</th>
            </tfoot>
        </table>

    </div>
</div>
@endsection

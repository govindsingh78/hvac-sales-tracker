

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="col-md-6"> 
            {{ Form::label('name',"Search by Salesperson") }}
            {{ Form::select('name',$users,null,array("class"=>"form-control search")) }}
            
        </div>
    </div>
    <div class="searchResult1 justify-content-center">
        
            @foreach($datas as $data)


                <div class="form-group pt-3 mt-3">
                    <a href="{{ route('admin.tracker.chart',$data->id) }}" class="title1">{{ $data->firstname.' '.$data->lastname }}</a>
                </div>

                
                    @foreach($data->fatchData as $child)

                        <div class="form-group pb-3 mb-3">
                            <a class="title2" href="{{ url('admin/sales-record-single',$child->id) }}">{{ $child->firstname.' '.$child->lastname }}</a>
                        </div> 
                        {!! helper::singleRecords($child->id) !!}

                    @endforeach


                
            @endforeach
    </div>
    <div class="searchResult justify-content-center">
    </div>
</div>

<script type="text/javascript">
    
    $(".search").change(function(){

        val = $(this).val();

        container = $(".searchResult");

        if((val!="") && val!=0){
            container.show();
            $(".searchResult1").hide();
           jQuery.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('input[name="_token"]').val()
              }
          });
           jQuery.ajax({
              url: "{{ route('admin.searchrecord') }}",
              method: 'get',
              data: {
                 id: val,
              },
              success: function(data){

                    container.html(data);

              }
                
              });
         }else{

            container.hide();
            $(".searchResult1").show();
         }

    })
    
</script>
@endsection

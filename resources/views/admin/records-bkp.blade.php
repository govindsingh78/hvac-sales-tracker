@extends('layouts.app')
  <!-- Content -->
@section('content')

        
          <div class="container-fluid">

            <h4 class="title font-weight-bold py-3 mb-4">
              Records
             
            </h4>

            @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
            @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   


            <table class="table" id="table">
               <thead>
                  <tr>
                     <th>Sr No</th>
                     <th>Data</th>
                     <th>Year</th>
                     <th>Month</th>
                     <th>Salesperson</th>
                     <th>Salesmanager</th>
                     <th>Created at</th>
                  </tr>
               </thead>
               <tbody>
                @foreach($records as $key=>$record)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td><a href="{{ url($record->data) }}"><img src="{{ asset('images/excel-icon.png') }}" width="40"></a></td>
                        <td>{{ $record->uploaded_year }}</td>
                        <td>{{ ucfirst(date('M',mktime(0, 0, 0, $record->uploaded_month, 10))) }}</td>
                        <td>{{ $record->salesuser }}</td>
                        <td>{{ $record->manageruser }}</td>
                        <td>{{ $record->created_at }}</td>
                        
                    </tr>
                @endforeach
               </tbody>
            </table>
         </div>

        
       <script>
         $(function() {
               $('#table').DataTable();
         });
         </script>
   
@endsection
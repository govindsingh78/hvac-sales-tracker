@extends('layouts.app')
  <!-- Content -->
@section('content')

<div class="layout-content">

  
  <div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-4">
      <span class="text-muted font-weight-light"><a href="{{ url('admin/') }}">Home</a> /</span> Profile
    </h4>

    <div class="row">
      <div class="col-md-4">
        @if(!empty($user) && !empty($user->image))
        <img src="{{ asset($user->image) }}" class="img-fluid">
        @else
        <img src="{{ asset('images/user-dummy.png') }}" class="img-fluid">
        @endif
      </div>
      <div class="card mb-4 offset-md-2 col-md-6">
        
          {{ Form::open(['route'=>'profile.store','files'=> true]) }}
            <div class="card-body">

              @if (\Session::has('success'))
                <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {{ \Session::get('success') }}
                </div>
                @endif
              @if (\Session::has('error'))
              <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {{ \Session::get('error') }}
                </div>
              @endif   


              <div class="form-group">

                @if(!empty($user) && !empty($user->image))

                  {{ Form::hidden('prev_image',$user->image) }}
              
                @endif

               
              </div>

              <div class="form-group">

                {{ Form::label('email','E-mail',array('class'=>"form-label")) }}

                {{ Form::label('email',$user->email,array('class'=>"form-control")) }}

               
              </div>

              <div class="form-group">

                {{ Form::label('firstname','First Name',array('class'=>"form-label")) }}

                @if(!empty($user) && !empty($user->firstname))

                {{ Form::text('feild[firstname]',$user->firstname,array('class'=>"form-control")) }}

                @endif
                @if ($errors->has('feild.firstname'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.firstname') }}</strong>
                    </span>
                @endif
              </div>
               <div class="form-group">

                {{ Form::label('lastname','Last Name',array('class'=>"form-label")) }}

                
                {{ Form::text('feild[lastname]',old("feild[lastname]",$user->lastname),array('class'=>"form-control")) }}

                
                @if ($errors->has('feild.lastname'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.lastname') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">

                {{ Form::label('role','Role',array('class'=>"form-label")) }}

                {{ Form::label('role',$user->role,array('class'=>"form-control")) }}
              </div>


              <div class="form-group">

                {{ Form::label('business','Business Line',array('class'=>"form-label")) }}
                {{ Form::text('feild[business]',old('feild[business]',$user->business),array('class'=>"form-control")) }}

                @if ($errors->has('feild.phone'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.phone') }}</strong>
                    </span>
                @endif
              </div>

             
             

              @if(empty($user))
              <div class="form-group">

                {{ Form::label('password','Password.',array('class'=>"form-label")) }}
                {{ Form::text('feild[password]','',array('class'=>"form-control")) }}

                @if ($errors->has('feild.password'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.password') }}</strong>
                    </span>
                @endif
              </div>
              @endif

              @if(!empty($user))
              <div class="form-group">

                {{ Form::label('status','Status.',array('class'=>"form-label")) }}
                {{ Form::select('feild[status]',['0'=>'In active','1'=>'Active'],$user->status,array('class'=>"form-control")) }}

                @if ($errors->has('feild.password'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('feild.password') }}</strong>
                    </span>
                @endif
              </div>

              @else
              {{ Form::hidden('feild[status]',1) }}
              @endif

              <div class="form-group">

                {{ Form::label('image','Display Image',array('class'=>"form-label")) }}
                {{ Form::file('image',array('class'=>"form-control")) }}

                @if ($errors->has('image'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                @endif

              </div>

               @if(!empty($user) && !empty($user->image))
                <div class="form-group mb-0">
                        {{ Form::checkbox('removeImage',1,0,array("id"=>'removeImage')) }}
                    
                      {{ Form::label('removeImage','Remove Image',array('class'=>"form-label")) }}
                   
                </div>
              @endif

              <div class="text-right">

                <button type="submit" class="btn btn-primary">Submit</button>

              </div>
            </div>
            
           
          {{ Form::close() }}
            

        </div>
      </div>
    </div>
    </div>
  </div>
@endsection
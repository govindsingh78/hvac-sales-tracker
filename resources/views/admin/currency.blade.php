@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
        	<label class="title1">Currency Settings</label>
        	<div class="card"> 
	        	{{ Form::open(['route'=>'currency.store','files'=> true]) }}
	        		<div class="card-body">

			              @if (\Session::has('success'))
			                <div class="alert alert-success">
			                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                  {{ \Session::get('success') }}
			                </div>
			                @endif
			              @if (\Session::has('error'))
			              <div class="alert alert-danger">
			                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                  {{ \Session::get('error') }}
			                </div>
			              @endif   


			              <div class="form-group">

				                {{ Form::label('title',"Dollar Value In Pound") }}
				              	
				                {{ Form::text("currency",old("currency",$currency->value),array("class"=>"form-control")) }}

			               
			              </div>

			              <div class="form-group">
			              	{{ Form::submit("Update Currency", array("class"=>"btn btn-danger")) }}
			              </div>

			          </div>
	        	{{ Form::close() }}
	        </div>
        </div>
    </div>
</div>

@endsection
@extends('layouts.app')
  <!-- Content -->
@section('content')
      
          <div class="container-fluid">

            <h4 class="title font-weight-bold py-3 mb-4">
              Records
              
              <button type="button" onclick="window.location.href='{{ route("admin.record_manage") }}'" class="btn btn-primary btn-round pull-right"><span class="ion ion-md-add"></span>&nbsp; Create Record</button>

            </h4>

            @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
            @endif
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   


          @if(!empty($data))
            @php $i=0; @endphp
            @foreach($data as $key=>$year)
              <div class="accordion" id="accordion{{ $key }}">
                <label data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="{{ $i == 0 ? 'true' : 'false' }}" aria-controls="collapse{{ $key }}" class="list-block">
                  @if($i == 0)
                              <svg width="50" height="45" class="ficon">
                                <use xlink:href="#folder-open"></use>
                              </svg>
                  @else
                        <svg width="50" height="45" class="ficon">
                          <use xlink:href="#folder-close"></use>
                        </svg>
                  @endif
                  {{ $key }}
                </label>
                <div id="collapse{{ $key }}" class="collapse {{ $i==0 ? 'show' : null }} " aria-labelledby="heading{{ $key }}" data-parent="#accordion{{ $key }}">
                
                @if(!empty($year))  
                @php $j=0; @endphp
                     @foreach($year as $key1=>$month)
                        <div class="accordion" id="accordion{{ $key.'_'.$key1 }}">
                            <label data-toggle="collapse" data-target="#collapse{{ $key.'_'.$key1 }}" aria-expanded="{{ $i==0 && $j == 0 ? 'true' : 'false' }}" aria-controls="collapse{{ $key.'_'.$key1 }}" class="list-block">
                              @if($i==0 && $j == 0)
                                          <svg width="50" height="45" class="ficon">
                                            <use xlink:href="#folder-open"></use>
                                          </svg>
                              @else
                                    <svg width="50" height="45" class="ficon">
                                      <use xlink:href="#folder-close"></use>
                                    </svg>
                              @endif
                              {{ ucfirst(date('M',mktime(0, 0, 0, $key1, 10))) }}
                            </label>
                            <div id="collapse{{ $key.'_'.$key1 }}" class="collapse {{ $i==0 && $j == 0 ? 'show' : null }} " aria-labelledby="heading{{ $key.'_'.$key1 }}" data-parent="#accordion{{ $key.'_'.$key1 }}">

                                
                              @if(!empty($month)) 

                                @php $k=0; @endphp
                                @foreach($month as $key2=>$manager)

                                    <div class="accordion" id="accordion{{ $key.'_'.$key1.'_'.str_replace(' ', '', $key2) }}">
                                        <label data-toggle="collapse" data-target="#collapse{{ $key.'_'.$key1.'_'.str_replace(' ', '', $key2) }}" aria-expanded="{{ $i==0 && $j == 0 && $k == 0 ? 'true' : 'false' }}" aria-controls="collapse{{ $key.'_'.$key1.'_'.str_replace(' ', '', $key2) }}" class="list-block">
                                          @if($i==0 && $j == 0 && $k == 0)
                                                      <svg width="50" height="45" class="ficon">
                                                        <use xlink:href="#folder-open"></use>
                                                      </svg>
                                          @else
                                                <svg width="50" height="45" class="ficon">
                                                  <use xlink:href="#folder-close"></use>
                                                </svg>
                                          @endif
                                          {{ $key2 }}
                                        </label>
                                        <div id="collapse{!! $key.'_'.$key1.'_'.str_replace(' ', '', $key2) !!}" class="collapse {{ $i==0 && $j == 0 && $k==0 ? 'show' : null }}" aria-labelledby="heading{{ $key.'_'.$key1.'_'.str_replace(' ', '', $key2) }}" data-parent="#accordion{{ $key.'_'.$key1.'_'.str_replace(' ', '', $key2) }}">
                                          <table>
                                            @if(!empty($manager))
                                              @foreach($manager as $key3=>$record)
                                                <tr>
                                                    <td>---</td>
                                                    <td><a href="{{ url($record->data) }}"><img src="{{ asset('images/excel-icon.png') }}" width="40">{{ $record->uploaded_year }} {{ ucfirst(date('M',mktime(0, 0, 0, $record->uploaded_month, 10))) }}</a></td>
                                                 
                                                    <td>{{ $record->salesuser }}</td>
                                                    <td><b>({{ $record->manageruser }})</b></td>
                                                    <td>{{ $record->created_at }}</td>
                                                    <td><a href="{{ route('admin.record_manage',['id'=>$record->id]) }}" class="btn btn-primary">Edit</a> <a onclick="deletePrompt(this)" link="{{ route('admin.records',['id'=>$record->id]) }}" class="btn btn-danger">Delete</a></td>
                                                </tr>
                                              @endforeach
                                            @endif
                                          </table>
                                        </div>
                                    </div>

                                    @php $k=$k+1; @endphp
                                @endforeach
                              @endif
                            </div>
                          </div>
                    @php $j=$j+1; @endphp
                    @endforeach
                  @endif
                </div>
              </div>

              @php $i=$i+1; @endphp
              @endforeach
            
          @else

            No Record Found

          @endif
         {{ Form::close() }}


   
@endsection
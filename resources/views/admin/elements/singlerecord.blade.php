<table class="table table-striped table-bordered mb-4">
<thead>
    <tr>
        <th>{{ Auth::user()->firstname.' '.Auth::user()->lastname }}</th>
        <th>Actual  {{ date("Y") - 1 }}</th>
        <th>Target  {{ date("Y") }}</th>
        <th>Actual {{ date("Y") }}</th>
        <th>YTD {{ date("Y")-1 }}</th>
        <th>Target {{ date("Y") }} YTD</th>
        <th>Actual YTD {{ date("Y") }}</th>
        <th>Growth YTD Current v Last</th>
        <th>Backlog</th>
    </tr>
</thead>
<tbody> 

    @php $target[] = 0; $actual[] = 0; $targetykd[] = 0; 
            $ykd[] = 0; $growthykd[] = 0; $backlog[] = 0; @endphp

    @foreach($datas as $key => $data)
         @php $target[] = $data->tarrget; $actual[] = $data->actual; $targetykd[] = $data->targetykd; 
            $ykd[] = $data->ykd; $growthykd[] = $data->growthykd; $backlog[] = $data->backlog; @endphp
         @if($data->year == date("Y"))   
           
            <tr>
                <th>{{ date('M', mktime(0, 0, 0, $data->month , 10)) }}</th>
                <td>{{ isset($actualPevious[$key-12]) ? round($actualPevious[$key-12]) : 0 }}</td>
                <td>
                    {{ round($data->tarrget) }}
                </td>


                <td>{{ round($data->actual) }}</td>
                <td>{{ isset($comPevious[$key-12]) ? round($comPevious[$key-12]) : 0 }}</td>
                <td>
                     {{ round($data->targetykd) }}
                </td>
                <td>
                    {{ round($data->ykd) }}
                </td>
                <td>{{ round($data->growthykd) }}</td>
                <td>{{ round($data->backlog) }}</td>
            </tr>
       @endif
    @endforeach
</tbody>
<tfoot>
    <tr>
        <th>Total</th>
        <th>{{ round(array_sum($actualPevious)) }}</th>
        <th><span class="totalTarget1">{{ round(array_sum($target)) }}</span></th>
        <th>{{ round(array_sum($actual)) }}</th>
        <th>{{ round(array_sum($comPevious)) }}</th>
        <th>{{ round(array_sum($targetykd)) }}</th>
        <th>{{ round(array_sum($ykd)) }}</th>
        <th>{{ round(array_sum($growthykd)) }}</th>
        <th>{{ round(array_sum($backlog)) }}</th>
    </tr>
</tfoot>
</table>
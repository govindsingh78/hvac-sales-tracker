@php //$parentRR = helper::getparent($users->parent); @endphp
@php $i = 0; @endphp
@foreach($grossData as $data)


{{ Form::open(['route'=>('admin.target.save')]) }}
            <div class="form-group py-3 pl-3 pr-3 mt-3" style="background-color: #ddd; padding: 10px">
            <a href="{{ route('admin.tracker.chart',$data->id) }}" class="title1">{{ $data->salesManagerName }}</a>
            <a href="javascript:void(0)" class="title2 pull-right" data-toggle="collapse" data-target="#show{{ $data->id }}" ><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            </div>
           <div id="show{{ $data->id }}" class="collapse <?php if($i == 0){echo 'show';}?>">

            

           <input type="hidden" value="{{$data->id}}" 
           name="calculateMultipleSum[{{$data->id}}]" class="calculateMultipleSum form-control">


    <table class="table table-bordered text-center">
        <thead>
            <tr>
            @if(count($data->fatchData) > 0)
            @php $record = helper::singleParam($data->fatchData[0]->id, $data->businessline_id,2); @endphp
            @else
            @php $record = helper::singleParam($data->id, $data->businessline_id,1); @endphp   
            @endif
            

                <th rowspan="2">Sales Person</th>
                <th>Monthly Targets %</th>
                 @for($i=1;$i<=12;$i++)

                <th><input type="text" value="{{ !empty($record) ? round($record->{ strtolower(date('M',mktime(0, 0, 0, $i, 10))) }) :0 }}" name="montht[]" class="form-control actual montht{{ $data->id }}" onkeyup="onChangeMt({{ $data->id }})"></th>
                 @endfor
            </tr>
            <tr>
                <th>Yearly Targets</th>
                @for($i=1;$i<=12;$i++)
                <th>{{ Date("M",mktime(0, 0, 0, $i, 10)) }}</th>
                @endfor
            </tr>
        </thead>
        <tbody>
        @if(count($data->fatchData) > 0)

        
            @foreach($data->fatchData as $key=>$child)

           

            @php $record = helper::singleParam($child->id, $data->businessline_id, 2); @endphp
            <tr>
                <td>
                    <a href="{{ url('admin/sales-record-single',$child->id) }}">{{ $child->firstname.' '.$child->lastname }}</a>
                    {{ Form::hidden("userId[]",$child->id) }}
                    {{ Form::hidden("parentId",$data->businessline_id) }}
                    {{ Form::hidden("salesManagerId",$data->id) }}

                    {{ Form::hidden("childParent[]",$child->parent) }}
                    


                    

                     
                </td> 
                <td>
                    <input type="text" value="{{ !empty($record) ?  $record->total : 0 }}" name="yearlyTarget[{{$child->id}}]" class="yearlytarget{{ $data->id }} actual{{ $data->id }} form-control"  onkeyup="onChangeYt({{ $data->id }})">
                </td>
                @for($i=1;$i<=12;$i++)
                <td><input type="text" value="" name="monthsRecord[{{$child->id}}][]" readonly class="monthsRecord{{ $data->id }}{{ $key }} actual form-control"></td>
               
                @endfor
            </tr>
           
            @endforeach
            @else
            <tr>
                <td colspan="14">
                    <a href="javascript:void(0)" style="color: red; font-style: italic">{{ "No Sales Person assigned yet for respective business line and sales manager" }}</a>
                     
                </td> 
                 
            </tr>
            @endif
        </tbody>
    </table>

    

    @php $i++; @endphp
    

    
    <div class="form-group text-center">
        {{ Form::submit("Update Records", array("class"=>"btn btn-danger")) }}
    </div>

    </div>
{{ Form::close() }}

@endforeach
<script type="text/javascript">

    function calculate(keySm){

        $(".yearlytarget"+keySm).each(function(){

            var index = $(".yearlytarget"+keySm).index(this);

            var yearA = $(this).val();



            $(".montht"+keySm).each(function(){

                var index1 = $(".montht"+keySm).index(this);

                if($(this).val() != ''){
                    
                    monthA = $(this).val();

                    if(monthA > 0 && yearA > 0){
                        var amount = (monthA*yearA)/100;
                    }else{
                        var amount = 0;
                    }
                   
                    amount = amount.toFixed(2);
                    $(".monthsRecord"+keySm+index).eq( index1 ).val(amount);

                }

            });

        });

    }


function onChangeYt(keyTy){
$(".yearlytarget"+keyTy).change(function(){
var index = $(".yearlytarget"+keyTy).index(this);

//console.log("govind" + index);
var yearA = $(this).val();

$(".montht"+keyTy).each(function(){

    var index1 = $(".montht"+keyTy).index(this);

    if($(this).val() != ''){
        
        monthA = $(this).val();
        if(monthA > 0 && yearA > 0){
            var amount = (monthA*yearA)/100;
        }else{
            var amount = 0;
        }

        $(".monthsRecord"+keyTy+index).eq( index1 ).val(amount);

    }

});

});

}

function onChangeMt(keyTm){
   
total = 0;
$(".montht"+keyTm).each(function(){

    if($(this).val() != ''){

        total += parseInt($(this).val());

    }





if(total <= 100 ){


    var index = $(".montht"+keyTm).index(this);

    var monthA = $(this).val();

    $(".yearlytarget"+keyTm).each(function(){

        var index1 = $(".yearlytarget"+keyTm).index(this);

        if($(this).val() != ''){

            var yearA = $(this).val();

            if(monthA > 0 && yearA > 0){
                var amount = (monthA*yearA)/100;
            }else{
                var amount = 0;
            }
            

            $(".monthsRecord"+keyTm+index1).eq( index ).val(amount);

        }

    });

}else{

    alert("Month total should not be greater than 100");

    //$(this).val(0);
    return false;

}
});
}


    $(document).ready(function(){

        // calculate();

        $(".calculateMultipleSum").each(function(){

            var indexSm = $(".calculateMultipleSum").index(this);
            var keySm = $(this).val();
            console.log(">>>>>"+keySm);
            calculate(keySm);
        })

        
        
        // $(".yearlytarget").change(function(){

        //     var index = $(".yearlytarget").index(this);
        //     var yearA = $(this).val();

        //     $(".montht").each(function(){

        //         var index1 = $(".montht").index(this);

        //         if($(this).val() != ''){
                    
        //             monthA = $(this).val();
        //             if(monthA > 0 && yearA > 0){
        //                 var amount = (monthA*yearA)/100;
        //             }else{
        //                 var amount = 0;
        //             }

        //             $(".monthsRecord"+index).eq( index1 ).val(amount);

        //         }

        //     });

        // });

        // $(".montht").change(function(){

        //     total = 0;
        //     $(".montht").each(function(){

        //         if($(this).val() != ''){

        //             total += parseInt($(this).val());

        //         }

        //     });



        //     if(total <= 100 ){

            
        //         var index = $(".montht").index(this);

        //         var monthA = $(this).val();

        //         $(".yearlytarget").each(function(){

        //             var index1 = $(".yearlytarget").index(this);

        //             if($(this).val() != ''){

        //                 var yearA = $(this).val();

        //                 if(monthA > 0 && yearA > 0){
        //                     var amount = (monthA*yearA)/100;
        //                 }else{
        //                     var amount = 0;
        //                 }
                        

        //                 $(".monthsRecord"+index1).eq( index ).val(amount);

        //             }

        //         });

        //     }else{

        //         alert("Month total should not be greater than 100");

        //         $(this).val(0);
        //         return false;

        //     }

        // });


    });
</script>
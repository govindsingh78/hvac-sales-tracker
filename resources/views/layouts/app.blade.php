
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <!-- Scripts -->
    <!--  <script src="{{ asset('js/app.js') }}" defer></script> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
    <!-- Fonts --><!-- 
    <link rel="dns-prefetch" href="//fonts.gstatic.com"> -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Styles -->
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.js"></script>


    <script type="text/javascript">
      function deletePrompt(a) {
        var txt;
        var r = confirm("Are you want to delete this row ??");
        if (r == true) {

          window.location.href=a.getAttribute("link");

        }

      }


      function show_alert() {
        var txt;
        var r = confirm("Are you sure to delete selected row??");
        if (r == true) {
          return true;
        } else {
          return false;
        }

      }
    </script>
    <script src="https://code.highcharts.com/highcharts.js"></script>

 


<style type="text/css">
.highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

#container {
    height: 400px;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}
.accordion table{
  margin-left: 24px;
  border-left: 1px dashed #333;
}
.accordion table td{
  padding: 10px 4px;
}
</style>

</head>
<body>

 

         
    <div id="app">
        @if(Auth::check() && Auth::user()->role==1)

        <nav class="navbar bg-primary shadow-sm navbar-expand-lg ">
          <a class="navbar-brand" href="{{ url('/') }}">
            @if(!empty(Auth::user()->image))
            <img src="{{ asset(Auth::user()->image) }}" alt=" {{ config('app.name', 'Laravel') }}" style="height: 40px">
            @else
             {{ config('app.name', 'Laravel') }}
            @endif
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{ url('/admin') }}">Dashboard <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/admin/user-list') }}">Users</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.tracker') }}">Sales Target</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.records') }}">Sales Reports</a>
              </li>
              <li class="nav-item"><a class="nav-link" href="{{ url('admin/currency') }}">Currency Settings</a></li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
              </li>

            </ul>
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown ">
                        <a class="nav-link" href="{{ route('profile') }}">
                            {{ Auth::user()->firstname. ' '.Auth::user()->lastname }}</a>
                    </li>
                @endguest
            </ul>
          </div>
        </nav>

       @elseif(Auth::check() && Auth::user()->role == 2)

        <nav class="navbar bg-primary shadow-sm navbar-expand-lg ">
          <a class="navbar-brand" href="{{ url('/') }}">
            @if(!empty(Auth::user()->image))
              <img src="{{ asset(Auth::user()->image) }}" alt=" {{ config('app.name', 'Laravel') }}" style="height: 40px">
            @else
              {{ config('app.name', 'Laravel') }}
            @endif
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{ url('/salesmanager') }}">Dashboard <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/salesmanager/user-list') }}">Users</a>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="{{ route('manager.records') }}">Sales Reports</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
              </li>
            </ul>
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown ">
                        <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->firstname. ' '.Auth::user()->lastname }} 
                        </a>

                    </li>
                @endguest
            </ul>
          </div>
        </nav>

      @elseif(Auth::check() && Auth::user()->role == 3)

        @php $parentRR = helper::getparent(Auth::user()->parent); @endphp


        <nav class="navbar bg-primary shadow-sm navbar-expand-lg ">
          <a class="navbar-brand" href="{{ url('/') }}">
            @if(!empty($parentRR->image))
              <img src="{{ asset($parentRR->image) }}" alt=" {{ $parentRR->firstname.' '.$parentRR->lastname }}" style="height: 40px">
            @else
              {{ config('app.name', 'Laravel') }}
            @endif
        </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{ url('/salesperson') }}">Dashboard <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="{{ route('salesperson.records') }}">Sales Reports</a>
              </li>


              <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
              </li>
            </ul>
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown ">
                        <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->firstname. ' '.Auth::user()->lastname }} 
                        </a>

                    </li>
                @endguest
            </ul>
          </div>
        </nav>

       @endif


       <div class="py-5">
            @yield('content')
       </div>
       <svg display="none">
            <symbol id="folder-close" >
                <path fill="#FFA000" d="M40,12H22l-4-4H8c-2.2,0-4,1.8-4,4v8h40v-4C44,13.8,42.2,12,40,12z"></path><path fill="#FFCA28" d="M40,12H8c-2.2,0-4,1.8-4,4v20c0,2.2,1.8,4,4,4h32c2.2,0,4-1.8,4-4V16C44,13.8,42.2,12,40,12z"></path>
            </symbol>
            <symbol id="folder-open">
                <path fill="#FFA000" d="M38,12H22l-4-4H8c-2.2,0-4,1.8-4,4v24c0,2.2,1.8,4,4,4h31c1.7,0,3-1.3,3-3V16C42,13.8,40.2,12,38,12z"></path><path fill="#FFCA28" d="M42.2,18H15.3c-1.9,0-3.6,1.4-3.9,3.3L8,40h31.7c1.9,0,3.6-1.4,3.9-3.3l2.5-14C46.6,20.3,44.7,18,42.2,18z"></path>
            </symbol>
        </svg>

       <script>
         $(function() {
               $(".list-block").click(function(){

                  if($(this).attr('aria-expanded') == 'false'){
                    
                      $(this).find(".ficon").html('<use xlink:href="#folder-open"></use>');

                  }else{
                    $(this).find(".ficon").html('<use xlink:href="#folder-close"></use>');
                  }
               });

                $(".ficon").click(function(){
                  $(this).parent(".list-block").click(); 
               });
         });
         </script>
       <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

       <script>
         $(function() {
               $('#table').DataTable();
         });
         </script>
   </body>
</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body class="login">
<div class="container login-block">
    <div class="row justify-content-center">


        <div class="col-md-5">
          <div class="login-block mt-4">
      @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif


    <form method="POST" action="{{ url('resetaction/'.$otp) }}">
        @csrf
          <label class="title1 mb-4">{{ __('Reset Password') }}</label>
                
          @if (\Session::has('success'))
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('success') }}
            </div>
            @endIf
          @if (\Session::has('error'))
          <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ \Session::get('error') }}
            </div>
          @endif   
                <input type="hidden" name="one_time_password" value="{{ $otp }}">

                <div class="form-group">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    
                </div>

                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
                    
                </div>

               

                <div class="form-group mb-0">
                    
                        <button type="submit" class="btn btn-primary">
                            Reset Password
                        </button>

                </div>
                 <div class="form-group">
                        <a href="{{ url('/') }}">Login</a>
                </div>
                
            </form>
               
    </div>
</div>

      </div>
    </div>

</body>
</html>

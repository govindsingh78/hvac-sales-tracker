<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBusinessLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_business_lines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('business_line_id', 12);
            $table->integer('user_id', 12);
            $table->integer('status');
            $table->timestamps();
            $table->foreign('business_line_id')->references('id')->on('business_line_details');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_business_lines');
    }
}

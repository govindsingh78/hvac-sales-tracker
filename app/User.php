<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function fatchData(){

        return $this->hasMany('App\User','parent','id');

    }



   
/*
    public function parent()
    {
        return $this->belongsTo('App\User','parent')->with('parent');
    }

    public function children()
    {
        return $this->hasMany('App\User','parent')->with('children');
    }
*/
    public function record(){

        return $this->hasMany('App\SalesTracker','salesperson','id');
        
    }

    public function recordParam(){

        return $this->hasMany('App\RecordTracker','salesperson','id');
        
    }
    
}

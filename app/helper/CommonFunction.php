<?php
namespace App\Helper;

use App\User;
use Session;
use Config;
use Auth;
use Mail;
use File;
use DB;
use Route;
use App\SalesTracker;
use App\RecordTracker;
/**
* 
*/
class CommonFunction
{
	
	public static function LoggedUsers($roleUser = 1){

        $userdata = null;
        if(Auth::guard('admin')->check() == true && $roleUser == "admin"){
            
            $userdata = User::find(Auth::guard('admin')->id());

        }elseif(Auth::guard('broker')->check()  && $roleUser == "company"){

            $userdata = Broker::find(Auth::guard('broker')->id());

        }elseif(Auth::guard('underwriter')->check() && $roleUser == "member"){

            $userdata = Underwriter::find(Auth::guard('underwriter')->id());

        }elseif(Auth::guard('user')->check()){

            $userdata = Brokeruser::find(Auth::guard('user')->id());

        }
		

		return $userdata;
	}

    

	
	Public static function generateUnique($v)
    {
            $salt = substr(md5(uniqid(rand(), true)), 0, $v);

            return $salt;
    }

    public static function theExpert($string, $limit)
    {   

        $string = strip_tags($string);

        $length = strlen($string); 
        $strPos = strrpos(substr($string, 0,$limit), " ");

        $details = substr($string,0, $strPos).($length > $limit ? "..." : null);
        return html_entity_decode($details);

    }



    public static function createSlug($table,$string, $number = 1)
    {   

        if($number == 1){
            $slug = str_slug($string);            
        }else{
            $slug = str_slug($string.$number);            
        } 
       
        $record = DB::table($table)->where('slug', $slug)->first();
        
        if(!empty($record->slug) || empty($record->slug)){
            return $slug;  
        }else{
            return CommonFunction::createSlug($table,$string, ++$number); 
        }   
        
    }

    public static function createIDSlug($table,$string,$id, $number = 1)
    {   

        if($number == 1){
            $slug = str_slug($string);            
        }else{
            $slug = str_slug($string.$number);            
        } 
       
        $record = DB::table($table)->where('slug', $slug)->where("id",$id)->first();
        
        if(!empty($record->slug) || empty($record->slug)){
            return $slug;  
        }else{
            return CommonFunction::createSlug($table,$string, ++$number); 
        }   
        
    }

    public static function getExtension($v){
    	
        $file_ext = explode(".",$v);

        $file_ext = substr($v, strrpos($v, '.') + 1);

        return strtolower($file_ext);
    }
    

    Public static function isvalidFile($v)
    {
            
            $valid = array("jpg","jpeg","pdf","doc","xlsx","xlsm","xlsb","xls","png","gif","docm","docx","dot","dotm","dotx","csv");

            if(in_array($v, $valid)){
                return true;
            }
            return false;           

    }

    /**
    * This is a common function to send email.
    *
    * @param  int  $id
    */
    public static function sendEmail($data)
    {
        return $result = Mail::send($data['template'], $data, function($message) use($data) {
            $message->to($data['templateData']['email'], $data['templateData']['name'])->subject($data['subject']);

            /*
            * Checks if from address is set.
            */
            if (!empty($data['from'])){
                $message->from($data['from'], $data['fromName']);
            }
        });


    }


    /**
    * This is a common function to send email.
    *
    * @param  int  $id
    */
    
    public static function UploadMedia($files,$directory){

        $ext = CommonFunction::getExtension($files->getClientOriginalName());

        if(CommonFunction::isvalidFile($ext)==true){

            $path =public_path("/uploads/".$directory."/");

            if (!File::exists($path)) {

                File::makeDirectory($path, 0777, true, true);
            }

            $image_name = CommonFunction::generateUnique(8).".".$ext;

            $files->move($path , $image_name);

            $image = "/uploads/".$directory."/".$image_name;

            
            return $image;

        }

    }

    public static function Management($data){

       
        
        if(!empty($data['id'])){

            if($data['status'] == "Sales Manager"){
                unset($data['update']['parent']);
            }
           
            
            $update['updated_at'] = date('Y-m-d h:i:s');

            $test = DB::table($data['table'])->where("id",base64_decode($data['id']))->update($data['update']);
      

            return (strrpos($data['table'],"_") ? str_replace("_"," ", $data['table']) : ucfirst($data['table']))." updated successfully";

           

        }else{

            $data['update']['created_at'] = date('Y-m-d h:i:s');

            $data['update']['updated_at'] = date('Y-m-d h:i:s');

            DB::table($data['table'])->insert($data['update']);



            return (strrpos($data['table'],"_") ? str_replace("_"," ",ucfirst($data['table'])) : ucfirst($data['table']))." Add successfully";
        }
    
    }


    public static function getUserParent($table,$id){

        $data = DB::table($table)->select(DB::raw('CONCAT(`first_name`," ",`last_name`," - ", `email`) AS name'),"id")->where('id', $id)->orderBy('id',"desc")->first();

        if(!empty($data)){
            return $data->name;
        }else{
            return null;
        }

        

    }


    public static function getSalesPersonManager($salesManagerString){
        
        $salesManagerArr = explode(",", $salesManagerString);
        
        
        $arraySalesManager = User::select(DB::raw("CONCAT(firstname,' ',lastname) AS salesManagerName"))
        ->whereIn("id",$salesManagerArr)->where("role",2)->pluck('salesManagerName')->toArray();
        $salesManagerName = "";
        foreach($arraySalesManager as $value){
            $salesManagerName .= $value.", ";
        }


        if(!empty($arraySalesManager)){
            echo trim($salesManagerName, ', ');
        }else{
            return "N/A";
        }

        

    }
   
    public static function ytdVTarget($id,$person){

        $records = DB::table("salesrecord")->where("salesmanager",$id)->where("salesperson",$person)->where('year',date("Y"))->orderBy("id","desc")->get();

        $ytd = 0;
        $target = 0;
        $current = 0;

        $rrr =0;
        foreach ($records as $key => $value) {
            if($value->actual == null || (int)$value->actual ==0){
                
            }else{
                $rrr =1;
            }

            if($rrr >0){

                $newKey = $key-1;
                $ytd += (float)$value->actual;
                $target += (float)$value->tarrget;

            }
                
        }


        if($target > 0){
            return ($ytd / $target)*100;
        }else{
            return null;
        }
        

    }

    public static function ytdVTargetNP($id,$person){

        $records = DB::table("salesrecord")->where("salesmanager",$id)->where("salesperson",$person)->where('year',date("Y"))->orderBy("id","desc")->get();

        $ytd = 0;
        $target = 0;
        $current = 0;

        $rrr =0;
        foreach ($records as $key => $value) {
            if($value->actual == null || (int)$value->actual ==0){
                
            }else{
                $rrr =1;
            }

            if($rrr >0){

                $newKey = $key-1;
                $ytd += (float)$value->actual;
                $target += (float)$value->tarrget;

            }
                
        }


        if($target > 0 && $ytd >0){
            return ((float)$ytd / (float)$target)*100;
        }else{
            return null;
        }
        

    }

    public static function growthVLast($id,$person){


        $record = DB::table("salesrecord")->select("ykd","actual", "growthykd","year","targetykd","tarrget")
        ->where("salesmanager",$id)
        ->where("salesperson",$person)->orderBy("id","desc")->get();
      
        //dd($record[0]->actual);


        $year = date("Y") - 1;

        $growth = $ytd = [0];
        $rrr = 0;

        $actual = [];

        $trg = 0;
        foreach ($record as $key => $daa) {
            if($daa->year == date("Y")){
                 if($daa->actual == null || (int)$daa->actual ==0){
                    
                }else{
                   $rrr = $key;
                   break;
                }
            }
           
        }
        $trg = array();
        //$actual = array();
        foreach ($record as $key => $daa) {
            if($daa->year == date("Y")){
                $trg[] = $daa->tarrget;
                $actual[] = $daa->actual;
            }
        }

        



       // echo $rrr;
        if(isset($record[$rrr+12])){
            $ytd = (float)$record[$rrr+12]->ykd;
        }else{
            $ytd = 0;
        }
           if(isset($record[$rrr])){

            $growth = (float)$record[$rrr]->growthykd;
            $targetYtd = (float)$record[$rrr]->targetykd;
        //echo $growth;
        }else{
           $targetYtd = $growth = 0;
        }



        

        if($ytd!=0){

            //return [round((($growth) / ($ytd))*100),round($targetYtd),$rrr];
            return [round((($growth) / ($ytd))*100),array_sum($trg),$rrr,array_sum($actual)];

        }else{
            //dd(array_sum($actual));
            return [0,array_sum($trg),$rrr,array_sum($actual)];
           
        }
        

    }
    
    public static function singleRecords($userId){
        
        $datas = SalesTracker::where("salesperson",$userId)->get();

        $comPevious = $comCurrent = $actualCurrent = $actualPevious=[];

        foreach ($datas as $data) {
                
            if($data->year == Date("Y")-1){

                $actualPevious[] = round($data->actual);

                $comPevious[] = round($data->ykd);

            }
            if($data->year == Date("Y")){
                
                $actualCurrent[] = round($data->actual);

                $comCurrent[] = round($data->ykd);

            }

        }



        return view('admin.elements.singlerecord',compact('datas','actualPevious','comPevious'));
    }

    public static function getparent($id){

       return User::where("id",$id)->first();
    }

    public static function singleParam($userId, $businessId, $type){

        if($type == 1){

            $datas = RecordTracker::where("busines_id",$businessId)->first();

        }else{

            $datas = RecordTracker::where("busines_id",$businessId)->where("salesperson",$userId)->first();

        }

        return $datas;
    }


    public static function numformat($num){
      return number_format((float)$num, 2, '.', '');
    }


    public static function referencePrefix($id){
         $data = [

                    '2' => 'CAD',
                    '3' => 'CID',
                    '4' => 'TDS',
                    '5' => 'MBS',
                    '6' => 'MBN',
                    '7' => 'Dist',
                    '74' => 'MBA',
                ];
                if(isset($data[$id])){
                    return $data[$id];
                }else{
                    return 'Admin';
                }
                
    }
}

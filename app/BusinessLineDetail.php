<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessLineDetail extends Model
{
     

    protected $table = "business_line_details";

    protected $fillable = [
        'business_line', 'status', 'sort_order',
    ];

     
    
}

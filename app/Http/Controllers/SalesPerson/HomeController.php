<?php

namespace App\Http\Controllers\SalesPerson;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $datas = DB::table("salesrecord")->where("salesperson",Auth::user()->id)->get();

        $comCurrent = $actualCurrent = $comPevious = $actualPevious =[];

        foreach ($datas as $data) {
                
            if($data->year == Date("Y")-1){

                $actualPevious[] = round($data->actual);

                $comPevious[] = round($data->ykd);

            }
            if($data->year == Date("Y")){
                
                $actualCurrent[] = round($data->actual);

                $comCurrent[] = round($data->ykd);

            }

        }



        $salesActual = [ 0=> ['name'=>'Actual Previous Year','data'=>$actualPevious,'color'=>'#7cb5ec'],1=>['name'=>'Actual Current Year','data'=>$actualCurrent,'color'=>'#f33']];

        /* ----------- Commulitive data YKD -----------*/

        $salesYkd = [ 0=> ['name'=>'Previous Year YTD','data'=>$comPevious,'color'=>'#f33'],1=>['name'=>'Current Year YTD','data'=>$comCurrent,'color'=>'#7cb5ec']];


        return view('salesperson.dashboard',compact('salesActual','salesYkd','datas','actualPevious','comPevious'));

    }


}

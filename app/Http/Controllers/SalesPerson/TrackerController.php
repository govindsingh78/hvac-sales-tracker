<?php

namespace App\Http\Controllers\SalesPerson;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use helper;
use App\RecordLoad;
use File;
class TrackerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $datas = DB::table("salesrecord")->where("salesperson",Auth::user()->id)->get();


        foreach ($datas as $data) {
                
            if($data->year == Date("Y")-1){

                $actualPevious[] = round($data->actual);

                $comPevious[] = round($data->ykd);

            }
            if($data->year == Date("Y")){
                
                $actualCurrent[] = round($data->actual);

                $comCurrent[] = round($data->ykd);

            }

        }



        $salesActual = [ 0=> ['name'=>'Actual Previous Year','data'=>$actualPevious,'color'=>'#7cb5ec'],1=>['name'=>'Actual Current Year','data'=>$actualCurrent,'color'=>'#f33']];

        /* ----------- Commulitive data YKD -----------*/

        $salesYkd = [ 0=> ['name'=>'Previous Year YTD','data'=>$comPevious,'color'=>'#f33'],1=>['name'=>'Current Year YTD','data'=>$comCurrent,'color'=>'#7cb5ec']];


        return view('salesperson.salestracker',compact('salesActual','salesYkd','datas','actualPevious','comPevious'));

    }



    public function save(Request $request){

        if(!empty($request->id)){

            foreach ($request->id as $key => $id) {
                
                DB::table("salesrecord")->where("salesperson",Auth::user()->id)->where("id",$id)->update(['actual'=>$request->actualSales[$key],'ykd'=>$request->ytd[$key],'growthykd'=>$request->growthykd[$key],'backlog'=>$request->backlog[$key]]);

            }
        }

        Session::flash('success', "Record Updated Successfully");

        return back();

    }

    public function updateList(Request $request){

        if($request->type == 'actualSales'){

            DB::table("salesrecord")->where("salesperson",Auth::user()->id)->where("id",$request->id)->update(['actual'=>$request->records]);

        }elseif($request->type == 'backlog'){

            DB::table("salesrecord")->where("salesperson",Auth::user()->id)->where("id",$request->id)->update(['backlog'=>$request->records]);
        }   

    }


    public function records(Request $request){

        if($request->id != null)
        {
            $data = RecordLoad::find($request->id)->first();



            if(!empty($data->data) && File::exists(public_path($data->data))){

                unlink(public_path($data->data));
            }  

            RecordLoad::find($request->id)->delete();

            return back();

        }
        $records = RecordLoad::where("salesperson",Auth::user()->id)->orderBy("id","desc")->get();

        $years = RecordLoad::where("salesperson",Auth::user()->id)->orderBy("uploaded_year","desc")->groupBy("uploaded_year")->get();

        return view('salesperson.records',compact('records','years'));

    }

    public function recordManage(Request $request){

        $data = null;

        if($request->id != null)
        {

            $data = RecordLoad::where("id",$request->id)->first();

        }
        return view('salesperson.addrcords',compact('data'));

    }

    public function saveRcords(Request $request){

        $rules['year'] = "required|numeric";

        $rules['month'] = "required|numeric";

        if($request->id ==null){
            $rules['file'] = "required";
        }

        $this->validate($request,$rules);
 
        if(!empty($request->file('file'))){

                $files = $request->file('file');

                if(!empty($request->prev_file) && File::exists(public_path($request->prev_file))){

                    unlink(public_path($request->prev_file));

                }            

                $update['data'] =  helper::UploadMedia($files,'user');

                

        }

        $update['uploaded_year'] =  $request->year;
        $update['uploaded_month'] =  $request->month;
        $update['salesperson'] =  Auth::user()->id;
        $update['salesmanager'] =  Auth::user()->parent;

        
        

        if($request->id !=null){
            $record = helper::Management(['id'=>base64_encode($request->id),"table"=>"uploaded_report","update"=>$update]);
            Session::flash('success', "Record Updated Successfully");
        }else{

            if(!empty($this->ExistData($update))){

                $record = RecordLoad::where("salesperson",Auth::user()->id)->where("uploaded_year",$update['uploaded_year'])->where("uploaded_month",$update['uploaded_month'])->update($update);

            }else{
                $record = helper::Management(['id'=>$request->id,"table"=>"uploaded_report","update"=>$update]);
            }
            
            Session::flash('success', "Record Added Successfully");
        }
        
        return redirect(route("salesperson.records"));

    }


    public function ExistData($data){


        $records = RecordLoad::where("salesperson",Auth::user()->id)->where("uploaded_year",$data['uploaded_year'])->where("uploaded_month",$data['uploaded_month'])->orderBy("id","desc")->first();

        if(!empty($records)){
            return $records->id;
        }else{
            return Null;
        }
        
    }
}

<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Session;
use App\User;

class LoginController extends Controller
{   


    public function index()
    { 

        //echo Hash::make("Admin@123");



        if (Auth::guard('web')->check()){
            if(Auth::user()->role == 1){
                 return redirect('/admin/'); 
             }else if(Auth::user()->role == 2){
                 return redirect('/salesmanager/'); 
             }else if(Auth::user()->role == 3){
                 return redirect('/salesperson/'); 
             }
        } else {
            return view('auth/login');
        }

    }
    

    /**
     * Validate user
     */
    public function postaction(Request $request)
    {   
        
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $remember_me = $request->has('remember') ? true : false; 
        
        $credentials = [
            'username' => $request['username'],
            'password' => $request['password'],
            'status' => 1
        ];  


        if (Auth::guard('web')->attempt($credentials, $remember_me)) { 

             
             if(Auth::user()->role == 1){
                 return redirect('/admin/'); 
             }else if(Auth::user()->role == 2){
                 return redirect('/salesmanager/'); 
             }else if(Auth::user()->role == 3){
                 return redirect('/salesperson/'); 
             }

        } else {

            Session::flash('error', "Your Username or Password has not been recognised, please contact your account administrator to reset your password or create an account !");
            return back();  

        }
        
    }
    
   
    /**
     * This is used for logout
     */
    public function logout(Request $request)
    {
      Auth::guard('web')->logout();
        
      return redirect('/');
        
    }   


    public function resetActionpassword(Request $request){

        $request->validate(['password' => "required|confirmed|min:6"]);

        $ud = User::where("otp",$request->otp);
        $udate = $ud->update(['password'=>Hash::make($request->password)]);
        $fetch = $ud->first();

        if(!empty($fetch)){

            $remember_me = false;
            $credentials = [
                'otp'=> $request->otp,
                'password' => $request->password,
                'status' => 1
            ];  

            
            
            if (Auth::guard('web')->attempt($credentials, $remember_me)) {     
                $ud->update(['otp'=>'']);
                if(Auth::user()->role == 1){
                     return redirect('/admin/'); 
                 }else if(Auth::user()->role == 2){
                     return redirect('/salesmanager/'); 
                 }else if(Auth::user()->role == 3){
                     return redirect('/salesperson/'); 
                 }

                 
            }

        }else{
            Session::flash('error', "OTP Expired");
            return back();
        }

    }

     public function resetpassword(Request $request){

        $otp = $request->token;

        return view("reset",compact('otp')); 

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function showResetForm(Request $request, $token = null)
    {
        return view('reset')->with(
            ['token' => $token]
        );
    }


    public function resetActionpassword(Request $request){

        $request->validate(['password' => "required|confirmed|min:6","username"=>"required"]);

        $ud = User::where("username",$request->username);
        $udate = $ud->update(['password'=>Hash::make($request->password)]);
        $fetch = $ud->first();

        if(!empty($fetch)){

            $remember_me = false;
            $credentials = [
                'username'=> $request->username,
                'password' => $request->password,
                'status' => 1
            ];  

            
            
            if (Auth::guard('web')->attempt($credentials, $remember_me)) {     
                //$ud->update(['otp'=>'']);
                if(Auth::user()->role == 1){
                     return redirect('/admin/'); 
                 }else if(Auth::user()->role == 2){
                     return redirect('/salesmanager/'); 
                 }else if(Auth::user()->role == 3){
                     return redirect('/salesperson/'); 
                 }

                 
            }

        }else{
            Session::flash('error', "Username Not Match");
            return back();
        }

    }

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}

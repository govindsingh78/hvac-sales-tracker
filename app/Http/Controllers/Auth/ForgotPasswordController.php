<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Session;
use DB;
use File;
use App\helper\CommonFunction;
use Auth;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    
    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {

        $rules['username'] = 'required';

        $messagesd['username.required'] = "Please enter valid username";

        $this->validate($request,$rules,$messagesd);

        $user =  User::select("users.*",DB::raw("(select type from userrole where userrole.id = users.role) as usertype"))->where("username",$request->username)->first();

        $otp =  CommonFunction::generateUnique(5);

        $update['otp'] = $otp;

        

        if(!empty($user) && User::where("id",$user->id)->update(["otp"=>$otp])==true){
        
            $templateData['name'] = $user->firstname.' '.$user->lastname;
            $templateData['email'] = $user->email;
            $templateData['role'] = $user->usertype;
            $templateData['type'] = $user->role;

            $templateData['otp'] = $otp;

            $templateData['sitename'] = Config::get('constants.siteName');
            $emailData['templateData'] = $templateData;
            $emailData['template'] = 'mailtemplates.passwordsent';
            $emailData['subject'] = 'Reset Password'; 

            CommonFunction::sendEmail($emailData);

            return view("mailtemplates.passwordsent",compact('templateData'));

           /* Session::flash('status', 'We have generate token please check email.');
            return back();*/
        }else{
            Session::flash('error', 'Username not found!');
            return back();
        }
        
    }
    
}

<?php


namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Session;

class LoginController extends Controller
{   


    public function index()
    { 

        //echo Hash::make("Admin@123");

        if (Auth::guard('web')->check()){
            return redirect('/home');       
        } else {
            return view('auth/login');
        }

    }
    

    /**
     * Validate user
     */
    public function postaction(Request $request)
    {   

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $remember_me = $request->has('remember') ? true : false; 
        
        $credentials = [
            'username' => $request['username'],
            'password' => $request['password'],
            'status' => 1
        ];  


        if (Auth::guard('web')->attempt($credentials, $remember_me)) {     

            return redirect('/');  

        } else {

            Session::flash('error', "Your Username or Password has not been recognised, please contact your account administrator to reset your password or create an account !");
            return back();  

        }
        
    }
    
   
    /**
     * This is used for logout
     */
    public function logout(Request $request)
    {
      Auth::guard('web')->logout();
        
      return redirect('/');
        
    }   

}

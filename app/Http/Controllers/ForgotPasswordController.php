<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Session;
use DB;
use File;
use helper;
use Auth;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    public function resetEmailLink(Request $request)
    {

        $rules['username'] = 'required';

        $messagesd['username.required'] = "Please enter valid username";

        $this->validate($request,$rules,$messagesd);

        $user =  User::select("users.*",DB::raw("(select type from userrole where userrole.id = users.role) as usertype"))->where("username",$request->username)->first();

        $otp =  helper::generateUnique(5);

        $update['otp'] = $otp;

        

        if(!empty($user) && User::where("id",$user->id)->update(["otp"=>$otp])==true){
            

            
            $templateData['name'] = trim($user->firstname.' '.$user->lastname);
            $templateData['email'] = $user->email;


            $templateData['role'] = $user->usertype;
            $templateData['type'] = $user->role;

            $templateData['otp'] = $otp;

            $templateData['sitename'] = Config::get('constants.siteName');
            $emailData['templateData'] = $templateData;
            $emailData['template'] = 'mailtemplates.passwordsent';
            $emailData['subject'] = 'Reset Password'; 

            helper::sendEmail($emailData);

            Session::flash('status', 'We have generate token please check email.');
            return back();
        }else{
            Session::flash('error', 'Username not found!');
            return back();
        }
        
    }
    
}

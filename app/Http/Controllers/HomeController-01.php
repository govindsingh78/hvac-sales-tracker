<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Config;
use helper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function salesmanagers(Request $request){

        $users = User::where("role",2)->get();

        echo '<div class="form-group"><label for="salesmanager" class="form-label">Sales Manager</label>';

        echo '<select class="form-control" name="feild[parent]">';

        foreach ($users as $key => $value) {
            echo '<option value="'.$value->id.'">'.$value->firstname.' '.$value->lastname.'</option>';
        }

        echo '</select>';

        echo '</div>';

    }


    public function reminderemail(){

        $record = User::where('role','=',2)->where("status",1)->get();

        foreach ($record as $key => $value) {
            

            $templateData['name'] = $value->firstname.' '.$value->lastname;
            $templateData['email'] = $value->email;


            $templateData['sitename'] = Config::get('constants.siteName');
            $emailData['templateData'] = $templateData;
            $emailData['template'] = 'mailtemplates.reminder';
            $emailData['subject'] = 'Sales update reminder email '; 

            helper::sendEmail($emailData);

        }


    }


    public function reminderAdmin(){

        $templateData['name'] = "Super User";
        $templateData['email'] = "MukulS@lucidux.com";


        $templateData['sitename'] = Config::get('constants.siteName');
        $emailData['templateData'] = $templateData;

        $settings = DB::table("currency")->where("id",1)->first();


        $managers = User::with('fatchData')->where("role",2)->where("status",1)->get();
        
        $settings = DB::table("currency")->where("id",1)->first();

        foreach ($managers as $key => $dataS) {
            $ytdTarget = $target = $growth = 0;
            $ids = null;
            foreach ($dataS->fatchData as $key => $child) {

                $reco = helper::growthVLast($dataS->id,$child->id);
                $growth += $reco[0];
                $target += $reco[1];

                $ytdTarget += helper::ytdVTargetNP($dataS->id,$child->id);
                
                $ids[] = $child->id;
            }

            $datas[] = ['name'=>$dataS->firstname." ".$dataS->lastname,"target"=>$target,"ytdTarget"=>round($ytdTarget),"growth"=>$growth,"id"=>$dataS->id,"count"=>count($ids)];


        }

        return view('mailtemplates.summeryadmin',compact('datas','settings','templateData'));

    }

    public function reminderTMemail(){

        $templateData['name'] = "Super User";
        $templateData['email'] = "MukulS@lucidux.com";


        $templateData['sitename'] = Config::get('constants.siteName');
        $emailData['templateData'] = $templateData;

        $managers = User::with('fatchData')->where("role",2)->where("id",4)->where("status",1)->get();
        
        $settings = DB::table("currency")->where("id",1)->first();

        foreach ($managers as $key => $dataS) {

            $templateData['name'] = $dataS->firstname." ".$dataS->lastname;

            return view('mailtemplates.summeryEngineer',compact('dataS','settings','templateData'));

        }


        
       
    }

}

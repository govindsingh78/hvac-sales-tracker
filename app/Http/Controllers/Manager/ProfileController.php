<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\helper\CommonFunction;
use Session;
use Auth;
use File;
use DB;
use App\User;

class ProfileController extends Controller
{
    
    public function index()
    {
        $user = User::select("users.*",DB::raw("(select type from userrole where users.role = userrole.id) as role"))->where("id",Auth::user()->id)->first();
        return view('manager.profile',compact("user"));
    }

    public function save(Request $request){

        foreach ($request->feild as $key => $value) {

                    
            if(strrpos($key, "_") > 0){
                $messagesd['feild.'.$key.".required"] = ucfirst(str_replace("_", " ", $key))." required";
            }else{
                $messagesd['feild.'.$key.".required"] = ucfirst($key)." required";
            }

            
            $rules['feild.'.$key] = "required";
          
            if($key=="password"){
                $update[$key]= Hash::make($value);
            }else{
                $update[$key]= $value;
            }
            
        }

        $this->validate($request,$rules,$messagesd);

        if(isset($request->removeImage)){

            if(!empty($request->prev_image) && File::exists(public_path($request->prev_image))){

                unlink(public_path($request->prev_image));

            }   
            $update['image'] = "";

            
        }else{

            if(!empty($request->file('image'))){

                $files = $request->file('image');

                if(!empty($request->prev_image) && File::exists(public_path($request->prev_image))){

                    unlink(public_path($request->prev_image));

                }            

                $update['image'] =  CommonFunction::UploadMedia($files,'user');

            }

        }



        if(!empty(Auth::user())){

            $record = CommonFunction::Management(['id'=>base64_encode(Auth::user()->id),"table"=>"users","update"=>$update]);

        }


        Session::flash('success', $record);

        return redirect('salesmanager/profile');

    }


    
}

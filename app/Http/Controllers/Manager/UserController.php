<?php
namespace App\Http\Controllers\Manager;

use App\helper\CommonFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;
use File;
use Auth;
use App\User;
use App\BusinessLineDetail;
use App\UserBusinessLine;
/**
* 
*/
class UserController extends Controller
{
	
	public function index(Request $request){

		$data['list'] = User::select("users.*",DB::raw("(select type from userrole where users.role = userrole.id) as userrole"))->where("role","=",3)->orderBy("id","desc")->get();
		
		$newArr = [];
		foreach($data['list'] as $key => $user){
			$parentCheck = explode(",",$user->parent);

			
			if(in_array(Auth::user()->id, $parentCheck)){
				
				$response = DB::table('business_line_details as bld')
				->select('bld.id as businessline_id','bld.business_line as businessline_name')
				->join('users_business_lines as ubl', 'ubl.business_line_id', '=', 'bld.id')
				->join('users as u', 'u.id', '=', 'ubl.user_id')
				->where('ubl.user_id', $user->id)
				->where('ubl.status', 0)
				->get();

				 

				$user->businessLine = $response;
				$newArr['list'][$user->id]['businessLine'] = $response; 
				$newArr['list'][$user->id] = $user;
				
				
			} 
		}

		 
		
		return view("manager.users.user-list")->with('data',$newArr);

	}

	public function create(Request $request){

		
		
		$data = [];

		
		//$data['parent'] = User::all();
		
		$roles = DB::table("userrole")->where("id","!=","1")->where("id","!=","2")->pluck('type','id');


		$usersR = User::select(DB::raw("CONCAT(`firstname`,' ',`lastname`) 
		as fullname"),"id","firstname","lastname")->where('status',1)
		->where("role",2)->where("id", Auth::user()->id)->get();


		

		foreach ($usersR as $key => $record) {
			if(!empty($record->fullname)){
				$data['parent'][$record->id] = $record->fullname;
			}else{
				$data['parent'][$record->id] = $record->firstname;
			}
			
		}

		 //dd($data['parent']);


		if(!empty($request->id)){

			$data['record'] = User::where("id",base64_decode($request->id))->first();

		}

		
		//dd($data['record']);

		return view("manager.users.user-create",compact("roles"))->with('data',$data);


	}

	public function store(Request $request){
	
		if(!empty($request->feild)){

			
				$messagesd["feild.email.regex"] = "Email not valid";


				if($request->salespersonbuisnessline == ''){
					$rules['salespersonbuisnessline[]'] = "required";
				}

				foreach ($request->feild as $key => $value) {

					if($key != "company"){

						if(strrpos($key, "_") > 0){
							$messagesd['feild.'.$key.".required"] = ucfirst(str_replace("_", " ", $key))." required";
						}else{
							$messagesd['feild.'.$key.".required"] = ucfirst($key)." required";
						}

						if(empty($request->id) || $this->isBroker($request->feild['username'],$request->id) == false){

							if($key == 'username'){
								$rules['feild.'.$key] = "required|unique:users,username";
							}elseif($key == 'email'){
								$rules['feild.'.$key] = "required|regex:/^.+@.+$/i";
							}
							else{
								$rules['feild.'.$key] = "required";
							}
						}else{


						if($key == 'email'){
								$rules['feild.'.$key] = "required|regex:/^.+@.+$/i";
							}
							else{
								$rules['feild.'.$key] = "required";
							}

						}

					}
					
					if($key=="password"){
						$update[$key]= Hash::make($value);
					}else{
						$update[$key]= $value;
					}
					
				}


				

				$this->validate($request,$rules,$messagesd);

				if(empty($this->isExist(Auth::user()->id,"created_by"))){
					$update['created_by'] = Auth::user()->id;
				}
				

				if(isset($request->removeImage)){

		        	if(!empty($request->prev_image) && File::exists(public_path($request->prev_image))){

		                unlink(public_path($request->prev_image));

		            }   
		            $update['image'] = "";

		            
		        }else{

		        	if(!empty($request->file('image'))){

				            $files = $request->file('image');

				            if(!empty($request->prev_image) && File::exists(public_path($request->prev_image))){

				                unlink(public_path($request->prev_image));

				            }            

				            $update['image'] =  CommonFunction::UploadMedia($files,'user');

				        }

		        }
			   
				
				//dd($update);


				if(!empty($request->id)){

					$id= base64_decode($request->id);
					

					if($update['role'] == 3){
						//call a function to save business line in case existing user
						$this->saveBusinessLine($id, $request, $update);
					}

					

					Session::flash('success', "Record Updated successfully");
					$record = CommonFunction::Management(['id'=>$request->id,"table"=>"users","update"=>$update, "status"=>"Sales Manager"]);

				}else{

					$role = DB::table("userrole")->select("type")->where("id",$update['role'])->first();

					$otp =  CommonFunction::generateUnique(5);

					$update['otp'] = $otp;
					
					$templateData['name'] = $update['firstname'].' '.$update['lastname'];
		            $templateData['email'] = $update['email'];
		            $templateData['role'] = $role->type;
		            $templateData['type'] = $update['role'];

		            $templateData['otp'] = $otp;
		            $templateData['message'] = "Your Account Created";
		            $templateData['sitename'] = Config::get('constants.siteName');
		            $emailData['templateData'] = $templateData;
		            $emailData['template'] = 'mailtemplates.accountcreate';
		            $emailData['subject'] = 'your account Created'; 

					//CommonFunction::sendEmail($emailData);
					

					$record = DB::table('users')->insertGetId($update);

					if($update['role'] == 3){
						//call a function to save business line in case new user created
						$this->saveBusinessLine($record, $request, $update);
					}
					

					 

					Session::flash('success', "Record added successfully");
				
				}

				

			

			return redirect('salesmanager/user-list');

		}

	}



	public function saveBusinessLine($user_id, $request, $update){

		//  dd($request->salespersonbuisnessline);
		if($update['role'] == 3){
			$arrayBusinessLine = $request->salespersonbuisnessline;
		} 



		//  Inactive if existing busines line
		 //UserBusinessLine::where('user_id','=',$user_id)->update(['status'=>1]);
		 
		 UserBusinessLine::where('user_id', '=', $user_id)->delete();

		foreach($arrayBusinessLine as $key => $businessLineId){

			
			// check if user_id and business line id exist 
			$checkCount = UserBusinessLine::where('user_id', '=', $user_id)->where('business_line_id', '=', $businessLineId)->count();

			if($checkCount > 0){
				// update status = 0 active 
				UserBusinessLine::where('user_id','=',$user_id)->where('business_line_id', '=', $businessLineId)->update(['status'=>0]);

			}else{
				// insert fresh entry
				$saveData = array('user_id'=>$user_id, 'business_line_id'=>$businessLineId);
				$record = DB::table('users_business_lines')->insert($saveData);
				
			}


			if($update['role'] == 3){
				
				if(empty($request->id)){
					for($i=1;$i<=12;$i++){

						$salesrecord = ['id'=>'','actual'=>'0','tarrget'=>'0','ykd'=>'0','targetykd'=>'0','growthykd'=>'0','backlog'=>'0',
						'month'=>$i,'year'=>date("Y"),'percent'=>'0','salesperson'=>$user_id,'salesmanager'=>$update['parent']];
	
						DB::table('salesrecord')->insert($salesrecord);
					}
				}

				 
			}

	}

 

}

	public function status(Request $request){

		if(!empty($request->id)){

			$record = User::where('id','=',base64_decode($request->id))->first();

			if($record->status==1){

				User::where('id','=',base64_decode($request->id))->update(['status'=>0]);

				Session::flash('success', "User inActive successfully");

			}elseif($record->status==0){

				User::where('id','=',base64_decode($request->id))->update(['status'=>1]);

				Session::flash('success', "User Active successfully");

			}

			return redirect('salesmanager/user-list');

		}


	}

	public function password(Request $request){

		if(!empty($request->id)){

		

			if($request->password == $request->cpassword){

				User::where('id',base64_decode($request->id))->update(['password'=>Hash::make($request->password)]);

				Session::flash('success', 'Password Updated!');

			}else{

				Session::flash('error', "Password Not Match");

			}

			return redirect('salesmanager/user-list');

		}
	

	}

	public function isBroker($email,$id){

		$record = User::where('username',$email)->where('id','=',base64_decode($id))->first();

		if(!empty($record)){

			return true;

		}else{

			return false;

		}
	}

	public function isExist($id,$feild){

		$record = User::where('id',''!='',$id)->first();

		$null = null;

		if(!empty($record)){

			return $record->$feild;

		}else{

			return $null;

		}
	}


}
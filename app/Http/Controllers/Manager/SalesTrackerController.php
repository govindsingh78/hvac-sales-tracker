<?php
namespace App\Http\Controllers\Manager;

use App\helper\CommonFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;
use File;
use Auth;
use App\User;
use App\RecordLoad;
/**
* 
*/

class SalesTrackerController extends Controller
{

    public function charts(){

        $data = User::with("record")->where("parent",Auth::user()->id)->orderBy("id","asc")->get();
        
        $recordList = null;
        

        foreach ($data as $key => $single) {

            $person = $single->firstname.' '.$single->lastname;

            if(!empty($single->record)){

                $comPevious = $comCurrent = $actualCurrent = $actualPevious=[];

                foreach ($single->record as $data) {
                    
                    if($data->year == Date("Y")-1){

                        $actualPevious[] = round($data->actual);

                        $comPevious[] = round($data->ykd);

                    }
                    if($data->year == Date("Y")){
                        
                        $actualCurrent[] = round($data->actual);

                        $comCurrent[] = round($data->ykd);

                    }

                }


                $salesActual = [ 0=> ['name'=>'Actual Previous Year','data'=>$actualPevious,'color'=>'#7cb5ec'],
                1=>['name'=>'Actual Current Year','data'=>$actualCurrent,'color'=>'#f33']];

                /* ----------- Commulitive data YKD -----------*/

                $salesYkd = [ 0=> ['name'=>'Previous Year YTD','data'=>$comPevious,'color'=>'#7cb5ec'],
                1=>['name'=>'Current Year YTD','data'=>$comCurrent,'color'=>'#f33']];

                 
                $recordList[] = ['person'=>$person,'salesActual'=>$salesActual,'salesYkd' =>$salesYkd,'id'=>$single->id];
            }


        }


        return view('manager.trackerchart',compact('recordList'));

    }

     public function records(){

         $records = RecordLoad::select("uploaded_report.*",DB::raw("(select concat(COALESCE(`firstname`,''),' ',COALESCE(`lastname`,'')) from users where users.id = uploaded_report.salesperson) as salesuser"))->where("salesmanager",Auth::user()->id)->orderBy("id","desc")->get();

        //$records = RecordLoad::where("salesmanager",Auth::user()->id)->orderBy("id","desc")->get();
         $years = RecordLoad::where("salesmanager",Auth::user()->id)->orderBy("uploaded_year","desc")->groupBy("uploaded_year")->get();
        return view('manager.records',compact('records','years'));

    }

	public function single($userId){

        $personRecord = User::with("record")->where("id",$userId)->first();

		$datas = DB::table("salesrecord")->where("salesperson",$userId)->where("salesmanager",$personRecord->parent)->get();


		$comPevious = $comCurrent = $actualCurrent = $actualPevious=[];

        foreach ($datas as $data) {
                
            if($data->year == Date("Y")-1){

                $actualPevious[] = round($data->actual);

                $comPevious[] = round($data->ykd);

            }
            if($data->year == Date("Y")){
                
                $actualCurrent[] = round($data->actual);

                $comCurrent[] = round($data->ykd);

            }

        }



        $salesActual = [ 0=> ['name'=>'Actual Previous Year','data'=>$actualPevious,'color'=>'#7cb5ec'],
        1=>['name'=>'Actual Current Year','data'=>$actualCurrent,'color'=>'#f33']];

        /* ----------- Commulitive data YKD -----------*/

        $salesYkd = [ 0=> ['name'=>'Previous Year YTD','data'=>$comPevious,'color'=>'#7cb5ec'],
        1=>['name'=>'Current Year YTD','data'=>$comCurrent,'color'=>'#f33']];


        return view('manager.tracker',compact('salesActual','salesYkd','datas','actualPevious','comPevious','personRecord'));

	}


    public function save(Request $request){


        // dd($request);
        if(!empty($request->id)){

            foreach ($request->id as $key => $id) {
                
                DB::table("salesrecord")->where("id",$id)->where("year",Date("Y"))->update(['actual'=>$request->actualSales[$key],'ykd'=>$request->ytd[$key],'growthykd'=>$request->growthykd[$key],'backlog'=>$request->backlog[$key]]);

            }
        }

        Session::flash('success', "Record Updated Successfully");

        return back();
    }




}
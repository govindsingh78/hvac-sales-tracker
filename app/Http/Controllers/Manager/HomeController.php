<?php

namespace App\Http\Controllers\Manager;
use App\BusinessLineDetail;
use App\UserBusinessLine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         $datas1 = User::with('fatchData')->where("role",2)->where("id",Auth::user()->id)->get();
        // dd($datas);
        
        $datas = [];
        $response = DB::table('business_line_details as bld')
        ->select('u.id as salesmanager_id','bld.id as businessline_id','bld.business_line as businessline_name')
        ->join('users_business_lines as ubl', 'ubl.business_line_id', '=', 'bld.id')
		->join('users as u', 'u.id', '=', 'ubl.user_id')
		->where('u.role', 2)
		->where('u.id', Auth::user()->id)
        ->where('bld.status', 0)
		->orderBy('bld.sort_order', 'ASC')
		->get();
		$arrayBusinessLine = [];
		foreach($response as $key => $val){
			$arrayBusinessLine[$val->businessline_id][] = $val;
		}
	 		 
		foreach($arrayBusinessLine as $key => $businessLineArr){
		   	
			foreach($businessLineArr as $k => $blArr){
                
                
                // $datas[$blArr->businessline_id]['business_line_name'] = $blArr->businessline_name;

                $responseSalesPers = DB::table('users as u')
				->select('u.*')
				->join('users_business_lines as ubl', 'ubl.user_id', '=', 'u.id')
				->where('u.role', 3)
				->where('ubl.business_line_id', $blArr->businessline_id)
				->where('ubl.status', 0)
				->orderBy('u.hod', 'ASC')
				->get();
				$i = 0;
				
				foreach ($responseSalesPers as $key => $child) {
					$parentArr = explode(",", $child->parent);
					if(in_array(Auth::user()->id, $parentArr)){
						$vari = $responseSalesPers[$key];
						unset($responseSalesPers[$key]);
						$responseSalesPers[$i] = $vari;
						
						$i++;
					}else{
						unset($responseSalesPers[$key]);
					}

					// $datas[$k]['fatchData'] = $responseSalesPers;
					
                }
                


                $blArr->fatchData = $responseSalesPers;
                $blArr->id = Auth::user()->id;
                $datas[$blArr->businessline_id] = $blArr;
                
                // $getSalesPerson = DB::table('business_line_details as bld')
                // ->select('u.*')
                // ->join('users_business_lines as ubl', 'ubl.business_line_id', '=', 'bld.id')
                // ->join('users as u', 'u.id', '=', 'ubl.user_id')
                // ->where('ubl.business_line_id', $blArr->businessline_id)
                // ->where('u.parent', $blArr->salesmanager_id)
                // ->where('u.role', 3)
                // ->where('bld.status', 0)
                // ->orderBy('bld.sort_order', 'ASC')
                // ->get();
                // $blArr->fatchData = $getSalesPerson;
                // $blArr->id = $blArr->salesmanager_id;
                

                // $datas[$blArr->businessline_id] = $blArr;


                 





                

                // dump($resp);
                
			}

        }

        //  dd($datas);
        
        $settings = DB::table("currency")->where("id", 1)->first();
        
        return view('manager.dashboard',compact('datas','settings'));
    }


}

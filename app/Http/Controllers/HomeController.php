<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\BusinessLineDetail;
use App\UserBusinessLine;
use Config;
use helper;
use DB;
 
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  /*  public function __construct()
    {
        $this->middleware('auth');
    }
*/
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function businessLineSalesPerson(Request $request){

        $businessLines = BusinessLineDetail::where("status",0)->orderby("sort_order", "ASC")->get(); 
        $arraySalesPersonBusinessLine = UserBusinessLine::where("user_id",base64_decode($request->user_id))->where("status",0)
        ->pluck('business_line_id')->toArray();
        echo '<div class="form-group"><label for="businessline" class="form-label">Business Line</label>';

        $roleData = User::where("id", base64_decode($request->user_id))->first();
      
        foreach ($businessLines as $key => $value) {
            if (in_array($value->id, $arraySalesPersonBusinessLine) && $request->roleChanged == 0)
            {
                $checked = "checked";
                

            }else{
                $checked = "";
            }
            echo '<div class="col-md-12"><input type="radio" class="salesPersonBusiness" name="buisnessline[]"  id="buisnessline'.$value->id.'" value="'.$value->id.'"   '.$checked.'>&nbsp;&nbsp;'.$value->business_line.'</div>';
        }
        echo '</div>';
    }

    

    public function salesManagerBusinessLineData(Request $request){

        

        $salesManager = DB::table('users_business_lines')
        ->leftJoin('users', 'users.id', '=', 'users_business_lines.user_id')
        ->where('users_business_lines.business_line_id', $request->bussiness_id)
        ->where("users_business_lines.status",0)
        ->where("users.role",2)
        ->orderby("users.id", "ASC")->get();


        //dd(base64_decode($request->user_id));
        if(!empty($request->user_id)){
            $getParent = User::where('id', '=', base64_decode($request->user_id))->first();
            $getParentArr = explode(",", $getParent->parent);
        }else{
            $getParentArr = [];
        }
        

        // dd($salesManager);

        echo '<div class="form-group"><label for="salesmanager" class="form-label">Sales Manager</label>';

        echo '<select class="form-control" name="parent[]" id="parent_id" multiple style="height: 100px; overflow-y: scroll">';
        echo '<option>Select Manager</option>';
        $prevSalesManager = "";
        foreach ($salesManager as $key => $value) {
            $selected = "";
            if (in_array($value->id, $getParentArr)){ 
                $prevSalesManager .= $value->id.",";
                $selected = "selected";
             }
            echo '<option value="'.$value->id.'" '.$selected.' class="highlight'.$selected.'">'.$value->firstname.' '.$value->lastname.'</option>';
        
            
        
        }
        $prevSalesManager = trim($prevSalesManager, ",");
        echo '</select>';
        echo '</div> <input type="hidden" name="prevSalesManager" id="prevSalesManager" value="'.$prevSalesManager.'">';

    }


    public function salesmanagers(Request $request){

        $users = User::where("role",2)->get();

        echo '<div class="form-group"><label for="salesmanager" class="form-label">Sales Manager</label>';

        echo '<select class="form-control" name="feild[parent]" id="parent_id">';
        echo '<option>Select Manager</option>';
        foreach ($users as $key => $value) {
            $selected = "";
            if($request->parent_id == $value->id){ $selected = "selected"; }

            echo '<option value="'.$value->id.'" '.$selected.'>'.$value->firstname.' '.$value->lastname.'</option>';
        }

        echo '</select>';

        echo '</div>';

    }


    public function businessLineSalesManagers(Request $request){

        $arraySalesManagerBusinessLine = UserBusinessLine::where("user_id", base64_decode($request->user_id))->where("status",0)->pluck('business_line_id')->toArray();

        $businessLines = BusinessLineDetail::where("status",0)->orderby("sort_order", "ASC")->get();

        $roleData = User::where("id", base64_decode($request->user_id))->first();

          

        echo '<div class="form-group"><label for="businessline" class="form-label">Business Line</label>';
      
        foreach ($businessLines as $key => $value) {

        if (in_array($value->id, $arraySalesManagerBusinessLine) && $request->roleChanged == 0)
        {
            $checked = "checked";
        }else{
            $checked = "";
        }

            echo '<div class="col-md-12"><input type="checkbox" name="buisnessline[]"  id="buisnessline'.$value->id.'" value="'.$value->id.'"   '.$checked.'>&nbsp;&nbsp;'.$value->business_line.'</div>';
        }
 

        echo '</div>';

       

    }

    public function salesPersonBusinessLineData(Request $request){

        // dd($request);

        $arraySalesPersonBusinessLine = UserBusinessLine::where("user_id",base64_decode($request->user_id))->where("status",0)->pluck('business_line_id')->toArray();

        $businessLinesSalesPerson = DB::table('users_business_lines')
        ->where("users_business_lines.status",0)
        ->where("users_business_lines.user_id",$request->id)
        ->leftJoin('business_line_details', 'business_line_details.id', '=', 'users_business_lines.business_line_id')
        ->orderby("business_line_details.sort_order", "ASC")->get();
        
        if(count($businessLinesSalesPerson) > 0){
        echo '<div class="form-group"><label for="businessLinesSalesPerson" class="form-label">Business Line</label>';
        

        
        $textMsg = "";
        foreach ($businessLinesSalesPerson as $key => $value) {
            if (in_array($value->id, $arraySalesPersonBusinessLine))
            {
                $checked = "checked";
                $disabled = "";
                 
            }else{
                $checked = "";
                $disabled = $request->user_id != '' ? 'disabled': '';
                $textMsg =  $request->user_id != '' ? '<div class="alert danger">Sales Person can not alter Business Line once assigned !!</div>': '';
                    
            }

            echo '<div class="col-md-12"><input type="radio" name="salespersonbuisnessline[]"  id="salespersonbuisnessline'.$value->id.'" 
            value="'.$value->id.'" '.$checked.' '.$disabled.'>&nbsp;&nbsp;'.$value->business_line.'</div>';
        }
 

        echo '</div>';

        echo $textMsg;
       


    }
        else{
            echo '<div class="alert alert-danger alert-sm">Sorry ! no business line for this Sales Manager</div>';
        }

           


       

    }


    

    


    public function reminderemail(){

        $record = User::where('role','=',2)->where("status",1)->get();

        foreach ($record as $key => $value) {
            

            $templateData['name'] = $value->firstname.' '.$value->lastname;
            $templateData['email'] = $value->email;


            $templateData['sitename'] = Config::get('constants.siteName');
            $emailData['templateData'] = $templateData;
            $emailData['template'] = 'mailtemplates.reminder';
            $emailData['subject'] = 'Sales update reminder email '; 

            helper::sendEmail($emailData);

        }


    }

    public function reminderAdmin(){

        $templateData['name'] = "Super User";

        $templateData['sitename'] = Config::get('constants.siteName');

        $settings = DB::table("currency")->where("id",1)->first();


        $managers = User::with('fatchData')->where("role",2)->where("status",1)->get();
        
        $settings = DB::table("currency")->where("id",1)->first();

        foreach ($managers as $key => $dataS) {


            $ddata = $this->TrackerForAdRecord($dataS->id);

            $ddata['name'] = $dataS->firstname." ".$dataS->lastname;

            $ddata['id'] = $dataS->id;

            $datas[] = $ddata;

            /*$ytdTarget = $target = $growth = 0;
            $ids = null;
            foreach ($dataS->fatchData as $key => $child) {

                $reco = helper::growthVLast($dataS->id,$child->id);
                $growth += $reco[0];
                $target += $reco[1];

                $ytdTarget += helper::ytdVTargetNP($dataS->id,$child->id);
                
                $ids[] = $child->id;
            }

            $datas[] = ['name'=>$dataS->firstname." ".$dataS->lastname,"target"=>$target,"ytdTarget"=>round($ytdTarget),"growth"=>$growth,"id"=>$dataS->id,"count"=>count($ids)];
            */


        }

        $users = User::where("role",1)->where('status',1)->get();

        foreach ($users as $key => $value) {
        	
        	//$templateData['email'] = "ShaktiP@lucidux.com";
        	$templateData['email'] = $value->email;
	        $templateData['datas'] = $datas;

	        $templateData['sitename'] = Config::get('constants.siteName');
	        $emailData['templateData'] = $templateData;
	        $emailData['template'] = 'mailtemplates.summeryadmin';
	        $emailData['subject'] = 'Sales Update Monthly Report'; 

	        helper::sendEmail($emailData);
        }
        

    }

    public function reminderTMemail(){

        //$managers = User::with('fatchData')->where("role",2)->where("id",4)->where("status",1)->get();
        
        $managers = User::with('fatchData')->where("role",2)->where("status",1)->get();

        $settings = DB::table("currency")->where("id",1)->first();

        foreach ($managers as $key => $dataS) {

            $templateData['name'] = $dataS->firstname." ".$dataS->lastname;
            
            foreach ($dataS->fatchData as $key => $child) {

                $ddata = $this->TrackerForEngRecord($dataS->id,$child->id);

                $ddata['name'] = $child->firstname.' '.$child->lastname;

                $ddata['id'] = $child->id;

                $datas[] = $ddata;
                
            }



            $templateData['email'] = $dataS->email;

            //$templateData['email'] = "JalS@lucidux.com";
            $templateData['datas'] = $datas;

            $templateData['sitename'] = Config::get('constants.siteName');
            $emailData['templateData'] = $templateData;
            $emailData['template'] = 'mailtemplates.summeryEngineer';
            $emailData['subject'] = 'Sales Update Monthly Report'; 

            helper::sendEmail($emailData);
           
        }


        
       
    }


    public function TrackerForEngRecord($id,$person){
        $record = DB::table("salesrecord")->select("tarrget","actual", "growthykd","year","targetykd")->where("salesmanager",$id)->where("salesperson",$person)->orderBy("id","desc")->get();

        $currentActual = $currentTarget = $currentGrowth = $prevActual = 0;


        foreach ($record as $key => $child) {

            if($child->year == date("Y")){
                $currentActual += (float)$child->actual;
                $currentTarget += (float)$child->tarrget;
                $currentGrowth += (float)$child->growthykd;
            }else{
                $prevActual +=(float)$child->actual;
            }

        }

        return ['currentActual'=>$currentActual,'currentTarget'=>$currentTarget,'currentGrowth'=>$currentGrowth,'prevActual'=>$prevActual];


    }

    public function TrackerForAdRecord($id){
        $record = DB::table("salesrecord")->select("tarrget","actual", "growthykd","year","targetykd")->where("salesmanager",$id)->orderBy("id","desc")->get();

        $currentActual = $currentTarget = $currentGrowth = $prevActual = 0;

        foreach ($record as $key => $child) {

            if($child->year == date("Y")){
                $currentActual += (float)$child->actual;
                $currentTarget += (float)$child->tarrget;
                $currentGrowth += (float)$child->growthykd;
            }else{
                $prevActual +=(float)$child->actual;
            }

        }

        return ['currentActual'=>$currentActual,'currentTarget'=>$currentTarget,'currentGrowth'=>$currentGrowth,'prevActual'=>$prevActual];


    }
   

    public function salesperson(Request $request){

        $users = User::where("parent",$request->id)->where('role',3)->get();

        echo '<div class="form-group"><label for="salesmanager" class="form-label">Sales Person</label>';

        echo '<select class="form-control" name="salesperson">';
                echo '<option>Select Manager</option>';
        foreach ($users as $key => $value) {
            echo '<option value="'.$value->id.'">'.$value->firstname.' '.$value->lastname.'</option>';
        }

        echo '</select>';

        echo '</div>';

    }
    
}

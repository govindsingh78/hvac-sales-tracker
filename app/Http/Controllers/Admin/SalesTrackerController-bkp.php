<?php
namespace App\Http\Controllers\Admin;

use App\helper\CommonFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;
use File;
use Auth;
use App\User;
use App\SalesTracker;
/**
* 
*/

class SalesTrackerController extends Controller
{

	public function charts($managerId){

		$data = User::with("record")->where("parent",$managerId)->orderBy("id","asc")->get();
		
		$recordList = null;
		

		foreach ($data as $key => $single) {

			$person = $single->firstname.' '.$single->lastname;

			if(!empty($single->record)){

				$comPevious = $comCurrent = $actualCurrent = $actualPevious=[];

				foreach ($single->record as $data) {
	                
		            if($data->year == Date("Y")-1){

		                $actualPevious[] = round($data->actual);

		                $comPevious[] = round($data->ykd);

		            }
		            if($data->year == Date("Y")){
		                
		                $actualCurrent[] = round($data->actual);

		                $comCurrent[] = round($data->ykd);

		            }

		        }


		        $salesActual = [ 0=> ['name'=>'Actual Previous Year','data'=>$actualPevious,'color'=>'#7cb5ec'],1=>['name'=>'Actual Current Year','data'=>$actualCurrent,'color'=>'#f33']];

		        /* ----------- Commulitive data YKD -----------*/

		        $salesYkd = [ 0=> ['name'=>'Previous Year YTD','data'=>$comPevious,'color'=>'#f33'],1=>['name'=>'Current Year YTD','data'=>$comCurrent,'color'=>'#7cb5ec']];

		         
		        $recordList[] = ['person'=>$person,'salesActual'=>$salesActual,'salesYkd' =>$salesYkd,'id'=>$single->id];
		    }


		}


		return view('admin.trackerchart',compact('recordList'));

	}


	public function single($userId){

		$personRecord = User::with("record")->where("id",$userId)->first();

		$datas = SalesTracker::where("salesperson",$userId)->get();

		$previous_id = $comPevious = $comCurrent = $actualCurrent = $actualPevious=[];

        foreach ($datas as $data) {
                
            if($data->year == Date("Y")-1){

                $actualPevious[] = round($data->actual,2);

                $comPevious[] = round($data->ykd,2);

                $previous_id[] = $data->id;

            }
            if($data->year == Date("Y")){
                
                $actualCurrent[] = round($data->actual,2);

                $comCurrent[] = round($data->ykd,2);

            }

        }

       // $userId = $datas->id;



        $salesActual = [ 0=> ['name'=>'Actual Previous Year','data'=>$actualPevious,'color'=>'#7cb5ec'],1=>['name'=>'Actual Current Year','data'=>$actualCurrent,'color'=>'#f33']];

        /* ----------- Commulitive data YKD -----------*/

        $salesYkd = [ 0=> ['name'=>'Previous Year YTD','data'=>$comPevious,'color'=>'#f33'],1=>['name'=>'Current Year YTD','data'=>$comCurrent,'color'=>'#7cb5ec']];


        return view('admin.singletracker',compact('salesActual','salesYkd','datas','actualPevious','comPevious','personRecord','previous_id'));

	}


	public function save(Request $request){



		if(!empty($request->id)){

            foreach ($request->id as $key => $id) {
                
                DB::table("salesrecord")->where("id",$id)->where("year",Date("Y"))->update(['actual'=>$request->actual[$key],'ykd'=>$request->ykd[$key],'growthykd'=>$request->growth[$key]]);

            }

            foreach ($request->prev_id as $key => $id) {

            	if($id==0){

            		$record = DB::table("salesrecord")->insert(['actual'=>$request->previous_sctual[$key],'ykd'=>$request->previousYtd[$key],'year'=>Date("Y")-1,'salesperson'=>$request->salesperson,'salesmanager'=>$request->salesmanager,'month'=>$request->month[$key]]);

            	}else{

            		
            		$record = DB::table("salesrecord")->where("id",$id)->update(['actual'=>$request->previous_sctual[$key],'ykd'=>$request->previousYtd[$key]]);

            	}

            }

        }

        Session::flash('success', "Record Updated Successfully");

        return back();
	}

	public function trackerList(Request $request){

		$datas = User::with('fatchData')->where("role",2)->get();

		$usersR = User::select(DB::raw("CONCAT(`firstname`,' ',`lastname`) as fullname"),"id","firstname","lastname")->where('status',1)->where("role",2)->get();

		foreach ($usersR as $key => $record) {
			if(!empty($record->fullname)){
				$users[$record->id] = $record->fullname;
			}else{
				$users[$record->id] = $record->firstname;
			}
			
		}

		$users[0] = "Search by Sales Departments";

		ksort($users);

		return view('admin.salesrecords',compact('datas','users'));

	}

	public function searchRecord(Request $request){

		$datas = User::with('fatchData')->where("users.role",2)->where("users.id",$request->id)->get();

        return view('admin.elements.searchrecord',compact('datas'));

	}

	public function targetSave(Request $request){

		if(!empty($request->userId)){

			

			foreach ($request->userId as $key => $users) {


				
				if(!empty($request->yearlyTarget[$users])){

					$total = $request->yearlyTarget[$users];

					$targetYtd = 0;

					$recordParam = ["salesperson"=>$users,'busines_id'=>$request->parentId,'total'=>$total,'year'=>date("Y")];

					foreach ($request->monthsRecord[$users] as $key => $value) {

						if(!empty($value)){

							$targetAm = $value;

						}else{

							$targetAm = 0;

						}

						$month = $recordParam[strtolower(Date("M",mktime(0, 0, 0, $key+1, 10)))] = $request->montht[$key];

						$targetYtd += $targetAm;

						$data = ['tarrget'=>$targetAm,'percent'=>$month,'year'=>date("Y"),'salesperson'=>$users,'salesmanager'=>$request->parentId,'targetykd'=>$targetYtd];

						$record = DB::table("salesrecord")->where('salesperson',$users)->where('year',date("Y"))->where('month',$key+1)->where('salesmanager',$request->parentId)->update($data);
						
					}

					if(empty($this->recordExist($request,$users))){

						DB::table("record_param")->insert($recordParam);

					}else{

						DB::table("record_param")->where("salesperson",$users)->update($recordParam);

					}

					//

				}

			}

			
		}

		Session::flash('success', "Record Updated Successfully");

		return back();

	}


	public function recordExist($request,$users){

		$data = DB::table("record_param")->where('salesperson',$users)->first();
		$record = null;
		if(isset($data->id)){
			$record = $data->id;
		}
		return $record;
	}

}
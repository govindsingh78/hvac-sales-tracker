<?php
namespace App\Http\Controllers\Admin;

use App\helper\CommonFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;
use File;
use Auth;
use App\User;
use App\SalesTracker;
use App\RecordLoad;
use App\BusinessLineDetail;
use App\UserBusinessLine;
/**
* 
*/

class SalesTrackerController extends Controller
{

	public function charts($managerId){

		$data = User::with("record")->where("parent",$managerId)->orderBy("id","asc")->get();
		
		$recordList = null;
		

		foreach ($data as $key => $single) {

			$person = $single->firstname.' '.$single->lastname;

			if(!empty($single->record)){

				$comPevious = $comCurrent = $actualCurrent = $actualPevious=[];

				foreach ($single->record as $data) {
	                
		            if($data->year == Date("Y")-1){

		                $actualPevious[] = round($data->actual);

		                $comPevious[] = round($data->ykd);

		            }
		            if($data->year == Date("Y")){
		                
		                $actualCurrent[] = round($data->actual);

		                $comCurrent[] = round($data->ykd);

		            }

		        }


		        $salesActual = [ 0=> ['name'=>'Actual Previous Year','data'=>$actualPevious,'color'=>'#7cb5ec'],1=>['name'=>'Actual Current Year','data'=>$actualCurrent,'color'=>'#f33']];

		        /* ----------- Commulitive data YKD -----------*/

		        $salesYkd = [ 0=> ['name'=>'Previous Year YTD','data'=>$comPevious,'color'=>'#f33'],1=>['name'=>'Current Year YTD','data'=>$comCurrent,'color'=>'#7cb5ec']];

		         
		        $recordList[] = ['person'=>$person,'salesActual'=>$salesActual,'salesYkd' =>$salesYkd,'id'=>$single->id];
		    }


		}


		return view('admin.trackerchart',compact('recordList'));

	}

	 public function records(Request $request){

       	if($request->id != null)
        {
            $data = RecordLoad::find($request->id)->first();



            if(!empty($data->data) && File::exists(public_path($data->data))){

                unlink(public_path($data->data));
            }  

            RecordLoad::find($request->id)->delete();

            Session::flash('success', "Record Deleted Successfully");

            return back();

        }


        $records = RecordLoad::select("uploaded_report.*",DB::raw("(select concat(COALESCE(`firstname`,''),' ',COALESCE(`lastname`,'')) from users where users.id = uploaded_report.salesperson) as salesuser, (select concat(COALESCE(`firstname`,''),' ',COALESCE(`lastname`,'')) from users where users.id = uploaded_report.salesmanager) as manageruser"))->orderBy("uploaded_year","desc")->orderBy("uploaded_month","desc")->get();
        
        //$years = RecordLoad::orderBy("uploaded_year","desc")->groupBy("uploaded_year")->groupBy("uploaded_month")->get();
		

        $data = array();
		foreach ($records as $key => $value) {

			$data[$value->uploaded_year][$value->uploaded_month][trim($value->manageruser)][] = $value;

		}


        return view('admin.records',compact('data'));

    }

     public function recordManage(Request $request){

        $data = null;
        $person = array();
        if($request->id != null)
        {

            $data = RecordLoad::where("id",$request->id)->first();
            $person = User::where("role",3)->where("parent",$data->salesmanager)->pluck('firstname','id')->toArray();

        }

        $users = User::where("role",2)->pluck('firstname','id')->toArray();

        $users[null] = "-- Select Manager --";

        ksort($users);

        return view('admin.addrecord',compact('data','users','person'));

    }

    public function saveRcords(Request $request){

        $rules['year'] = "required|numeric";

        $rules['month'] = "required|numeric";

        $rules['salesmanager'] = "required|numeric";

        $rules['salesperson'] = "required|numeric";


        if($request->id ==null){
            $rules['file'] = "required";
        }

        $this->validate($request,$rules);
 
        if(!empty($request->file('file'))){

                $files = $request->file('file');

                if(!empty($request->prev_file) && File::exists(public_path($request->prev_file))){

                    unlink(public_path($request->prev_file));

                }            

                $update['data'] =  CommonFunction::UploadMedia($files,'user');

                

        }

        $update['uploaded_year'] =  $request->year;
        $update['uploaded_month'] =  $request->month;
        $update['salesperson'] =  $request->salesperson;
        $update['salesmanager'] =  $request->salesmanager;
        $update['admin_id'] =  Auth::user()->id;



        if($request->id !=null){
            $record = CommonFunction::Management(['id'=>base64_encode($request->id),"table"=>"uploaded_report","update"=>$update]);
            Session::flash('success', "Record Updated Successfully");
        }else{

            if(!empty($this->ExistData($update))){

                $record = RecordLoad::where("salesperson",Auth::user()->id)->where("uploaded_year",$update['uploaded_year'])->where("uploaded_month",$update['uploaded_month'])->update($update);

            }else{
                $record = CommonFunction::Management(['id'=>$request->id,"table"=>"uploaded_report","update"=>$update]);
            }
            
            Session::flash('success', "Record Added Successfully");
        }
        
        return redirect(route("admin.records"));

    }


	public function single($userId){

		
		// dd(Session::get('salesmanager_'.$userId));

		$personRecord = User::with("record")->where("id",$userId)->first();
		$datas = SalesTracker::where("salesperson",$userId)->where('salesmanager', Session::get('salesmanager_'.$userId))->get();

		

		
		$previous_id = $comPevious = $comCurrent = $actualCurrent = $actualPevious=[];

        foreach ($datas as $data) {
                
            if($data->year == Date("Y")-1){

                $actualPevious[] = round($data->actual,2);

                $comPevious[] = round($data->ykd,2);

                $previous_id[] = $data->id;

            }
            if($data->year == Date("Y")){

            	              
                $actualCurrent[] = round($data->actual,2);

                $comCurrent[] = round($data->ykd,2);

            }

        }



		$salesActual = [ 0=> ['name'=>'Actual Previous Year','data'=>$actualPevious,
		'color'=>'#7cb5ec'],1=>['name'=>'Actual Current Year','data'=>$actualCurrent
		,'color'=>'#f33']];

        /* ----------- Commulitive data YKD -----------*/

		$salesYkd = [ 0=> ['name'=>'Previous Year YTD','data'=>$comPevious,
		'color'=>'#7cb5ec'],1=>['name'=>'Current Year YTD','data'=>$comCurrent
		,'color'=>'#f33']];


        return view('admin.singletracker',compact('salesActual','salesYkd','datas','actualPevious','comPevious','personRecord','previous_id'));

	}

	

	 


	public function save(Request $request){


		// dd($request);

		if(!empty($request->id)){

            foreach ($request->id as $key => $id) {
                
				DB::table("salesrecord")->where("id",$id)->where("year",Date("Y"))->update(['actual'=>$request->actual[$key]
				,'ykd'=>$request->ykd[$key],'growthykd'=>$request->growth[$key],'backlog'=>$request->backlog[$key]]);

            }

            foreach ($request->prev_id as $key => $id) {

            	

            	if($id==0){

					$record = DB::table("salesrecord")->insert(['actual'=>$request->previous_sctual[$key],
					'ykd'=>$request->previousYtd[$key],'year'=>Date("Y")-1,'salesperson'=>$request->salesperson,
					'salesmanager'=>$request->salesmanager,'month'=>$request->month[$key]]);

            	}else{

            		
					$record = DB::table("salesrecord")->where("id",$id)->update(['actual'=>$request->previous_sctual[$key],
					'ykd'=>$request->previousYtd[$key]]);
            		
            	}

            }

        }


        Session::flash('success', "Record Updated Successfully");

        return back();
	}

	public function trackerList(Request $request){

		$datas = User::with('fatchData')->where("role",2)->get();

		$usersR = BusinessLineDetail::select("id","business_line")->where('status',0)->get();

		foreach ($usersR as $key => $record) {
			if(!empty($record->business_line)){
				$users[$record->id] = $record->business_line;
			}else{
				$users[$record->id] = $record->business_line;
			}
			
		}

		//dd($users);

		$users[0] = "Select Business Line";

		ksort($users);

		return view('admin.salesrecords',compact('datas','users'));

	}

	public function searchRecord(Request $request){

		$response = DB::table('business_line_details as bld')
		->select('u.id as salesmanager_id', 'u.firstname as firstnameSaleMan', 'u.lastname as lastnameSaleMan','bld.id as businessline_id','bld.business_line as businessline_name')
		->join('users_business_lines as ubl', 'ubl.business_line_id', '=', 'bld.id')
		->join('users as u', 'u.id', '=', 'ubl.user_id')
		->where('u.role', 2)
		->where('bld.id', $request->id)
		->where('bld.status', 0)
		->orderBy('u.hod', 'ASC')
		->get();
		$arrayBusinessLine = [];
		foreach($response as $key => $val){
			$arrayBusinessLine[$val->businessline_id][] = $val;
		}
		$grossData = [];
		foreach($arrayBusinessLine as $key => $businessLineArr){
			$business_name = $businessLineArr[0]->businessline_name;
			$grossDatas = []; 
			foreach($businessLineArr as $k => $blArr){
 
				$salesmanager_id = $blArr->salesmanager_id;
				$salesmanager_name = $blArr->firstnameSaleMan." ".$blArr->lastnameSaleMan;
				 
  				$responseSalesPers = DB::table('users as u')
				->select('u.*')
				->join('users_business_lines as ubl', 'ubl.user_id', '=', 'u.id')
				->where('u.role', 3)
				->where('ubl.business_line_id', $request->id)
				->where('ubl.status', 0)
				->orderBy('u.hod', 'ASC')
				->get();
				$i = 0;
				
				foreach ($responseSalesPers as $key => $child) {
					$parentArr = explode(",", $child->parent);
					if(in_array($salesmanager_id, $parentArr)){
						$vari = $responseSalesPers[$key];
						unset($responseSalesPers[$key]);
						$responseSalesPers[$i] = $vari;
						$i++;
					}else{
						unset($responseSalesPers[$key]);
					}

					
				}
				$grossData[$salesmanager_id]['businessline_id'] = $request->id;
				$grossData[$salesmanager_id]['fatchData'] = $responseSalesPers;
				$grossData[$salesmanager_id]['id'] = $salesmanager_id;
				$grossData[$salesmanager_id]['salesManagerName'] = $salesmanager_name;
		 	}

		}
		$grossData = json_decode(json_encode($grossData), FALSE);
		return view('admin.elements.searchrecord',compact('grossData'));
	}




	public function targetSave(Request $request){



		//  dd($request->childParent);

		if(!empty($request->userId)){

			

			foreach ($request->userId as $key => $users) {

				$request->salesManagerId = $request->childParent[$key];
				 
				if(!empty($request->yearlyTarget[$users])){

					$total = $request->yearlyTarget[$users];

					$targetYtd = 0;

					$recordParam = ["salesperson"=>$users,'busines_id'=>$request->parentId,'total'=>$total,'year'=>date("Y")];

					foreach ($request->monthsRecord[$users] as $key => $value) {

						if(!empty($value)){

							$targetAm = $value;

						}else{

							$targetAm = 0;

						}

						$month = $recordParam[strtolower(Date("M",mktime(0, 0, 0, $key+1, 10)))] = $request->montht[$key];

						$targetYtd += $targetAm;

						$data = ['tarrget'=>$targetAm,'percent'=>$month,'year'=>date("Y"),'salesperson'=>$users,'salesmanager'=>$request->salesManagerId,'targetykd'=>$targetYtd];
						 
						$checkCount = DB::table("salesrecord")->where('year',date("Y"))->where("month",$key+1)->where('salesperson',$users)->where('salesmanager', $request->salesManagerId)->count(); 
						if($checkCount > 0){
							$record = DB::table("salesrecord")->where('salesperson',$users)->where('year',date("Y"))->where('month',$key+1)
							->where('salesmanager',$request->salesManagerId)->update($data);
						}else{

							$data['year'] = date("Y");	
							$data['salesperson'] = $users;
							$data['month'] = $key+1;
							$data['salesmanager'] = $request->salesManagerId;
							$record = DB::table("salesrecord")->insert($data);
						}
						

						 
						
					}

					//dump($recordParam->busines_id);


					if(empty($this->recordExist($request,$users))){

						DB::table("record_param")->insert($recordParam);

					}else{

						//dump(DB::table("record_param")->where("salesperson",$users)->update($recordParam));

						DB::table("record_param")->where("salesperson",$users)->update($recordParam);

					}

					//

				}

			}

			
		}

		Session::flash('success', "Record Updated Successfully");

		return back();

	}
	public function targetSaveOld(Request $request){
 
		
		if(!empty($request->userId)){
			$i = 0;
			foreach ($request->userId as $key => $users) {

				//dump($request->childParent[$i]);


 				if(!empty($request->yearlyTarget[$users])){
					$total = $request->yearlyTarget[$users];
					$targetYtd = 0;
					$recordParam = ["salesperson"=>$users,'busines_id'=>$request->parentId,'total'=>$total,'year'=>date("Y")];
					foreach ($request->monthsRecord[$users] as $key => $value) {
						if(!empty($value)){
							$targetAm = $value;
						}else{
							$targetAm = 0;
						}

						$month = $recordParam[strtolower(Date("M",mktime(0, 0, 0, $key+1, 10)))] = $request->montht[$key];

						$targetYtd += $targetAm;

						$data = ['tarrget'=>$targetAm,'percent'=>$month,'year'=>date("Y"),
						'salesperson'=>$users,'salesmanager'=>$request->childParent[$i],
						'targetykd'=>$targetYtd];
						// dd($request->salesManagerId);
						$countData = DB::table("salesrecord")->where('year',date("Y"))->where("month",$key+1)->where('salesperson',$users)
						->where('salesmanager', $request->childParent[$i])->count();
						if($countData > 0){
							$record = DB::table("salesrecord")->where('salesperson',$users)->where('year',date("Y"))->where('month',$key+1)
							->where('salesmanager',$request->childParent[$i])->update($data);
						}else{
							$data['year'] = date("Y");	
							$data['salesperson'] = $users;
							$data['month'] = $key+1;
							$data['salesmanager'] = $request->childParent[$i];
							$record = DB::table("salesrecord")->insert($data);
						}
						

						 
						
					}

					//dump($recordParam->busines_id);


					if(empty($this->recordExist($request,$users))){

						DB::table("record_param")->insert($recordParam);

					}else{

						//dump(DB::table("record_param")->where("salesperson",$users)->update($recordParam));

						DB::table("record_param")->where("salesperson",$users)->update($recordParam);

					}

					//

				}
				$i++;
			}

			
		}

		Session::flash('success', "Record Updated Successfully");

		return back();

	}

	public function recordDataExist($users,$month){

		$data = DB::table("salesrecord")->where('year',date("Y"))->where("month",$month)->where('salesperson',$users)->first();
		$record = null;
		if(isset($data->id)){
			$record = $data->id;
		}
		return $record;
	}

	public function recordDataExistNew($users,$smid,$month){

		$data = DB::table("salesrecord")->where('year',date("Y"))->where("month",$month)->where('salesperson',$users)->where('salesmanager', $smid)->first();
		$record = null;
		if(isset($data->id)){
			$record = $data->id;
		}
		return $record;
	}


	public function recordExist($request,$users){

		$data = DB::table("record_param")->where('salesperson',$users)->first();
		$record = null;
		if(isset($data->id)){
			$record = $data->id;
		}
		return $record;
	}

	public function ExistData($data){


        $records = RecordLoad::where("salesperson",Auth::user()->id)->where("uploaded_year",$data['uploaded_year'])->where("uploaded_month",$data['uploaded_month'])->orderBy("id","desc")->first();

        if(!empty($records)){
            return $records->id;
        }else{
            return Null;
        }
        
    }

}
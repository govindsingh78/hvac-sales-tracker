<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use helper;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        

        $managers = User::with('fatchData')->where("role",2)->where("status",1)->get();
        
        $settings = DB::table("currency")->where("id",1)->first();

        foreach ($managers as $key => $dataS) {
            $ytdTarget = $target = $growth = 0;
            $ids = null;
            foreach ($dataS->fatchData as $key => $child) {

                $reco = helper::growthVLast($dataS->id,$child->id);
                $growth += $reco[0];
                $target += $reco[1];

                $ytdTarget += helper::ytdVTargetNP($dataS->id,$child->id);
                
                $ids[] = $child->id;
            }

            $datas[] = ['name'=>$dataS->firstname." ".$dataS->lastname,"target"=>$target,"ytdTarget"=>round($ytdTarget),"growth"=>$growth,"id"=>$dataS->id,"count"=>count($ids)];


        }

        return view('admin.dashboard',compact('datas','settings'));
    }

    public function businessline(Request $request)
    {

        $datas = User::with('fatchData')->where("role",2)->where("id",$request->id)->first();
        
        $settings = DB::table("currency")->where("id",1)->first();

        return view('admin.businessline',compact('datas','settings'));

    }

}

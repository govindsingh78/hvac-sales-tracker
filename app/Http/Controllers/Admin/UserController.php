<?php
namespace App\Http\Controllers\Admin;

use App\helper\CommonFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;
use File;
use Auth;
use App\User;
use App\SalesTracker;
use App\BusinessLineDetail;
use App\UserBusinessLine;
/**
* 
*/ 
class UserController extends Controller
{
	
	public function index(Request $request){


		$data['list'] = User::select("users.*",DB::raw("(select type from userrole where users.role = userrole.id) as userrole, (select firstname from users u where u.id = users.parent) as salesmanager"))->where("id","!=",Auth::user()->id)->orderBy("id","desc")->get();
		   
		
		
		if(!empty($request->ids)){

			$files = User::whereIn('id',$request->ids)->get();

			foreach ($files as $file) {

				if(!empty($file->image) && File::exists(public_path($file->image))){

		            unlink(public_path($file->image));

		        } 

			}

			Session::flash('success', "User deleted successfully.");

			DB::table("salesrecord")->whereIn("salesperson",$request->ids)->delete();

			DB::table("record_param")->whereIn("salesperson",$request->ids)->delete();

			User::whereIn('id',$request->ids)->delete();

			return redirect('admin/user-list');

		}
		elseif(!empty($request->id)){

			$file = User::where('id',base64_decode($request->id))->first();

			if(!empty($file->image) && File::exists(public_path($file->image))){

	            unlink(public_path($file->image));

	        } 

	        DB::table("salesrecord")->where("salesperson",base64_decode($request->id))->delete();

	        DB::table("record_param")->where("salesperson",base64_decode($request->id))->delete();

	        Session::flash('success', "User deleted successfully.");

			User::where('id',base64_decode($request->id))->delete();

			return redirect('admin/user-list');

		}else{

			return view("admin.users.user-list")->with('data',$data);

		}

	}

	public function create(Request $request){



		$data = [];

		//$parent = User::select(DB::raw("CONCAT(firstname,lastname) as fullname"),"id")->where("role",2)->pluck("fullname","id");

		$usersR = User::select(DB::raw("CONCAT(`firstname`,' ',`lastname`) as fullname"),"id","firstname","lastname")->where('status',1)->where("role",2)->get();

		foreach ($usersR as $key => $record) {
			if(!empty($record->fullname)){
				$parent[$record->id] = $record->fullname;
			}else{
				$parent[$record->id] = $record->firstname;
			}
			
		}

		$roles = DB::table("userrole")->pluck('type','id')->toArray();

		$roles[0] = "Select Role";

		ksort($roles);
		
		if(!empty($request->id)){

			$data['record'] = User::where("id",base64_decode($request->id))->first();

		}

		//dd($parent); 

		return view("admin.users.user-create",compact("roles","parent"))->with('data',$data);


	}

	public function store(Request $request){

	//  //dd($request->buisnessline);
	//  dd($request->salespersonbuisnessline);

		if(!empty($request->feild)){


				$messagesd["feild.email.regex"] = "Email not valid";

				foreach ($request->feild as $key => $value) {

					if($key != "company"){

						if(strrpos($key, "_") > 0){
							$messagesd['feild.'.$key.".required"] = ucfirst(str_replace("_", " ", $key))." required";
						}else{
							$messagesd['feild.'.$key.".required"] = ucfirst($key)." required";
						}

						if(empty($request->id) || $this->isBroker($request->feild['username'],$request->id) == false){
							 
							if($key == 'username'){

								$rules['feild.'.$key] = "required|unique:users,username";

							}elseif($key == 'email'){
								$rules['feild.'.$key] = "required|regex:/^.+@.+$/i";
							}
							else{
								$rules['feild.'.$key] = "required";
								 
							}
							//dump($key);
						}else{


							 if($key == 'email'){

								$rules['feild.'.$key] = "required|regex:/^.+@.+$/i";
							}
							else{
								//dump($key);
								$rules['feild.'.$key] = "required";
							}

						}

					}
					
					if($key=="password"){
						$update[$key]= Hash::make($value);
					}else{
						$update[$key]= $value;
					}
					
				}

				
		
			
				$this->validate($request,$rules,$messagesd);
				//dd($update['role']);
				

				if($update['role'] == 3){

					if(!isset($request->parent)){

						Session::flash('error', "Please select manager");

						return back();

					} else{

						$update['parent'] = implode(",", $request->parent);
					
					}
					
					
				}

				if($update['role'] == 2){

					  if(!isset($request->buisnessline)){

						Session::flash('error', "Please select at least one business line");

						return back();

					}
				 
				}


				if(empty($this->isExist(Auth::user()->id,"created_by"))){
					$update['created_by'] = Auth::user()->id;
				}
				
				


				if(isset($request->removeImage)){

		        	if(!empty($request->prev_image) && File::exists(public_path($request->prev_image))){

		                unlink(public_path($request->prev_image));

		            }   
		            $update['image'] = "";

		            
		        }else{

		        	if(!empty($request->file('image'))){

				            $files = $request->file('image');

				            if(!empty($request->prev_image) && File::exists(public_path($request->prev_image))){

				                unlink(public_path($request->prev_image));

				            }            

				            $update['image'] =  CommonFunction::UploadMedia($files,'user');

				        }

		        }
		       
				if(!empty($request->id)){

					$id= base64_decode($request->id);
					

					if($update['role'] == 2 || $update['role'] == 3){
						//call a function to save business line in case existing user
						$this->saveBusinessLine($id, $request, $update);
					}
					
					
					$record = CommonFunction::Management(['id'=>$request->id,"table"=>"users","update"=>$update,"status"=>"Admin"]);

					Session::flash('success', "Record Updated Successfully");

				}else{

					$role = DB::table("userrole")->select("type")->where("id",$update['role'])->first();

					$otp =  CommonFunction::generateUnique(5);

					$update['otp'] = $otp;
					
					$templateData['name'] = $update['firstname'].' '.$update['lastname'];
		            $templateData['email'] = $update['email'];
		            $templateData['role'] = $role->type;
		            $templateData['type'] = $update['role'];

		            $templateData['otp'] = $otp;
		            $templateData['message'] = "Your Account Created";
		            $templateData['sitename'] = Config::get('constants.siteName');
		            $emailData['templateData'] = $templateData;
		            $emailData['template'] = 'mailtemplates.accountcreate';
		            $emailData['subject'] = 'your account Created'; 

		            //CommonFunction::sendEmail($emailData);
					$record = DB::table('users')->insertGetId($update);
					if($update['role'] == 2 || $update['role'] == 3){
						//call a function to save business line in case new user created
						$this->saveBusinessLine($record, $request, $update);
					}


					

					Session::flash('success', "Record Added Successfully");

				}

				

				return redirect('admin/user-list');
			

		}

	}

	public function saveBusinessLine($user_id, $request, $update){

		// dd(implode(",", $request->parent));
		$arrayBusinessLine = $request->buisnessline;
		UserBusinessLine::where('user_id', '=', $user_id)->delete();

		foreach($arrayBusinessLine as $key => $businessLineId){
			// check if user_id and business line id exist 
			$checkCount = UserBusinessLine::where('user_id', '=', $user_id)->where('business_line_id', '=', $businessLineId)->count();

			if($checkCount > 0){
				// update status = 0 active 
				UserBusinessLine::where('user_id','=',$user_id)->where('business_line_id', '=', $businessLineId)->update(['status'=>0]);

			}else{
				// insert fresh entry
				$saveData = array('user_id'=>$user_id, 'business_line_id'=>$businessLineId);
				$record = DB::table('users_business_lines')->insert($saveData);
				
			}

			// edit case 


			// fresh business id
			if($update['role'] == 3){
				
				$existSalesManagerPerson = DB::table('salesrecord')
				->where('salesperson', '=', $user_id)->count();

				if($existSalesManagerPerson > 0){
					DB::table('salesrecord')->where('salesperson',$user_id)->update(['salesmanager'=>$update['parent']]);
	
				}else{
					for($i=1;$i<=12;$i++){

						$salesrecord = ['id'=>'','actual'=>'0','tarrget'=>'0','ykd'=>'0','targetykd'=>'0','growthykd'=>'0','backlog'=>'0',
						'month'=>$i,'year'=>date("Y"),'percent'=>'0','salesperson'=>$user_id,'salesmanager'=>$update['parent']];
	
						DB::table('salesrecord')->insert($salesrecord);
					}
				}

				 
			}
 
			

		}



	}




	public function referencePrefix(Request $resuest){
		echo CommonFunction::referencePrefix($resuest->id);
	}

	
	public function status(Request $request){

		if(!empty($request->id)){

			$record = User::where('id','=',base64_decode($request->id))->first();

			if($record->status==1){

				User::where('id','=',base64_decode($request->id))->update(['status'=>0]);

				Session::flash('success', "User inActive successfully");

			}elseif($record->status==0){

				User::where('id','=',base64_decode($request->id))->update(['status'=>1]);

				Session::flash('success', "User Active successfully");

			}

			return redirect('admin/user-list');

		}


	}


	public function sendPassword(Request $request){


		$user = User::select("users.*",DB::raw("(select type from userrole where userrole.id = users.role) as usertype"))->where("users.id",base64_decode($request->id))->first();

		$otp =  CommonFunction::generateUnique(5);

		$update['otp'] = $otp;

        if(User::where("id",base64_decode($request->id))->update(["otp"=>$otp])==true){
		
			$templateData['name'] = $user->firstname.' '.$user->lastname;
	        $templateData['email'] = $user->email;
	        $templateData['role'] = $user->usertype;
	        $templateData['type'] = $user->role;

	        $templateData['otp'] = $otp;

	        $templateData['sitename'] = Config::get('constants.siteName');
	        $emailData['templateData'] = $templateData;
	        $emailData['template'] = 'mailtemplates.passwordsent';
	        $emailData['subject'] = 'Password Reset Link'; 

	        CommonFunction::sendEmail($emailData);

		    Session::flash('success', 'Email sent to the Member!');
	        return back();
	    }else{

	    	Session::flash('error', 'There may be some technical issue!');

	    	return back();
	    }


	}



	public function password(Request $request){

		if(!empty($request->id)){

		

			if($request->password == $request->cpassword){

				User::where('id',base64_decode($request->id))->update(['password'=>Hash::make($request->password)]);

				Session::flash('success', 'Password Updated!');

			}else{

				Session::flash('error', "Password Not Match");

			}

			//return redirect('admin/user-list');

		}
	

	}

	public function isBroker($email,$id){

		$record = User::where('username',$email)->where('id','=',base64_decode($id))->first();

				
		if(!empty($record)){

			return true;

		}else{

			return false;

		}

	}

	public function isExist($id,$feild){

		$record = User::where('id',''!='',$id)->first();

		$null = null;

		if(!empty($record)){

			return $record->$feild;

		}else{

			return $null;

		}
	}


}
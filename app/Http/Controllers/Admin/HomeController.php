<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusinessLineDetail;
use App\UserBusinessLine;
use App\User;
use DB;
use helper;
use App\SalesTracker;
use App\RecordLoad;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		
		$response = DB::table('business_line_details as bld')
		->select('bld.id as businessline_id',	'bld.business_line as businessline_name')
		->where('bld.status', 0)
		->orderBy('bld.sort_order', 'ASC')
		->get();
	 

		
		$datas = [];
		 
		foreach($response as $key => $businessLineArr){

			$business_id = $businessLineArr->businessline_id;
			$business_name = $businessLineArr->businessline_name;
			 
			 $idArr = [];
			 $actualTotal =  $ytdTargetTotal = $targetTotal = $growthTotal = 0;
			 $idsCount = 0;


			$responseSalesPers = DB::table('users as u')
			->select('u.*')
			->join('users_business_lines as ubl', 'ubl.user_id', '=', 'u.id')
			->where('u.role', 3)
			->where('ubl.business_line_id', $business_id)
			->where('ubl.status', 0)
			->orderBy('u.hod', 'ASC')
			->get();
			$actual =  $ytdTarget = $target = $growth = 0;
			$ids = [];

			foreach($responseSalesPers as $k => $child){



				if(!empty($child->id)){
						$reco = helper::growthVLast($child->parent,$child->id);

						$growth += $reco[0];
						$target += $reco[1];
						$actual += $reco[3];
						$ytdTarget += helper::ytdVTargetNP($child->parent,$child->id);
						$ids[] = $child->id;
				}
			 

			}
				 
				$growthTotal += $growth;
				$targetTotal += $target;
				$actualTotal += $actual;
				$ytdTargetTotal += $ytdTarget;
				$idsCount += count($ids);


				$datas[] = ['name'=>$business_name,"target"=>$targetTotal,"ytdTarget"=>round($ytdTargetTotal),
				"growth"=>$growthTotal,"id"=>$business_id,"count"=>$idsCount,"actual"=>$actualTotal];	 
				
			}
			
			
		
 
 
		 $settings = DB::table("currency")->where("id",1)->first();
	     return view('admin.dashboard',compact('datas','settings'));
	}
	


	 


    
    public function businessline(Request $request)
    {
        
		$response = DB::table('business_line_details as bld')
		->select('u.*','bld.id as businessline_id','bld.business_line as businessline_name')
		->join('users_business_lines as ubl', 'ubl.business_line_id', '=', 'bld.id')
		->join('users as u', 'u.id', '=', 'ubl.user_id')
		->where('u.role', 2)
		->where('bld.id', $request->id)
		->where('bld.status', 0)
		->orderBy('u.hod', 'ASC')
		->get();
		$arrayBusinessLine = [];
		foreach($response as $key => $val){
			$arrayBusinessLine[$val->businessline_id][] = $val;
		}
	 
		foreach($arrayBusinessLine as $key => $businessLineArr){
			$business_name = $businessLineArr[0]->businessline_name;
			$grossData = [];
			
			// $grossData[] = $businessLineArr;
			foreach($businessLineArr as $k => $blArr){

				//dd($blArr);
 
				$salesmanager_id = $blArr->id;
				// $salesmanager_name = $blArr->firstname." ".$blArr->lastname;

				 $grossData[$k][] = $blArr;
				 
  				$responseSalesPers = DB::table('users as u')
				->select('u.*')
				->join('users_business_lines as ubl', 'ubl.user_id', '=', 'u.id')
				->where('u.role', 3)
				->where('ubl.business_line_id', $request->id)
				->where('ubl.status', 0)
				->orderBy('u.hod', 'ASC')
				->get();
				$i = 0;
				
				foreach ($responseSalesPers as $key => $child) {
					$parentArr = explode(",", $child->parent);
					if(in_array($salesmanager_id, $parentArr)){
						$vari = $responseSalesPers[$key];
						unset($responseSalesPers[$key]);
						$responseSalesPers[$i] = $vari;
						
						$i++;
					}else{
						unset($responseSalesPers[$key]);
					}

					$grossData[$k]['fatchData'] = $responseSalesPers;
					
				}

				

				  
				// $grossData[$salesmanager_id]['businessline_id'] = $request->id;
				
				// $grossData[$salesmanager_id]['id'] = $salesmanager_id;
				// $grossData[$salesmanager_id]['salesManagerName'] = $salesmanager_name;
		 	}

		}
		//$grossData = json_decode(json_encode($grossData), FALSE);


		$settings = DB::table("currency")->where("id",1)->first();
		
		//dd($newArr);
		
		// Combined calculation starts for Distribution
		$datas_4 = User::with('fatchData')->where("role",2)->where("id",4)->first();
        $datas_2 = User::with('fatchData')->where("role",2)->where("id",2)->first();
        $datas_3 = User::with('fatchData')->where("role",2)->where("id",3)->first();
    	// dd($grossData);

        if($request->id == 7){

            $this->single(81);
            $this->single(82);
            $this->single(83);
            $this->single(84);
            $this->single(77);

        }

		//dd($grossData);
        return view('admin.businessline',compact('grossData','settings'));

    }

// Functions added for calculating the combined Distribution by Govind Singh Start

    public function single($userId){

		$personRecord = User::with("record")->where("id",$userId)->first();
		$datas = SalesTracker::where("salesperson",$userId)->get();

		
			$this->cleanUpData(7,$userId,date("Y"));
				
				switch ($userId) {
				case '81':
				 
				$this->combinedCalculation(7,$userId,4,95, date("Y"));
				$this->combinedCalculation(7,$userId,2,85, date("Y"));
				$this->combinedCalculation(7,$userId,3,90, date("Y"));
				break;
				case '82':
				 
				$this->combinedCalculation(7,$userId,4,96, date("Y"));
				$this->combinedCalculation(7,$userId,2,86, date("Y"));
				$this->combinedCalculation(7,$userId,3,91, date("Y"));
				break;
				case '83':
				 
				$this->combinedCalculation(7,$userId,4,97, date("Y"));
				$this->combinedCalculation(7,$userId,2,87, date("Y"));
				$this->combinedCalculation(7,$userId,3,92, date("Y"));
				break;
				case '84':
				 
				$this->combinedCalculation(7,$userId,4,98, date("Y"));
				$this->combinedCalculation(7,$userId,2,88, date("Y"));
				$this->combinedCalculation(7,$userId,3,93, date("Y"));
				break;
				case '77':
				
				$this->combinedCalculation(7,$userId,4,99, date("Y"));
				$this->combinedCalculation(7,$userId,2,17, date("Y"));
				$this->combinedCalculation(7,$userId,3,94, date("Y"));
				break;
			


		}
	 




	 
    }
    



    public function cleanUpData($smanager, $sperson, $year){
	 
        $cleanData = DB::table('salesrecord')->where(['salesperson'=>$sperson, 'salesmanager'=> $smanager, 'year'=> $year])
        ->update(array('actual' => 0,'ykd' =>0, 'growthykd' => 0, 'tarrget' => 0, 'targetykd' => 0, 'percent' => 0));

	}

	

	public function combinedCalculation($smanager, $sperson, $smanager_from, $sperson_from, $year){

	
		$businessLineData = SalesTracker::where("salesmanager",$smanager_from)
		->where("salesperson",$sperson_from)
		->where("year",$year)
		->get();
 

		foreach ($businessLineData as $key => $value) {
			 $month = $value->month;
			 $year = $value->year;
			 $this->updateMonthlyData($smanager, $sperson, $year, $month, $value);
		}


	}




	public function updateMonthlyData($smanager, $sperson, $year ,$month, $value){

			$dataMonth1 = SalesTracker::where("salesmanager",$smanager)
			->where("salesperson",$sperson)
			->where("year",$year)
			->where("month",$month)
			->first();
 
			 $actual = $dataMonth1->actual;
			//  $actualTotal = $actual + $value->actual;
			$actualTotal = 0;

			 $ykd = $dataMonth1->ykd;
			//  $ykdTotal = $ykd + $value->ykd;

			$ykdTotal = 0;

			 $growthykd = $dataMonth1->growthykd;
			//  $growthykdTotal = $growthykd + $value->growthykd;

			$growthykdTotal = 0;


			 $tarrget = $dataMonth1->tarrget;
			 $tarrgetTotal = $tarrget + $value->tarrget;


			 $targetykd = $dataMonth1->targetykd;
			 //$targetykdTotal = $targetykd + $value->targetykd;

			 $targetykdTotal = 0;

			 $percent = $dataMonth1->percent;
			//  $percentTotal = $percent + $value->percent;
			 $percentTotal = 0;
 

 			$saveData = DB::table('salesrecord')->where(['salesperson'=>$sperson, 'salesmanager'=> $smanager, 'year'=> $year, 'month' => $month])->update(array('actual' => $actualTotal,'ykd' => $ykdTotal, 'growthykd' => $growthykdTotal, 'tarrget' => $tarrgetTotal, 'targetykd' => $targetykdTotal, 'percent' => $percentTotal));
 

	}



// Functions added for calculating the combined Distribution by Govind Singh End

}

<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class VerifySalesPerson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'salesperson')
    {


        if (Auth::check() == true &&  Auth::user()->role == 3) {

            return $next($request);     

        }elseif (Auth::check() == true &&  Auth::user()->role == 2) { 

            return redirect('/salesmanager');   

        }elseif (Auth::check() == true &&  Auth::user()->role == 1) { 
           
            return redirect('/admin');   

        }else{
            return redirect('/salesperson');
        }
        
    }


}

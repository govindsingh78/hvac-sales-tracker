<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBusinessLine extends Model
{
     

    protected $table = "users_business_lines";

    protected $fillable = [
        'business_line_id', 'user_id', 'status',
    ];

    
    
}
